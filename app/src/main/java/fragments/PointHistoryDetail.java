package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import Constants.Tags;
import adapters.ImageAdapter;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PointsHistoryModel;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class PointHistoryDetail extends Fragment {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private PointsHistoryModel product;
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy;
    private ArrayList<ProductListModel> productListModelArrayList;
    private ImageView image;
    private TextView drivername,comp_name,race_name,lap,date,total_credit,total_point,time,address;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.points_history_detail, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Points Detail");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        bundle = getArguments();
        product=new PointsHistoryModel();
        product = bundle.getParcelable(Tags.parcel_pointsHistoryItem);
        AppDelegate.Log("Tag",product+"");

        drivername = (TextView) rootview.findViewById(R.id.driver_name);
        comp_name = (TextView) rootview.findViewById(R.id.competion_name);
        race_name = (TextView) rootview.findViewById(R.id.race_name);
        lap = (TextView) rootview.findViewById(R.id.lap);
        date = (TextView) rootview.findViewById(R.id.date);
        total_credit = (TextView) rootview.findViewById(R.id.credit);
        total_point  = (TextView) rootview.findViewById(R.id.points);
        rank = (TextView) rootview.findViewById(R.id.rank);
        time=(TextView)rootview.findViewById(R.id.time);
        address=(TextView)rootview.findViewById(R.id.address);
        String sdate = null;
        try {
            sdate = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(product.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        drivername.setText(product.getDriver_name()+"");
        comp_name.setText(product.getCompetitions_name()+"");
        race_name.setText(product.getRace_name()+"");
        lap.setText(product.getLap()+"");
        date.setText(sdate+"");
        total_credit.setText(String.valueOf(product.getTotal_credit()+"")+"");
        total_point.setText(String.valueOf(product.getTotal_point()+"")+"");
        rank.setText(product.getCurrent_rank()+"");
        time.setText(product.getTime());
        address.setText(product.getAddress());
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
