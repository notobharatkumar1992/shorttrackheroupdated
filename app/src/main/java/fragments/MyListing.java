package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import adapters.DriverListAdapter;
import adapters.RaceHistoryAdapter;
import interfaces.OnReciveServerResponse;
import model.LoginModel;
import model.PostAysnc_Model;
import model.RaceHistroyModel;

/**
 * Created by admin on 30-06-2016.
 */
public class MyListing extends Fragment implements OnReciveServerResponse {
    private View rootview;
    private RecyclerView driverlist;
    private String identifier;
    private int status;
    public LoginModel getProfile_model;
    private int user_id;
    private RaceHistroyModel racehistory_model;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.driver, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Race History");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        driverlist = (RecyclerView) rootview.findViewById(R.id.driverslist);
        execute_raceHistory();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void execute_raceHistory() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;

                    mPostAsyncObj = new PostAsync(getActivity(), MyListing.this, ServerRequestConstants.RACE_HISTORY,
                            mPostArrayList, MyListing.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();


            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void setracehistory(ArrayList<RaceHistroyModel> raceHistoryArray) {
            LinearLayoutManager layoutManager
                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            RaceHistoryAdapter adapter = new RaceHistoryAdapter(getActivity(), raceHistoryArray);
            driverlist.setLayoutManager(layoutManager);
            driverlist.setAdapter(adapter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), " Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.RACE_HISTORY)) {

            parseRaceHistory(result);
        }
    }

    private void parseRaceHistory(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            racehistory_model = new RaceHistroyModel();
            racehistory_model.setStatus(jsonObject.getInt("status"));
            if (racehistory_model.getStatus() == 1) {
                JSONObject object = jsonObject.getJSONObject("response");
                JSONArray jsonArray = object.getJSONArray("Race");
                ArrayList<RaceHistroyModel> raceHistoryArray = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    racehistory_model = new RaceHistroyModel();
                    racehistory_model.setFan_id(obj.getInt("fan_id"));
                    racehistory_model.setTrack_id(obj.getInt("track_id"));
                    racehistory_model.setTrack_name(obj.getString("track_name"));
                    racehistory_model.setCompetition_id(obj.getInt("competition_id"));
                    racehistory_model.setRace_name(obj.getString("race_name"));
                    racehistory_model.setCompetition_name(obj.getString("competition_name"));
                    racehistory_model.setRace_id(obj.getInt("race_id"));
                    raceHistoryArray.add(racehistory_model);
                }
                setracehistory(raceHistoryArray);
            } else if (racehistory_model.getStatus() == 0) {
                AppDelegate.ShowDialog(getActivity(), jsonObject.getString("message"), "Alert !!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
