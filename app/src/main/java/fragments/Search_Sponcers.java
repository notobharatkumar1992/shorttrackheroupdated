package fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.CategoryAdapter;
import adapters.SponcerAdapter;
import interfaces.OnReciveServerResponse;
import model.GetProfile_Model;
import model.Get_News_Model;
import model.PostAysnc_Model;
import model.SponcerCategoryModel;
import model.SponcerModel;
import model.SponsorDetail;


/**
 * Created by admin on 16-06-2016.
 */
public class Search_Sponcers extends Fragment implements OnReciveServerResponse, View.OnClickListener {
    View rootview;
    EditText confirm_password;
    TextView submit;
    private String device_id;
    private int status;
    GetProfile_Model getProfile_model;
    private TextView search_sponcer, search, title;
    private String identifier;
    GridView sponcerList;
    private String message;
    private TextView save;
    private int driver_id = 0;
    private SponcerCategoryModel categoryModel;
    private Dialog dialog;
    ListView list;
    private int cat_id;
    private SponcerModel sponcerModel;
    private EditText search_by_keyword;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponsorDetail sponsor_detail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.search_sponcers, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        driver_id = bundle.getInt("driver_id");
        initializeObjects();
        get_news();
    }

    private void get_news() {
        parseProfile();

        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initializeObjects() {
        search_sponcer = (TextView) rootview.findViewById(R.id.search_sponcer);
        search = (TextView) rootview.findViewById(R.id.search);
        sponcerList = (GridView) rootview.findViewById(R.id.sponcer);
        search.setOnClickListener(this);
        search_sponcer.setOnClickListener(this);
        save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("SHORT TRACK HERO");
        ImageView searchimg = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        searchimg.setVisibility(View.GONE);
        search_by_keyword = (EditText) rootview.findViewById(R.id.search_sponcer_by_keyword);
        search_by_keyword.setVisibility(View.GONE);
        news = (TextView) rootview.findViewById(R.id.news);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.search:
                execute_sponcerlist();
                break;
            case R.id.search_sponcer:
                boolean result = validateall(search.getText().toString());
                if (result == true) {
                    sponcerList.setAdapter(null);
                    execute_searchSponcer();
                }
                break;
        }
    }

    private void execute_sponcerlist() {
        try {
            search_by_keyword.setVisibility(View.GONE);
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.DRIVER_SEARCH_SPONCER_CATEGORY,
                            mPostArrayList, Search_Sponcers.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.SEARCH_SPONCER_CATEGORY,
                            mPostArrayList, Search_Sponcers.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_searchSponcer() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.cat_id, cat_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.keyword, search_by_keyword.getText().toString() + "", ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.DRIVER_SPONCER_LIST,
                            mPostArrayList, Search_Sponcers.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.SPONCER_LIST,
                            mPostArrayList, Search_Sponcers.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private boolean validateall(String sponcer) {
        boolean validate = true;
        try {
            if (sponcer.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please select Sponsor", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.get_profile);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new GetProfile_Model();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();

            }
            if (status == 0) {
                AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");

            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_SPONCER_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.sponcer_list);
            search_by_keyword.setText("");
            parse_search_Sponcer_list();
        } else if (apiName.equals(ServerRequestConstants.SPONCER_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.sponcer_list);
            search_by_keyword.setText("");
            parse_search_Sponcer_list();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_SEARCH_SPONCER_CATEGORY)) {
            AppDelegate.save(getActivity(), result, Parameters.search_sponcer_category);
            parseSponcer_list();
        } else if (apiName.equals(ServerRequestConstants.SEARCH_SPONCER_CATEGORY)) {
            AppDelegate.save(getActivity(), result, Parameters.search_sponcer_category);
            parseSponcer_list();
        } else if (apiName.equals(ServerRequestConstants.SPONCER_DETAIL_DRIVER)) {
            parseSponcer_detail(result);
        } else if (apiName.equals(ServerRequestConstants.SPONCER_DETAIL)) {
            parseSponcer_detail(result);
        } else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        }
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
                //news.setText(newsModel.getDescription() + "");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseSponcer_detail(String result) {
        Bundle bundle = new Bundle();
        try {
            ArrayList<SponsorDetail> sponsorDetails=new ArrayList<>();
            JSONObject obj = new JSONObject(result);
            sponsor_detail = new SponsorDetail();
            int status = (obj.getInt("status"));
            if (status == 1 && (obj.getInt("dataFlow")==1)) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    sponsor_detail = new SponsorDetail();
                    sponsor_detail.setImage(jsonObject.getString("image"));
                    sponsor_detail.setAddress(jsonObject.getString("address"));
                    sponsor_detail.setCompany_name(jsonObject.getString("name"));
                    sponsor_detail.setPhone(jsonObject.getString("phone_number"));
                    sponsorDetails.add(sponsor_detail);
                }
                bundle.putInt(Tags.sponsor_id,0);
                bundle.putParcelable(Tags.sponsor_detail,sponsor_detail);
                AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        title = (TextView) dialog.findViewById(R.id.dialog_title);
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    private void setsponcer(final ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList) {
        Dialogue();
        CategoryAdapter spinnerAdapter = new CategoryAdapter(getActivity(), sponcerCategoryModelArrayList);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = sponcerCategoryModelArrayList.get(position).getName();
                search.setText(timee);
                cat_id = sponcerCategoryModelArrayList.get(position).getCategory_id();
                dialog.dismiss();
            }
        });
        title.setText("Select Sponsor Category ");
        search_by_keyword.setVisibility(View.VISIBLE);
        dialog.show();
    }

    private void parse_search_Sponcer_list() {
        sponcerList.setAdapter(null);
        String sponcercategory = AppDelegate.getValue(getActivity(), Parameters.sponcer_list);
        try {
            ArrayList<SponcerModel> sponcerModelArrayList = new ArrayList<>();
            JSONObject obj = new JSONObject(sponcercategory);
            sponcerModel = new SponcerModel();
            int status = (obj.getInt("status"));
            if (status == 1) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    sponcerModel = new SponcerModel();
                    sponcerModel.setSponcer_img(jsonObject.getString("image"));
                    sponcerModel.setSponcer_name(jsonObject.getString("name"));
                    sponcerModel.setSponcer_id(jsonObject.getInt("id"));
                    sponcerModelArrayList.add(sponcerModel);
                }
                setsponcermodel(sponcerModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setsponcermodel(final ArrayList<SponcerModel> sponcerModelArrayList) {
        AppDelegate.Log("sponcer", sponcerModelArrayList + "");

        SponcerAdapter sponcerAdapter = new SponcerAdapter(getActivity(), sponcerModelArrayList);
        sponcerList.setAdapter(sponcerAdapter);
        sponcerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sponcerModelArrayList.get(position).getSponcer_id();
                execute_sponcerDetail(sponcerModelArrayList.get(position).getSponcer_id());
            }
        });
    }

    private void execute_sponcerDetail(int sponcer_id) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.sponsor_id, sponcer_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.SPONCER_DETAIL_DRIVER,
                            mPostArrayList, Search_Sponcers.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), Search_Sponcers.this, ServerRequestConstants.SPONCER_DETAIL,
                            mPostArrayList, Search_Sponcers.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseSponcer_list() {
        String sponcercategory = AppDelegate.getValue(getActivity(), Parameters.search_sponcer_category);
        try {
            ArrayList<SponcerCategoryModel> sponcerCategoryModelArrayList = new ArrayList<>();
            JSONObject obj = new JSONObject(sponcercategory);
            categoryModel = new SponcerCategoryModel();
            categoryModel.setStatus(obj.getInt("status"));
            if (categoryModel.getStatus() == 1) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    categoryModel = new SponcerCategoryModel();
                    categoryModel.setCategory_id(jsonObject.getInt("id"));
                    categoryModel.setName(jsonObject.getString("name"));
                    sponcerCategoryModelArrayList.add(categoryModel);
                }
                setsponcer(sponcerCategoryModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
