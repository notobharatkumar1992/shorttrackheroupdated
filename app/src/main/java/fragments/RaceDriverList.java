package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import Constants.Tags;
import adapters.DriverAdapter;
import adapters.DriverListAdapter;
import model.Driver;
import model.RaceDetailModel;

/**
 * Created by admin on 30-06-2016.
 */
public class RaceDriverList extends Fragment {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel=new RaceDetailModel();

    private ArrayList<Driver> driverModel=new ArrayList<>();
    private RecyclerView driverlist;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.driver, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        driverModel = bundle.getParcelableArrayList(Tags.DriverList);
        initializeObjects();
        AppDelegate.LogT("driver model for race =>"+driverModel);
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        //txttitle.setText("Short Track Hero");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        driverlist = (RecyclerView) rootview.findViewById(R.id.driverslist);
        if(driverModel!=null) {
            AppDelegate.Log("RACE", driverModel + "");
            if (driverModel.size() > 0) {
                LinearLayoutManager layoutManager
                        = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                DriverAdapter adapter = new DriverAdapter(getActivity(), driverModel);
                driverlist.setLayoutManager(layoutManager);
                driverlist.setAdapter(adapter);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No details Available.", "Alert!!!");
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
