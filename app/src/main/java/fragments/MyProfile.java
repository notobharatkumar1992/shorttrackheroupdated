package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.Login;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;

import Async.LoadImage;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.CityAdapter;
import adapters.CountryAdapter;
import adapters.StateAdapter;
import adapters.TrackAdapter;
import interfaces.OnReciveServerResponse;
import model.CityListModel;
import model.CountryListModel;
import model.GetProfile_Model;
import model.PostAysnc_Model;
import model.StateListModel;
import model.TrackListModel;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;


/**
 * Created by admin on 16-06-2016.
 */
public class MyProfile extends Fragment implements OnReciveServerResponse, View.OnClickListener {
    View rootview;
    CircleImageView uploadImg;
    EditText fullname, city, contact_no, zipcode, password, confirm_password;
    TextView country, state, track, signup, email_address;
    CheckBox subscribe;
    Intent i;
    Dialog dialog;
    private static int RESULT_LOAD_IMAGE = 1;
    private Uri selectedImage;

    private TrackListModel tracklistitem;
    private TextView title;
    private ListView list;
    private CountryListModel countrylistitem;
    private int country_id;
    private StateListModel statelistitem;
    private int state_id;
    private CityListModel citylistitem;
    private String device_id;

    private Bitmap OriginalPhoto;
    //    private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private String city_id;
    private String message;
    private int dataFlow, status;
    private TextView save;
    private LinearLayout profile_detail;
    private TextView profile, change_password, likes;
    private String identifier;
    private int track_id;
    private int id;
    private GetProfile_Model getProfile_model;
    private Bitmap bitmap;
    String username, email_id, contact, postcode;
    private int user_id;
    private FragmentManager fragmentManager;
    private ImageView search;
    private EditText car_no, fb_link, tw_link, insta_link, about_me, achievements;
    private String car_num, fb_lin, tw_lin, insta_lin, about_m, achievement;
    private ImageView userimg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            rootview = inflater.inflate(R.layout.my_profile_driver, container, false);
        } else {
            rootview = inflater.inflate(R.layout.my_profile, container, false);
        }

        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    void reset() {
        fullname.setText("");
        contact_no.setText("");
        country.setText("");
        state.setText("");
        city.setText("");
        zipcode.setText("");
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            car_no.setText("");
            fb_link.setText("");
            tw_link.setText("");
            insta_link.setText("");
            about_me.setText("");
            achievements.setText("");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        save.setVisibility(View.VISIBLE);
        country.setOnClickListener(this);
        state.setOnClickListener(this);
        change_password.setOnClickListener(this);
        save.setOnClickListener(this);
        uploadImg.setOnClickListener(this);
        city.setOnClickListener(this);
        device_id = AppDelegate.getUUID(getActivity());
        getprofile();
    }

    private void setvalues() {
        String login = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        Log.e("login value", login + "");
        try {
            JSONObject json = new JSONObject(login);
            int status = json.getInt("status");
            if (status == 1) {
                JSONObject jsonObject = json.getJSONObject("response");
                id = jsonObject.getInt("id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getprofile() {
        setvalues();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.DRIVER_GET_PROFILE,
                            mPostArrayList, MyProfile.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.GET_PROFILE,
                            mPostArrayList, MyProfile.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Edit Profile");
        uploadImg = (CircleImageView) rootview.findViewById(R.id.uploadimg);
        change_password = (TextView) rootview.findViewById(R.id.change_password);

        fullname = (EditText) rootview.findViewById(R.id.full_name);
        email_address = (TextView) rootview.findViewById(R.id.email_address);
        contact_no = (EditText) rootview.findViewById(R.id.contact_no);
        zipcode = (EditText) rootview.findViewById(R.id.zip_code);
        country = (TextView) rootview.findViewById(R.id.country);
        state = (TextView) rootview.findViewById(R.id.state);
        save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        city = (EditText) rootview.findViewById(R.id.city);

        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {

            car_no = (EditText) rootview.findViewById(R.id.car_no);
            fb_link = (EditText) rootview.findViewById(R.id.fb_link);
            tw_link = (EditText) rootview.findViewById(R.id.tw_link);
            insta_link = (EditText) rootview.findViewById(R.id.insta_link);
            about_me = (EditText) rootview.findViewById(R.id.about_me);
            achievements = (EditText) rootview.findViewById(R.id.achievements);

        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.uploadimg:
                showImageSelectorList();
                break;
            case R.id.change_password:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Fragment f = new ChangePassword();
                        fragmentManager = getActivity().getSupportFragmentManager();
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    }
                }, 400);
                break;

            case R.id.select_track:

                break;
            case R.id.country:
                execute_Countrylist();
                state.setText("");
                city.setText("");
                break;
            case R.id.state:
                if (country.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select Country first", "Alert!!!");
                } else {
                    execute_Statelist();
                    city.setText("");
                }
                break;
            case R.id.city:
                if (state.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select state first", "Alert!!!");

                } else {

                }
                break;
            case R.id.save:
                String name, email, contact, password, confirmpass, country, state, city, zipcode, track;
                name = fullname.getText().toString();

                contact = contact_no.getText().toString();

                country = this.country.getText().toString();
                state = this.state.getText().toString();
                city = this.city.getText().toString();
                zipcode = this.zipcode.getText().toString();

                boolean valid;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    valid = validate(country, state, city, zipcode, fb_link.getText().toString(), tw_link.getText().toString(), insta_link.getText().toString(), about_me.getText().toString(), achievements.getText().toString(), name, contact);
                    if (valid == true) {
                        execute_update();
                    }
                } else {
                    valid = validateall(country, state, city, zipcode, name, contact);
                    if (valid == true) {
                        execute_update();
                    }
                }


                break;
        }
    }

    private boolean validate(String country, String state, String city, String zipcode, String fb_link, String tw_link, String insta_link, String about_me, String achievements, String name, String contact) {
        boolean validate = true;
        try {
            if (country.isEmpty() && state.isEmpty() && city.isEmpty() && zipcode.isEmpty() && name.isEmpty() && contact.isEmpty() && fb_link.isEmpty() && tw_link.isEmpty() && insta_link.isEmpty() && about_me.isEmpty() && achievements.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please fill the fields", "Alert!!!");
            } else if (name.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Name", "Alert!!!");
            } else if (contact.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Contact no.", "Alert!!!");
            } else if (country.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter country", "Alert!!!");
            } else if (state.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter state", "Alert!!!");
            } else if (city.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Please Enter city", "Alert!!!");
            } else if (zipcode.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter zip-code", "Alert");
            } else if (fb_link.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Facebook Link", "Alert!!!");
            } else if (tw_link.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Twitter Link", "Alert!!!");
            } else if (insta_link.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Instagram Link", "Alert!!!");
            } else if (about_me.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Please About me", "Alert!!!");
            } else if (achievements.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Achievements", "Alert");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }


    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(getActivity());
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = getActivity()
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        uploadImg.setImageBitmap(OriginalPhoto);
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    uploadImg.setImageBitmap(OriginalPhoto);
                                }

                            } else {
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                        uploadImg.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));
                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "cropuser");
            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");
            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean validateall(String country, String state, String city, String zipcode, String fullname, String contact_no) {
        boolean validate = true;
        try {
            if (country.isEmpty() && state.isEmpty() && city.isEmpty() && zipcode.isEmpty() && fullname.isEmpty() && contact_no.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please fill the fields", "Alert!!!");
            } else if (fullname.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Name", "Alert!!!");
            } else if (contact_no.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter Contact no.", "Alert!!!");
            } else if (country.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter country", "Alert!!!");
            } else if (state.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter state", "Alert!!!");
            } else if (city.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Please Enter city", "Alert!!!");
            } else if (zipcode.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Enter zip-code", "Alert");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }

    private void execute_update() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                if (OriginalPhoto != null) {
                    if (capturedFile == null)
                        capturedFile = new File(getNewFile());
                    FileOutputStream fOut = null;
                    try {
                        fOut = new FileOutputStream(capturedFile);
                    } catch (FileNotFoundException e) {
                        AppDelegate.LogE(e);
                    }
                    OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                    try {
                        fOut.flush();
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        fOut.close();
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, capturedFile, ServerRequestConstants.Key_PostFileValue);

                }
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.username, fullname.getText().toString() + "");
                // AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.email, email_address.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.mobile, Long.parseLong(contact_no.getText().toString()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.post_code, Long.parseLong(zipcode.getText().toString()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.state_id, state_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.city_id, city_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.car_number, car_no.getText().toString() + "");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.fb_link, fb_link.getText().toString() + "");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.tw_link, tw_link.getText().toString() + "");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.insta_link, insta_link.getText().toString() + "");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.about_me, about_me.getText().toString() + "");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.achievements, achievements.getText().toString() + "");
                    mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.EDIT_PROFILE_DRIVER,
                            mPostArrayList, MyProfile.this);
                } else {
                  /*  track.setVisibility(View.GONE);*/
                    mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.EDIT_PROFILE_FAN,
                            mPostArrayList, MyProfile.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_Statelist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.GET_STATELIST,
                        mPostArrayList, MyProfile.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_Countrylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.GET_COUNTRY_LIST,
                        mPostArrayList, MyProfile.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Time out!!!Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TRACKLIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savetracklist);
            parseTracklist();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_TRACKLIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savetracklist);
            parseTracklist();
        } else if (apiName.equals(ServerRequestConstants.GET_COUNTRY_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savecountrylist);
            Log.e("country", result + "");
            parseCountryList();
        } else if (apiName.equals(ServerRequestConstants.GET_STATELIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savestatelist);
            Log.e("state", result + "");
            parseStatelist();
        } else if (apiName.equals(ServerRequestConstants.EDIT_PROFILE_FAN)) {
            AppDelegate.save(getActivity(), result, Parameters.editprofilefan);
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            Log.e("register", result + "");
            parseRegister();
        } else if (apiName.equals(ServerRequestConstants.EDIT_PROFILE_DRIVER)) {
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            AppDelegate.save(getActivity(), result, Parameters.editprofilefan);
            Log.e("register", result + "");
            parseRegister();
        } else if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_PROFILE)) {
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        }
    }

    public class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if (image != null) {
                uploadImg.setImageBitmap(image);
            } else {

            }
        }
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.get_profile);
        AppDelegate.Log("response", Register + "");
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                status = jsonObject.getInt("status");
                dataFlow = jsonObject.getInt("dataFlow");
                message = jsonObject.getString("message");
                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    getProfile_model.setId(response.getInt("id"));
                    getProfile_model.setUsername(response.getString("username"));

                    getProfile_model.setEmail(response.getString("email"));
                    getProfile_model.setCountry_id(response.getInt("country_id"));
                    getProfile_model.setState_id(response.getInt("state_id"));
                    getProfile_model.setCity_id(response.getString("city_id"));
                    getProfile_model.setPost_code(response.getString("post_code"));
                    getProfile_model.setMobile(response.getString("mobile"));


                    getProfile_model.setCar_number(response.getString("car_number"));
                    try {
                        getProfile_model.setFb_link(response.getString("fb_link"));
                        getProfile_model.setTw_link(response.getString("tw_link"));
                        getProfile_model.setInsta_link(response.getString("insta_link"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        JSONObject fanresponse = response.getJSONObject("driver_detail");
                        getProfile_model.setUser_image(response.getString("user_image"));
                        getProfile_model.setCountry(response.getString("country"));
                        getProfile_model.setState(response.getString("state"));
                        //  getProfile_model.setLogin_type(response.getInt("login_type"));
                        getProfile_model.setCity_name(response.getString("city_name"));
                        try {
                            getProfile_model.setView_achievement(fanresponse.getString("view_achievement"));
                            getProfile_model.setAbout_me(fanresponse.getString("about_me"));
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                        getProfile_model.setDriver_id(fanresponse.getInt("driver_id"));
                        getProfile_model.setTrack_id(fanresponse.getInt("track_id"));
                        getProfile_model.setTotal_credit(fanresponse.getInt("total_credit"));
                        getProfile_model.setTotal_point(fanresponse.getInt("total_point"));
                        getProfile_model.setTotal_win(fanresponse.getString("total_win"));
                        getProfile_model.setTotal_race(fanresponse.getInt("total_race"));

                        getProfile_model.setCurrent_rank(fanresponse.getString("current_rank"));
                        getProfile_model.setSponcer_list(fanresponse.getString("sponcer_list"));
                        getProfile_model.setHeight(fanresponse.getDouble("height"));
                        getProfile_model.setWeight(fanresponse.getDouble("weight"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        getProfile_model.setRefer_code(response.getString("refer_code"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    new LoadImage().execute(getProfile_model.getUser_image());
                   /* AppDelegate.getInstance(getActivity()).getImageLoader().get(getProfile_model.getUser_image(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AppDelegate.LogE(error);
                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null)
                                uploadImg.setImageBitmap(response.getBitmap());
                            OriginalPhoto = response.getBitmap();
                            //capturedFile = bitmapToFile(OriginalPhoto);
                        }
                    });*/
                    fullname.setText(getProfile_model.getUsername() + "");
                    email_address.setText(getProfile_model.getEmail());
                    contact_no.setText(getProfile_model.getMobile());
                    country.setText(getProfile_model.getCountry());
                    state.setText(getProfile_model.getState());
                    city.setText(getProfile_model.getCity_name());
                    zipcode.setText(getProfile_model.getPost_code());
                    car_no.setText(getProfile_model.getCar_number() + "");
                    fb_link.setText(getProfile_model.getFb_link() + "");
                    tw_link.setText(getProfile_model.getTw_link() + "");
                    insta_link.setText(getProfile_model.getInsta_link() + "");

                    username = getProfile_model.getUsername();
                    email_id = getProfile_model.getEmail();
                    contact = getProfile_model.getMobile();
                    postcode = getProfile_model.getPost_code();
                    country_id = getProfile_model.getCountry_id();
                    state_id = getProfile_model.getState_id();
                    city_id = getProfile_model.getCity_id();
                    user_id = getProfile_model.getId();
                    car_num = getProfile_model.getCar_number() + "";
                    fb_lin = getProfile_model.getFb_link() + "";
                    tw_lin = getProfile_model.getTw_link() + "";
                    insta_lin = getProfile_model.getInsta_link() + "";
                    if (!getProfile_model.getAbout_me().equals("null")) {
                        about_me.setText(getProfile_model.getAbout_me() + "");
                        about_m = getProfile_model.getAbout_me() + "";
                    }
                    if (!getProfile_model.getView_achievement().equals("null")) {
                        achievements.setText(getProfile_model.getView_achievement() + "");
                        achievement = getProfile_model.getView_achievement() + "";
                    }

                    // capturedFile=getProfile_model.getUser_image();
                }
                if (status == 0) {
                    AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");
                    reset();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                status = jsonObject.getInt("status");
                dataFlow = jsonObject.getInt("dataFlow");
                message = jsonObject.getString("message");
                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    getProfile_model.setId(response.getInt("id"));
                    getProfile_model.setUsername(response.getString("username"));
                    getProfile_model.setRole_id(response.getInt("role_id"));
                    getProfile_model.setAdmin_id(response.getInt("admin_id"));
                    getProfile_model.setEmail(response.getString("email"));
                    getProfile_model.setCountry_id(response.getInt("country_id"));
                    getProfile_model.setState_id(response.getInt("state_id"));
                    getProfile_model.setCity_id(response.getString("city_id"));
                    getProfile_model.setPost_code(response.getString("post_code"));
                    getProfile_model.setMobile(response.getString("mobile"));
                    getProfile_model.setUser_image(response.getString("user_image"));
                    getProfile_model.setCountry(response.getString("country"));
                    getProfile_model.setState(response.getString("state"));
                    getProfile_model.setLogin_type(response.getInt("login_type"));
                    getProfile_model.setCity_name(response.getString("city_name"));
                    try {
                        JSONObject fanresponse = response.getJSONObject("fan_detail");
                        getProfile_model.setTrack_id(fanresponse.getInt("track_id"));
                        getProfile_model.setTotal_credit(fanresponse.getInt("total_credit"));
                        getProfile_model.setTotal_point(fanresponse.getInt("total_point"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        getProfile_model.setRefer_code(response.getString("refer_code"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    new LoadImage().execute(getProfile_model.getUser_image());
                    /*AppDelegate.getInstance(getActivity()).getImageLoader().get(getProfile_model.getUser_image(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AppDelegate.LogE(error);
                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null)
                                uploadImg.setImageBitmap(response.getBitmap());
                            OriginalPhoto = response.getBitmap();
                            //capturedFile = bitmapToFile(OriginalPhoto);
                        }
                    });*/
                    fullname.setText(getProfile_model.getUsername() + "");
                    email_address.setText(getProfile_model.getEmail());
                    contact_no.setText(getProfile_model.getMobile());
                    country.setText(getProfile_model.getCountry());
                    state.setText(getProfile_model.getState());
                    city.setText(getProfile_model.getCity_name());
                    zipcode.setText(getProfile_model.getPost_code());
                    username = getProfile_model.getUsername();
                    email_id = getProfile_model.getEmail();
                    contact = getProfile_model.getMobile();
                    postcode = getProfile_model.getPost_code();
                    country_id = getProfile_model.getCountry_id();
                    state_id = getProfile_model.getState_id();
                    city_id = getProfile_model.getCity_id();
                    user_id = getProfile_model.getId();
                    // capturedFile=getProfile_model.getUser_image();
                }
                if (status == 0) {
                    AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");
                    reset();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void parseRegister() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.editprofilefan);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            dataFlow = jsonObject.getInt("dataFlow");
            message = jsonObject.getString("message");
            if (status == 1) {
                if (OriginalPhoto != null) {
                    ((HomeScreen) getActivity()).parseProfile();
                    userimg = (ImageView) getActivity().findViewById(R.id.user_img);
                    userimg.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));
                }
                TextView  username = (TextView)getActivity(). findViewById(R.id.username);
                JSONObject jsonobj = jsonObject.getJSONObject("response");
                AppDelegate.save(getActivity(),jsonobj.getString("username"),"username");
                username.setText(jsonobj.getString("username")+"");
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    AppDelegate.save(getActivity(),jsonobj.getString("image"),"user_image");
                } else {
                    AppDelegate.save(getActivity(),jsonobj.getString("user_image"),"user_image");
                }

                AppDelegate.ShowDialog(getActivity(), "Update Successfully.", "Alert!!!");
            }
            if (status == 0) {
                AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");
                reset();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseStatelist() {
        String Statelist = AppDelegate.getValue(getActivity(), Parameters.savestatelist);
        try {
            JSONObject jsonObject = new JSONObject(Statelist);
            ArrayList<StateListModel> stateListModels = new ArrayList<StateListModel>();
            statelistitem = new StateListModel();
            statelistitem.setStatus(jsonObject.getInt("status"));
            statelistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (statelistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    statelistitem = new StateListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    statelistitem.setId(obj.getInt("id"));
                    statelistitem.setRegion_name(obj.getString("name"));
                    stateListModels.add(statelistitem);
                }
                stateset(stateListModels);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void stateset(final ArrayList<StateListModel> stateListModels) {
        Dialogue();
        StateAdapter spinnerAdapter = new StateAdapter(getActivity(), stateListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = stateListModels.get(position).getRegion_name();
                state.setText(timee);
                state_id = stateListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select State ");
        dialog.show();
    }

    private void parseCountryList() {
        String Countrylist = AppDelegate.getValue(getActivity(), Parameters.savecountrylist);
        try {
            JSONObject jsonObject = new JSONObject(Countrylist);
            ArrayList<CountryListModel> countryListModels = new ArrayList<CountryListModel>();
            countrylistitem = new CountryListModel();
            countrylistitem.setStatus(jsonObject.getInt("status"));
            countrylistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (countrylistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    countrylistitem = new CountryListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    countrylistitem.setId(obj.getInt("id"));
                    countrylistitem.setName(obj.getString("name"));
                    countrylistitem.setCode(obj.getString("code"));
                    countryListModels.add(countrylistitem);
                }
                countryset(countryListModels);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void countryset(final ArrayList<CountryListModel> countryListModels) {
        Dialogue();
        CountryAdapter spinnerAdapter = new CountryAdapter(getActivity(), countryListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = countryListModels.get(position).getName();
                country.setText(timee);
                country_id = countryListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select Country ");
        dialog.show();
    }

    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        title = (TextView) dialog.findViewById(R.id.dialog_title);
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    private void parseTracklist() {
        String tracklist = AppDelegate.getValue(getActivity(), Parameters.savetracklist);
        try {
            JSONObject jsonObject = new JSONObject(tracklist);
            ArrayList<TrackListModel> trackListModels = new ArrayList<TrackListModel>();
            tracklistitem = new TrackListModel();

            tracklistitem.setStatus(jsonObject.getInt("status"));
            tracklistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (tracklistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    tracklistitem = new TrackListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    tracklistitem.setId(obj.getInt("id"));
                    tracklistitem.setCompany_name(obj.getString("company_name"));
                    tracklistitem.setPayment_method_id(obj.getInt("payment_method_id"));
                    tracklistitem.setCreated(obj.getString("created"));
                    tracklistitem.setUser_id(obj.getInt("id"));
                    trackListModels.add(tracklistitem);
                }
                trackset(trackListModels);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No tracklist found.", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void trackset(final ArrayList<TrackListModel> trackListModels) {
        Dialogue();
        TrackAdapter spinnerAdapter = new TrackAdapter(getActivity(), trackListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = trackListModels.get(position).getCompany_name();
                track.setText(timee);
                track_id = trackListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select track ");
        dialog.show();
    }

    private void execute_TRACKLIST() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
            PostAsync mPostAsyncObj;
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.DRIVER_TRACKLIST,
                        mPostArrayList, MyProfile.this);
            } else {
                mPostAsyncObj = new PostAsync(getActivity(), MyProfile.this, ServerRequestConstants.TRACKLIST,
                        mPostArrayList, MyProfile.this);
            }
            AppDelegate.showProgressDialog(getActivity());
            mPostAsyncObj.execute();
        }
    }
}
