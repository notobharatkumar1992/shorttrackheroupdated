package fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.CityAdapter;
import adapters.CountryAdapter;
import adapters.ImageAdapter;
import adapters.StateAdapter;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.CityListModel;
import model.CountryListModel;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.GetProfile_Model;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;
import model.StateListModel;

/**
 * Created by admin on 30-06-2016.
 */
public class CheckOut extends Fragment implements OnReciveServerResponse, OnListItemClickListener, View.OnClickListener {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    String message;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private ProductListModel product = new ProductListModel();
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy, email_address, country, state, quantity;
    private int id;
    private EditText fullname, contact_no, zipcode,city;
    private CountryListModel countrylistitem;
    private ListView list;
    private int country_id;
    Dialog dialog;
    private TextView title;
    private GetProfile_Model getProfile_model;
    private String city_id;
    CityListModel citylistitem;
    private int state_id;
    StateListModel statelistitem;
    private CircleImageView image;
    private TextView chk_out;
    private EditText address;
    private String identifier;
    private TextView promo_code;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.buy_item, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AdView adView = (AdView) rootview.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        initializeObjects();
    }
    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Shop");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        bundle = getArguments();
        product = new ProductListModel();
        product = bundle.getParcelable(Tags.parcel_ProductDetail);
        image = (CircleImageView) rootview.findViewById(R.id.image);
        name = (TextView) rootview.findViewById(R.id.name);
        price = (TextView) rootview.findViewById(R.id.price);
        fullname = (EditText) rootview.findViewById(R.id.full_name);
        quantity = (EditText) rootview.findViewById(R.id.quantity);
        email_address = (TextView) rootview.findViewById(R.id.email_address);
        contact_no = (EditText) rootview.findViewById(R.id.contact_no);
        zipcode = (EditText) rootview.findViewById(R.id.zip_code);
        country = (TextView) rootview.findViewById(R.id.country);
        state = (TextView) rootview.findViewById(R.id.state);
        chk_out = (TextView) rootview.findViewById(R.id.chk_out);
        promo_code = (TextView) rootview.findViewById(R.id.promo_code);
        promo_code.setVisibility(View.GONE);
        address = (EditText) rootview.findViewById(R.id.address);
        name.setText(product.getProduct_name() + "");
        price.setText("Credit :" + String.valueOf(product.getPrice()));
        AppDelegate.getInstance(getActivity()).getImageLoader().get(product.getImage1(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null)
                    image.setImageBitmap(response.getBitmap());
            }
        });
      /*  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);*/
        city = (EditText) rootview.findViewById(R.id.city);
        country.setOnClickListener(this);
        state.setOnClickListener(this);

        chk_out.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getprofile();
    }

    private void setvalues() {
        String login = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        Log.e("login value", login + "");
        try {
            JSONObject json = new JSONObject(login);
            int status = json.getInt("status");
            if (status == 1) {
                JSONObject jsonObject = json.getJSONObject("response");
                id = jsonObject.getInt("id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getprofile() {
        setvalues();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.DRIVER_GET_PROFILE,
                            mPostArrayList, CheckOut.this);
                }else{
                    mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.GET_PROFILE,
                            mPostArrayList, CheckOut.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_Statelist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.GET_STATELIST,
                        mPostArrayList, CheckOut.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }



    private void parseCountryList() {
        String Countrylist = AppDelegate.getValue(getActivity(), Parameters.savecountrylist);
        try {
            JSONObject jsonObject = new JSONObject(Countrylist);
            ArrayList<CountryListModel> countryListModels = new ArrayList<CountryListModel>();
            countrylistitem = new CountryListModel();
            countrylistitem.setStatus(jsonObject.getInt("status"));
            countrylistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (countrylistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    countrylistitem = new CountryListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    countrylistitem.setId(obj.getInt("id"));
                    countrylistitem.setName(obj.getString("name"));
                    countrylistitem.setCode(obj.getString("code"));
                    countryListModels.add(countrylistitem);
                }
                countryset(countryListModels);
            }
            else{
                AppDelegate.ShowDialog(getActivity(),"No record found.","Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void countryset(final ArrayList<CountryListModel> countryListModels) {
        Dialogue();
        CountryAdapter spinnerAdapter = new CountryAdapter(getActivity(), countryListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = countryListModels.get(position).getName();
                country.setText(timee);
                country_id = countryListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select Country ");
        dialog.show();
    }

    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        title = (TextView) dialog.findViewById(R.id.dialog_title);
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(),"Service Time out!!!","Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.PRODUCT_DETAIL)) {
            AppDelegate.save(getActivity(), result, Parameters.product_detail);
            parseProduct_detail();
        }
        if (apiName.equals(ServerRequestConstants.DRIVER_PRODUCT_DETAIL)) {
            AppDelegate.save(getActivity(), result, Parameters.product_detail);
            parseProduct_detail();
        }else if (apiName.equals(ServerRequestConstants.GET_COUNTRY_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savecountrylist);
            Log.e("country", result + "");
            parseCountryList();
        } else if (apiName.equals(ServerRequestConstants.GET_STATELIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savestatelist);
            Log.e("state", result + "");
            parseStatelist();
        } else if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        }
        else if (apiName.equals(ServerRequestConstants.DRIVER_GET_PROFILE)) {
            AppDelegate.save(getActivity(), result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        }else if (apiName.equals(ServerRequestConstants.PURCHASE_PRODUCT)) {
            AppDelegate.save(getActivity(), result, Parameters.purchase_product);
            Log.e("purchase_product", result + "");
            parse_purchase();
        }else if (apiName.equals(ServerRequestConstants.DRIVER_PURCHASE_PRODUCT)) {
            AppDelegate.save(getActivity(), result, Parameters.purchase_product);
            Log.e("purchase_product", result + "");
            parse_purchase();
        }
    }

    private void parse_purchase() {
        String purchase = AppDelegate.getValue(getActivity(), Parameters.purchase_product);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(purchase);
            status = jsonObject.getInt("status");
            if(status==1){
                bundle=new Bundle();
                message = jsonObject.getString("response");
                bundle.putString("message",message);
                AppDelegate.showFragment(getActivity(), new ThankYou(), R.id.framelayout, bundle, null);
            }
            else {
                message = jsonObject.getString("message");

                    AppDelegate.ShowDialog(getActivity(),message+"","Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.get_profile);
        AppDelegate.Log("response", Register + "");
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                status = jsonObject.getInt("status");
              int  dataFlow = jsonObject.getInt("dataFlow");
                message = jsonObject.getString("message");
                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    getProfile_model.setId(response.getInt("id"));
                    getProfile_model.setUsername(response.getString("username"));

                    getProfile_model.setEmail(response.getString("email"));
                    getProfile_model.setCountry_id(response.getInt("country_id"));
                    getProfile_model.setState_id(response.getInt("state_id"));
                    getProfile_model.setCity_id(response.getString("city_id"));
                    getProfile_model.setPost_code(response.getString("post_code"));
                    getProfile_model.setMobile(response.getString("mobile"));


                    getProfile_model.setCar_number(response.getString("car_number"));
                    try {
                        getProfile_model.setFb_link(response.getString("fb_link"));
                        getProfile_model.setTw_link(response.getString("tw_link"));
                        getProfile_model.setInsta_link(response.getString("insta_link"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        JSONObject fanresponse = response.getJSONObject("driver_detail");
                        getProfile_model.setUser_image(response.getString("user_image"));
                        getProfile_model.setCountry(response.getString("country"));
                        getProfile_model.setState(response.getString("state"));
                        //  getProfile_model.setLogin_type(response.getInt("login_type"));
                        getProfile_model.setCity_name(response.getString("city_name"));
                        try{
                            getProfile_model.setView_achievement(fanresponse.getString("view_achievement"));
                            getProfile_model.setAbout_me(fanresponse.getString("about_me"));
                        }catch(Exception e){
                            AppDelegate.LogE(e);
                        }
                        getProfile_model.setDriver_id(fanresponse.getInt("driver_id"));
                        getProfile_model.setTrack_id(fanresponse.getInt("track_id"));
                        getProfile_model.setTotal_credit(fanresponse.getInt("total_credit"));
                        getProfile_model.setTotal_point(fanresponse.getInt("total_point"));
                        getProfile_model.setTotal_win(fanresponse.getString("total_win"));
                        getProfile_model.setTotal_race(fanresponse.getInt("total_race"));

                        getProfile_model.setCurrent_rank(fanresponse.getString("current_rank"));
                        getProfile_model.setSponcer_list(fanresponse.getString("sponcer_list"));
                        getProfile_model.setHeight(fanresponse.getDouble("height"));
                        getProfile_model.setWeight(fanresponse.getDouble("weight"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        getProfile_model.setRefer_code(response.getString("refer_code"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    fullname.setText(getProfile_model.getUsername() + "");
                    email_address.setText(getProfile_model.getEmail());
                    contact_no.setText(getProfile_model.getMobile());
                    country.setText(getProfile_model.getCountry());
                    state.setText(getProfile_model.getState());
                    city.setText(getProfile_model.getCity_name());
                    zipcode.setText(getProfile_model.getPost_code());
                    country_id = getProfile_model.getCountry_id();
                    state_id = getProfile_model.getState_id();
                    city_id = getProfile_model.getCity_id();
                    user_id = getProfile_model.getId();

                    // capturedFile=getProfile_model.getUser_image();
                }
                if (status == 0) {
                    AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");

                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                status = jsonObject.getInt("status");
             int    dataFlow = jsonObject.getInt("dataFlow");
                message = jsonObject.getString("message");
                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    getProfile_model.setId(response.getInt("id"));
                    getProfile_model.setUsername(response.getString("username"));
                    getProfile_model.setRole_id(response.getInt("role_id"));
                    getProfile_model.setAdmin_id(response.getInt("admin_id"));
                    getProfile_model.setEmail(response.getString("email"));
                    getProfile_model.setCountry_id(response.getInt("country_id"));
                    getProfile_model.setState_id(response.getInt("state_id"));
                    getProfile_model.setCity_id(response.getString("city_id"));
                    getProfile_model.setPost_code(response.getString("post_code"));
                    getProfile_model.setMobile(response.getString("mobile"));
                    getProfile_model.setUser_image(response.getString("user_image"));
                    getProfile_model.setCountry(response.getString("country"));
                    getProfile_model.setState(response.getString("state"));
                    getProfile_model.setLogin_type(response.getInt("login_type"));
                    getProfile_model.setCity_name(response.getString("city_name"));
                    try {
                        JSONObject fanresponse = response.getJSONObject("fan_detail");
                        getProfile_model.setTrack_id(fanresponse.getInt("track_id"));
                        getProfile_model.setTotal_credit(fanresponse.getInt("total_credit"));
                        getProfile_model.setTotal_point(fanresponse.getInt("total_point"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    try {
                        getProfile_model.setRefer_code(response.getString("refer_code"));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    fullname.setText(getProfile_model.getUsername() + "");
                    email_address.setText(getProfile_model.getEmail());
                    contact_no.setText(getProfile_model.getMobile());
                    country.setText(getProfile_model.getCountry());
                    state.setText(getProfile_model.getState());
                    city.setText(getProfile_model.getCity_name());
                    zipcode.setText(getProfile_model.getPost_code());
                    country_id = getProfile_model.getCountry_id();
                    state_id = getProfile_model.getState_id();
                    city_id = getProfile_model.getCity_id();
                    user_id = getProfile_model.getId();
                    // capturedFile=getProfile_model.getUser_image();
                }
                if (status == 0) {
                    AppDelegate.ShowDialog(getActivity(), "Please Try again!!!", "Alert!!!");

                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void parseProduct_detail() {
        String shop_list = AppDelegate.getValue(getActivity(), Parameters.product_detail);
        try {
            JSONObject obj = new JSONObject(shop_list);
            shoplistModel = new ProductListModel();
            shoplistModel.setStatus(obj.getInt("status"));
            ArrayList<ProductListModel> productListModelArrayList = new ArrayList<>();
            shoplistModel.setDataflow(obj.getInt("dataFlow"));
            if (shoplistModel.getStatus() == 1) {
                JSONArray response = obj.getJSONArray("response");
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    shoplistModel = new ProductListModel();
                    shoplistModel.setProduct_id(jsonObject.getInt("id"));
                    shoplistModel.setProduct_name(jsonObject.getString("name"));
                    shoplistModel.setDescription(jsonObject.getString("description"));
                    shoplistModel.setPrice(jsonObject.getDouble("price"));
                    shoplistModel.setQuantityString(jsonObject.getString("quantity"));
                    shoplistModel.setImage1(jsonObject.getString("image1"));
                    shoplistModel.setImage2(jsonObject.getString("image2"));
                    shoplistModel.setImage3(jsonObject.getString("image3"));
                    shoplistModel.setImage4(jsonObject.getString("image4"));

                    productListModelArrayList.add(shoplistModel);

                }
                setProductList(productListModelArrayList);
                setproductdetails(productListModelArrayList);
            }
            else{
                AppDelegate.ShowDialog(getActivity(),"No record found.","Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setproductdetails(ArrayList<ProductListModel> productListModelArrayList) {
        for (ProductListModel product : productListModelArrayList) {
            name.setText(product.getProduct_name());
            price.setText(String.valueOf(product.getPrice()));
            description.setText(product.getDescription());

        }
    }

    private void setProductList(ArrayList<ProductListModel> productListModelArrayList) {
        slider = new ArrayList<>();
        for (ProductListModel product : productListModelArrayList) {
            slider.add(product.getImage1());
            slider.add(product.getImage2());
            slider.add(product.getImage3());
            slider.add(product.getImage4());
        }
        imageAdapter = new ImageAdapter(slider, CheckOut.this);
        viewpager.setAdapter(imageAdapter);
        setUiPageViewController();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = imageAdapter.getCount();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
        next();
    }

    private void next() {
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }





    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        this.click_type = 0;
    }

    private void execute_Countrylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.GET_COUNTRY_LIST,
                        mPostArrayList, CheckOut.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_Citylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.state_id, state_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.GET_CITYLIST,
                        mPostArrayList, CheckOut.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseStatelist() {
        String Statelist = AppDelegate.getValue(getActivity(), Parameters.savestatelist);
        try {
            JSONObject jsonObject = new JSONObject(Statelist);
            ArrayList<StateListModel> stateListModels = new ArrayList<StateListModel>();
            statelistitem = new StateListModel();
            statelistitem.setStatus(jsonObject.getInt("status"));
            statelistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (statelistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    statelistitem = new StateListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    statelistitem.setId(obj.getInt("id"));
                    statelistitem.setRegion_name(obj.getString("name"));
                    stateListModels.add(statelistitem);
                }
                stateset(stateListModels);
            }
            else{
                AppDelegate.ShowDialog(getActivity(),"No record found.","Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void stateset(final ArrayList<StateListModel> stateListModels) {
        Dialogue();
        StateAdapter spinnerAdapter = new StateAdapter(getActivity(), stateListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = stateListModels.get(position).getRegion_name();
                state.setText(timee);
                state_id = stateListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select State ");
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.country:
                execute_Countrylist();
                state.setText("");
                city.setText("");
                break;
            case R.id.state:
                if (country.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select Country first", "Alert!!!");
                } else {
                    execute_Statelist();
                    city.setText("");
                }
                break;
            case R.id.city:
                if (state.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select state first", "Alert!!!");

                } else {

                }
                break;
            case R.id.chk_out:
                boolean result = validate();
                if (result == true) {
                    checkOut();
                }
                break;
        }
    }

    private void checkOut() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.name, fullname.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.email, email_address.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.contact, Long.parseLong(contact_no.getText().toString()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.post_code, zipcode.getText().toString());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.state_id, state_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.city_id, city_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.address, address.getText().toString());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.quantity, Integer.parseInt(quantity.getText().toString()),ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.product_id, (product.getProduct_id()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.price, (product.getPrice()), ServerRequestConstants.Key_PostDoubleValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.DRIVER_PURCHASE_PRODUCT,
                            mPostArrayList, CheckOut.this);
                }else{
                    mPostAsyncObj = new PostAsync(getActivity(), CheckOut.this, ServerRequestConstants.PURCHASE_PRODUCT,
                            mPostArrayList, CheckOut.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
    private boolean validate() {
        boolean validate = true;
        try {
            /*if (fullname.getText().toString().isEmpty() && email_address.getText().toString().isEmpty() && contact_no.getText().toString().isEmpty() && quantity.getText().toString().isEmpty() && country.getText().toString().isEmpty() && state.getText().toString().isEmpty() && city.getText().toString().isEmpty() && zipcode.getText().toString().isEmpty() && quantity.getText().toString().isEmpty() && address.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please fill the fields", "Alert!!!");
            } else */if (fullname.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter  name", "Alert!!!");
            } else if (email_address.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter Email id", "Alert!!!");
            } else if (!email_address.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Invalid Email Address", "Alert!!!");

            } else if (contact_no.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter Contact no.", "Alert!!!");
            } else if (contact_no.getText().toString().length() < 10) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter  correct Contact no.", "Alert!!!");
            } else if (country.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter country", "Alert!!!");
            } else if (state.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter state", "Alert!!!");
            } else if (city.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter city", "Alert!!!");
            } else if (zipcode.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter zip-code", "Alert");
            } else if (address.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter address", "Alert");
            } else if (quantity.getText().toString().isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter quantity", "Alert!!!");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }

}
