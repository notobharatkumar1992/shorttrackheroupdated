package fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.MyViewPager;
import adapters.FanListAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.RaceDetailModel;
import model.SponcerBannerModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class DriverHistory extends Fragment implements OnReciveServerResponse {
    private View rootview;
    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private Driver driver;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private String identifier;
    private ImageView tag;
    private ImageView search;
    private ViewPager sponcer;
    private HashMap<Integer, ArrayList<SponcerModel>> arrayListHashMap = new HashMap<>();
    ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
    private SponcerBannerModel value = new SponcerBannerModel();
    private List<Fragment> bannerFragment = new ArrayList();
    private PagerAdapter bannerPagerAdapter;
    private Handler mHandler = new Handler();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.race_history_detail, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        driver = bundle.getParcelable(Tags.parcel_FanDetail);
        initializeObjects();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_news() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }

                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSearch();
            }
        });
        news = (TextView) rootview.findViewById(R.id.news);
        fanlist = (RecyclerView) rootview.findViewById(R.id.fan_list);
        driver_img = (ImageView) rootview.findViewById(R.id.driver_img);
        driver_name = (TextView) rootview.findViewById(R.id.driver_name);
        fans = (TextView) rootview.findViewById(R.id.points);
        total_races = (TextView) rootview.findViewById(R.id.total_race);
        total_wins = (TextView) rootview.findViewById(R.id.total_win);
        rank = (TextView) rootview.findViewById(R.id.rank);
        sponcer = (ViewPager) rootview.findViewById(R.id.banner_list);
        tag = (ImageView) rootview.findViewById(R.id.tag);
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            tag.setVisibility(View.GONE);
            rank.setVisibility(View.GONE);
        } else {
            tag.setVisibility(View.VISIBLE);
            rank.setVisibility(View.VISIBLE);
        }
    }

    private void callSearch() {
        Bundle bundle = new Bundle();
        bundle.putInt("driver_id", driver.getDriver_id());
        AppDelegate.showFragment(getActivity(), new Search_Sponcers(), R.id.framelayout, bundle, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setValues();
        get_news();
        getdriverDetails();
        if (bundle.isEmpty()) {

        } else {
            getfanlist();
        }
    }

    private void getdriverDetails() {
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver.getDriver_id(), ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.DRIVER_DRIVER_DETAILS,
                                mPostArrayList, DriverHistory.this);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.DRIVER_DETAILS,
                                mPostArrayList, DriverHistory.this);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void getfanlist() {
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver.getDriver_id(), ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.DRIVER_FAN_LIST,
                                mPostArrayList, DriverHistory.this);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverHistory.this, ServerRequestConstants.FAN_LIST,
                                mPostArrayList, DriverHistory.this);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void setValues() {
        driver_name.setText(driver.getUsername() + "");
        total_races.setText("Race: " + driver.getTotal_race());
        total_wins.setText("Wins: " + driver.getTotal_win());
        //  rank.setText(driver.getCurrent_rank() + "");
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            fans.setVisibility(View.VISIBLE);
            fans.setText(driver.getTotal_point() + "");
        } else {
            rank.setText(driver.getCurrent_rank() + "");
            fans.setVisibility(View.GONE);
        }
        AppDelegate.getInstance(getActivity()).getImageLoader().get(driver.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Time out!!!Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FAN_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_list);
            parseFan_List();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_FAN_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_list);
            parseFan_List();
        } else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_DETAILS)) {
            AppDelegate.save(getActivity(), result, Parameters.driver_details);
            AppDelegate.Log("DRIVER", result);
            parse_driverDetails();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_DRIVER_DETAILS)) {
            AppDelegate.save(getActivity(), result, Parameters.driver_details);
            AppDelegate.Log("DRIVER", result);
            parse_driverDetails();
        }
    }

    private void parse_driverDetails() {
        String driver_response = AppDelegate.getValue(getActivity(), Parameters.driver_details);
        try {
            JSONObject jsonObject = new JSONObject(driver_response);
            sponcerModel = new SponcerModel();
            driverDetailModel = new DriverDetailModel();
            driverDetailModel.setStatus(jsonObject.getInt("status"));
            if (driverDetailModel.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("sponcer");
                ArrayList<SponcerModel> sponcerModelArrayList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    sponcerModel = new SponcerModel();
                    sponcerModel.setSponcer_img(obj.getString("image"));
                    sponcerModel.setSponcer_name(obj.getString("sponcer_name"));
                    sponcerModelArrayList.add(sponcerModel);
                }
                setdriverSponcer(sponcerModelArrayList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillChooserArray(List<SponcerModel> sponcer) {
        int position = -1;
        ArrayList<SponcerModel> models = new ArrayList<>();
        for (int i = 0; i < sponcer.size(); i++) {
            if (i % 6 == 0) {
                position++;
                models = new ArrayList<>();
            }
            models.add(sponcer.get(i));
            AppDelegate.Log("get", models + "");
            arrayListHashMap.put(position, models);
        }
        arrayFavCategoModelList.clear();
        for (int i = 0; i < arrayListHashMap.size(); i++) {
            arrayFavCategoModelList.add(new SponcerBannerModel(arrayListHashMap.get(i)));
            AppDelegate.LogT("arrayListHashMap pos = " + i + ", size  = " + arrayListHashMap.get(i).size());
        }
        AppDelegate.LogT("arrayFavCategoModelList = " + arrayFavCategoModelList.size() + ", arrayListHashMap = " + arrayListHashMap.size());
        // mHandler.sendEmptyMessage(2);
    }

    private void setdriverSponcer(ArrayList<SponcerModel> sponcerModels) {
        fillChooserArray(sponcerModels);
        bannerFragment.clear();
        for (int i = 0; i < arrayFavCategoModelList.size(); i++) {
            Fragment fragment = new BannerHomeFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.slider_id, arrayFavCategoModelList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        sponcer.setAdapter(bannerPagerAdapter);
        timeOutLoop();

    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    int value = sponcer.getCurrentItem();
                    if (bannerFragment.size() == 0) {
                        return;
                    } else {
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                            sponcer.setCurrentItem(value, false);
                        } else {
                            value++;
                            sponcer.setCurrentItem(value, true);
                        }
                        if (isAdded()) {
                            if (sponcer.getCurrentItem() == 0) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.postDelayed(this, 7000);
                            }
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseFan_List() {
        String Fan_list = AppDelegate.getValue(getActivity(), Parameters.fan_list);
        try {
            ArrayList<FanModel> fanList = new ArrayList<>();
            JSONObject object = new JSONObject(Fan_list);
            fanModel = new FanModel();
            fanModel.setStatus(object.getInt("status"));
            fanModel.setDataFlow(object.getInt("dataFlow"));
            if (fanModel.getStatus() == 1) {
                JSONObject response = object.getJSONObject("response");
                JSONArray FAN = response.getJSONArray("Fan List");
                for (int i = 0; i < FAN.length(); i++) {
                    JSONObject FanDetail = FAN.getJSONObject(i);
                    fanModel = new FanModel();
                    fanModel.setFan_id(FanDetail.getInt("Fan_id"));
                    fanModel.setFan_name(FanDetail.getString("name"));
                    fanModel.setFan_image(FanDetail.getString("image"));
                    fanModel.setCreated(FanDetail.getString("created"));
                    fanList.add(fanModel);
                }
                setFanLIST(fanList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFanLIST(ArrayList<FanModel> fanList) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        FanListAdapter adapter = new FanListAdapter(getActivity(), fanList);
        fanlist.setLayoutManager(layoutManager);
        fanlist.setAdapter(adapter);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        /*LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModels);
        banners.setLayoutManager(layoutManager);
        banners.setAdapter(adapter);*/
    }
}
