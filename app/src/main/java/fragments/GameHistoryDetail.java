package fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Constants.Parameters;
import Constants.Tags;
import UI.CircleImageView;
import UI.MyViewPager;
import adapters.ImageAdapter;
import adapters.SponcerBannerAdapter;
import model.DashboardModel;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.GameHistoryModel;
import model.Get_News_Model;
import model.LoginModel;
import model.ProductListModel;
import model.RaceDetailModel;
import model.RaceModel;
import model.SponcerBannerModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class GameHistoryDetail extends Fragment {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;

    private GridView productlist;
    private ProductListModel shoplistModel;
    private GameHistoryModel product;
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy;
    private ArrayList<ProductListModel> productListModelArrayList;
    private CircleImageView image;
    private TextView comp_name, race_name, track_name, prize, drivername, lap, date, total_credit, total_point;
    private DashboardModel dashboard_Model;
    private RaceModel race_Model;
    private ViewPager sponcer;
    private HashMap<Integer, ArrayList<SponcerModel>> arrayListHashMap = new HashMap<>();
    ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
    private SponcerBannerModel value = new SponcerBannerModel();
    private List<Fragment> bannerFragment = new ArrayList();
    private PagerAdapter bannerPagerAdapter;
    private Handler mHandler = new Handler();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.game_history_detail, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
        parse_dashboard();
    }

    private void parse_dashboard() {

        String dashboard = AppDelegate.getValue(getActivity(), Parameters.get_dashboard);
        try {
            ArrayList<DashboardModel> dashboardModelArrayList = new ArrayList<>();
            ArrayList<SponcerModel> sponcerModels = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(dashboard);
            dashboard_Model = new DashboardModel();
            dashboard_Model.setStatus(jsonObject.getInt("status"));
            dashboard_Model.setDataflow(jsonObject.getInt("dataFlow"));
            if (dashboard_Model.getStatus() == 1) {

                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ArrayList<RaceModel> raceModels = new ArrayList<>();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    dashboard_Model = new DashboardModel();
                    dashboard_Model.setId(obj.getInt("id"));
                    dashboard_Model.setCompetition_name(obj.getString("name"));
                    JSONArray race = obj.getJSONArray("races");
                    for (int j = 0; j < race.length(); j++) {
                        JSONObject race_obj = race.getJSONObject(j);
                        race_Model = new RaceModel();
                        race_Model.setRace_id(race_obj.getInt("id"));
                        race_Model.setClass_race_name(race_obj.getString("name"));
                        race_Model.setCompetition_id(race_obj.getInt("competition_id"));
                        raceModels.add(race_Model);
                    }
                    dashboard_Model.setRaces(raceModels);
                    dashboardModelArrayList.add(dashboard_Model);
                }
                JSONArray sponcer = null;
                try {
                    sponcer = jsonObject.getJSONArray("sponcer");
                    sponcerModel = new SponcerModel();
                    for (int i = 0; i < sponcer.length(); i++) {
                        JSONObject race_obj = sponcer.getJSONObject(i);
                        sponcerModel = new SponcerModel();
                        sponcerModel.setSponcer_id(race_obj.getInt("id"));
                        sponcerModel.setSponcer_name(race_obj.getString("sponcer_name"));
                        sponcerModel.setSponcer_img(race_obj.getString("image"));
                        sponcerModels.add(sponcerModel);
                    }
                    showSponcer(sponcerModels);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillChooserArray(List<SponcerModel> sponcer) {
        int position = -1;
        ArrayList<SponcerModel> models = new ArrayList<>();
        for (int i = 0; i < sponcer.size(); i++) {
            if (i % 6 == 0) {
                position++;
                models = new ArrayList<>();
            }
            models.add(sponcer.get(i));
            AppDelegate.Log("get", models + "");
            arrayListHashMap.put(position, models);
        }
        arrayFavCategoModelList.clear();
        for (int i = 0; i < arrayListHashMap.size(); i++) {
            arrayFavCategoModelList.add(new SponcerBannerModel(arrayListHashMap.get(i)));
            AppDelegate.LogT("arrayListHashMap pos = " + i + ", size  = " + arrayListHashMap.get(i).size());
        }
        AppDelegate.LogT("arrayFavCategoModelList = " + arrayFavCategoModelList.size() + ", arrayListHashMap = " + arrayListHashMap.size());
        // mHandler.sendEmptyMessage(2);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        MyViewPager sponcer = (MyViewPager) rootview.findViewById(R.id.banner_list);
        fillChooserArray(sponcerModels);
        bannerFragment.clear();
        for (int i = 0; i < arrayFavCategoModelList.size(); i++) {
            Fragment fragment = new BannerHomeFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.slider_id, arrayFavCategoModelList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        sponcer.setAdapter(bannerPagerAdapter);
        timeOutLoop();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    int value = sponcer.getCurrentItem();
                    if (bannerFragment.size() == 0) {
                        return;
                    } else {
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                            sponcer.setCurrentItem(value, false);
                        } else {
                            value++;
                            sponcer.setCurrentItem(value, true);
                        }
                        if (isAdded()) {
                            if (sponcer.getCurrentItem() == 0) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.postDelayed(this, 7000);
                            }
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Game Details");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        bundle = getArguments();
        product = new GameHistoryModel();
        product = bundle.getParcelable(Tags.parcel_gameHistoryItem);
        image = (CircleImageView) rootview.findViewById(R.id.image);
        comp_name = (TextView) rootview.findViewById(R.id.competion_name);
        race_name = (TextView) rootview.findViewById(R.id.class_name);
        // track_name = (TextView) rootview.findViewById(R.id.track_name);
        //  prize = (TextView) rootview.findViewById(R.id.prize);
        drivername = (TextView) rootview.findViewById(R.id.driver_name);
        lap = (TextView) rootview.findViewById(R.id.lap);
        date = (TextView) rootview.findViewById(R.id.date);
        total_credit = (TextView) rootview.findViewById(R.id.credit);
        total_point = (TextView) rootview.findViewById(R.id.points);
        TextView time = (TextView) rootview.findViewById(R.id.time);
        time.setText(product.getTime());
        drivername.setText(product.getAddress() + "");
        comp_name.setText(product.getCompetitions_name() + "");
        race_name.setText(product.getRace_name() + "");
        lap.setText(product.getLap() + "");
        date.setText(product.getDate() + "");
        // prize.setText(String.valueOf(product.getTotal_credit() + "") + "");
        total_point.setText(String.valueOf(product.getTotal_point() + "") + "");
        //  track_name.setText(String.valueOf(product.getTrack_name()) + "");
        total_credit.setText(String.valueOf(product.getCurrent_rank()) + "");
        AppDelegate.getInstance(getActivity()).getImageLoader().get(product.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    image.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
