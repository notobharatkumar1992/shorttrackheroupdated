package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import Constants.Tags;
import model.SponcerBannerModel;

/**
 * Created by admin on 27-07-2016.
 */
public class BannerHomeFragment extends Fragment implements View.OnClickListener {
    private View rootview;
    ImageView image1, image2, image3, image4, image5, image6;
    private Bundle bundle;
    private SponcerBannerModel sponcerbannermodel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.banner, container, false);
        bundle = getArguments();
        sponcerbannermodel = bundle.getParcelable(Tags.slider_id);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image1 = (ImageView) rootview.findViewById(R.id.im1);
        image2 = (ImageView) rootview.findViewById(R.id.im2);
        image3 = (ImageView) rootview.findViewById(R.id.im3);
        image4 = (ImageView) rootview.findViewById(R.id.im4);
        image5 = (ImageView) rootview.findViewById(R.id.im5);
        image6 = (ImageView) rootview.findViewById(R.id.im6);
        ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
        SponcerBannerModel value = new SponcerBannerModel();
        for (int j = 0; j < sponcerbannermodel.getArrayListHashMap().size(); j++) {
            final int finalJ = j;
            AppDelegate.getInstance(getActivity()).getImageLoader().get(sponcerbannermodel.getArrayListHashMap().get(j).getSponcer_img(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppDelegate.LogE(error);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        if (finalJ % 6 == 0) {
                            image1.setImageBitmap(response.getBitmap());
                            image1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 1 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        } else if (finalJ % 6 == 1) {
                            image2.setImageBitmap(response.getBitmap());
                            image2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 2 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        } else if (finalJ % 6 == 2) {
                            image3.setImageBitmap(response.getBitmap());
                            image3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 3 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        } else if (finalJ % 6 == 3) {
                            image4.setImageBitmap(response.getBitmap());
                            image4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 4 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        } else if (finalJ % 6 == 4) {
                            image5.setImageBitmap(response.getBitmap());
                            image5.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 5 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        } else if (finalJ % 6 == 5) {
                            image6.setImageBitmap(response.getBitmap());
                            image6.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AppDelegate.LogT("image 6 clicked");
                                    bundle.putInt(Tags.sponsor_id, sponcerbannermodel.getArrayListHashMap().get(finalJ).getSponcer_id());
                                    AppDelegate.showFragment(getActivity(), new SponsorDetails(), R.id.framelayout, bundle, null);
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {

    }
    // }
}
