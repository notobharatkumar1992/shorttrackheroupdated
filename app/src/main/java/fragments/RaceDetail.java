package fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.Time;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import ExpendibleRecyclerView.ChildGetterSetter;
import adapters.SponcerBannerAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.Race;
import model.RaceDetailModel;
import model.RankList;
import model.SponcerModel;
import utils.DateUtils;

public class RaceDetail extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnDialogClickListener {
    private Bundle bundle = new Bundle();
    private ChildGetterSetter raceModel;
    public static View rootview;
    FrameLayout frame;
    RecyclerView banners;
    TextView news, competition_title, class_race_title, date, time, laps, address, endday, endtime, racedetail, drivers;
    private String title;
    private RaceDetailModel raceDetailModel;
    private Race racemodel;
    private SponcerModel sponcerModel;
    private Driver driverModel;
    private Get_News_Model newsModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private TextView interested;
    private String identifier;
    private ImageView search;
    private int key;
    String[] startdatetime;
    private boolean result;
    private String[] enddatetime;
    public static TextView fan_points;
    private RelativeLayout rel_one;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.race_detail, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        key = bundle.getInt("interested");
        raceModel = bundle.getParcelable(Tags.parcel_raceDetail);
        title = bundle.getString("Parent");
        AppDelegate.Log("parent", title);
        initializeObjects();
        get_news();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_news() {
        parseProfile();

        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), RaceDetail.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), RaceDetail.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void callSearch() {
        Bundle bundle = new Bundle();
        bundle.putInt("comp_id", raceModel.getComp_id());
        AppDelegate.showFragment(getActivity(), new Search_SponcersComp(), R.id.framelayout, bundle, null);
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("SHORT TRACK HERO");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSearch();
            }
        });
        news = (TextView) rootview.findViewById(R.id.news);
        competition_title = (TextView) rootview.findViewById(R.id.title);
        fan_points = (TextView) rootview.findViewById(R.id.fan_points);
        class_race_title = (TextView) rootview.findViewById(R.id.class_race_title);
        date = (TextView) rootview.findViewById(R.id.day);
        time = (TextView) rootview.findViewById(R.id.time);
        laps = (TextView) rootview.findViewById(R.id.laps);
        address = (TextView) rootview.findViewById(R.id.address);
        frame = (FrameLayout) rootview.findViewById(R.id.frame_race);

        interested = (TextView) rootview.findViewById(R.id.interested);
        interested.setVisibility(View.GONE);
       /* banners = (RecyclerView) rootview.findViewById(R.id.banner);*/
        endday = (TextView) rootview.findViewById(R.id.endday);
        endtime = (TextView) rootview.findViewById(R.id.endtime);
        racedetail = (TextView) rootview.findViewById(R.id.racedetails);
        drivers = (TextView) rootview.findViewById(R.id.drivers);
       /* drivers.setOnClickListener(this);*/
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            fan_points.setVisibility(View.GONE);
            interested.setVisibility(View.VISIBLE);
            if (key == 1) {
                interested.setText("CHECK IN");
                interested.setVisibility(View.GONE);
            } else {
                interested.setText("I AM INTERESTED");
            }
            interested.setOnClickListener(this);
        } else {
            fan_points.setVisibility(View.VISIBLE);
            interested.setVisibility(View.GONE);
        }
        competition_title.setText(title + "");
        rel_one = (RelativeLayout) rootview.findViewById(R.id.rel_one);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_raceDetail();
    }

    private void execute_raceDetail() {
        try {
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getComp_id()), Tags.competitionID);
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getRace_id()), Tags.raceID);
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.race_id, raceModel.getRace_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.comp_id, raceModel.getComp_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.DRIVER_RACE_DETAILS,
                            mPostArrayList, RaceDetail.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.RACE_DETAILS,
                            mPostArrayList, RaceDetail.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.RACE_DETAILS)) {
            AppDelegate.save(getActivity(), result, Parameters.race_details);
            AppDelegate.Log("race_details", result);
            parse_raceDetails();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_RACE_DETAILS)) {
            AppDelegate.save(getActivity(), result, Parameters.race_details);
            AppDelegate.Log("race_details driver", result);
            parse_raceDetails();
        } else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_COMPETITION_INTERESTED)) {
            AppDelegate.save(getActivity(), result, Parameters.interested);
            AppDelegate.Log("interested", result);
            parse_interested();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_CHECKIN)) {
            AppDelegate.save(getActivity(), result, Parameters.checkin);
            AppDelegate.Log("interested", result);
            parse_checkin();
        }
    }

    private void parse_checkin() {
        String interest = AppDelegate.getValue(getActivity(), Parameters.checkin);
        try {
            JSONObject jsonObject = new JSONObject(interest);
            int status = jsonObject.getInt("status");

            String message = jsonObject.getString("message");
            AppDelegate.showAlert(getActivity(), message + "");
            if (status == 1) {
                interested.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parse_interested() {
        String interested = AppDelegate.getValue(getActivity(), Parameters.interested);
        try {
            JSONObject jsonObject = new JSONObject(interested);
            String message = jsonObject.getString("message");
            AppDelegate.ShowDialog(getActivity(), message + "", "Alert!!!");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parse_raceDetails() {
        String race_details_response = AppDelegate.getValue(getActivity(), Parameters.race_details);
        try {
            JSONObject obj = new JSONObject(race_details_response);
            raceDetailModel = new RaceDetailModel();
            raceDetailModel.setStatus(obj.getInt("status"));
            raceDetailModel.setDataflow(obj.getInt("dataFlow"));
            if (raceDetailModel.getStatus() == 1) {
                JSONObject response = obj.getJSONObject("response");
               /* get race response*/
                JSONObject raceobject = response.getJSONObject("Race");
                racemodel = new Race();
                racemodel.setRace_id(raceobject.getInt("id"));
                racemodel.setName(raceobject.getString("name"));
                racemodel.setAddresss(raceobject.getString("address"));
                racemodel.setLap(raceobject.getString("lap"));
                racemodel.setTotal_point(raceobject.getInt("total_point"));
                racemodel.setOther(raceobject.getString("other"));
                racemodel.setStart_date(raceobject.getString("start_date"));
                racemodel.setEnd_date(raceobject.getString("end_date"));
                JSONArray racehistory = raceobject.getJSONArray("result");
                ArrayList<RankList> rankListArrayList = new ArrayList<>();
                for (int i = 0; i < racehistory.length(); i++) {
                    JSONObject object = racehistory.getJSONObject(i);
                    RankList rank = new RankList();
                    rank.setPoint(object.getString("point"));
                    rank.setRank(object.getString("rank"));
                    rankListArrayList.add(rank);
                }
                if(racehistory.length()>0){
                    rel_one.setVisibility(View.VISIBLE);
                }else{
                    rel_one.setVisibility(View.GONE);
                }
                racemodel.setRanklist(rankListArrayList);
                raceDetailModel.setRace(racemodel);
                showDetails();
                racedetail.setOnClickListener(this);
              /*  get driver response*/
                try {
                    ArrayList<Driver> driver = new ArrayList<>();
                    // response = obj.getJSONObject("response");
                    JSONArray driverobject = response.getJSONArray("driver");
                    driverModel = new Driver();
                    AppDelegate.LogT("driver size" + driverobject.length() + "driver");
                    for (int i = 0; i < driverobject.length(); i++) {
                        JSONObject driver_obj = driverobject.getJSONObject(i);
                        driverModel = new Driver();
                        driverModel.setDriver_id(driver_obj.getInt("id"));
                        driverModel.setUsername(driver_obj.getString("username"));
                        driverModel.setDriver_address(driver_obj.getString("address"));
                        driverModel.setTotal_win(driver_obj.getString("total_win"));
                        driverModel.setTotal_race(driver_obj.getInt("total_race"));
                        driverModel.setCurrent_rank(driver_obj.getInt("current_rank"));
                        driverModel.setTotal_credit(driver_obj.getInt("total_credit"));
                        driverModel.setTotal_point(driver_obj.getInt("total_point"));
                        driverModel.setDriver_img(driver_obj.getString("img"));
                        driver.add(driverModel);
                    }
                    raceDetailModel.setDriver(driver);
                    //   drivers.setOnClickListener(this);
                    AppDelegate.LogT("driver size." + driver.size() + "add");
                    // result = compare("2016-07-20", "10:45:00", df.format(c.getTime()), tf.format(c.getTime()));
                } catch (JSONException e) {
                    e.printStackTrace();
                    raceDetailModel.setDriver(null);
                }
                try {
            /*get sponcer response*/
                    ArrayList<SponcerModel> sponcer = new ArrayList<>();
                    //response = obj.getJSONObject("response");
                    JSONArray sponcerobject = response.getJSONArray("sponcer");
                    sponcerModel = new SponcerModel();
                    for (int i = 0; i < sponcerobject.length(); i++) {
                        JSONObject race_obj = sponcerobject.getJSONObject(i);
                        sponcerModel = new SponcerModel();
                        sponcerModel.setSponcer_id(race_obj.getInt("id"));
                        sponcerModel.setSponcer_name(race_obj.getString("sponcer_name"));
                        sponcerModel.setSponcer_img(race_obj.getString("image"));
                        sponcer.add(sponcerModel);
                    }
                    raceDetailModel.setSponcer(sponcer);
                    // showSponcer(sponcer);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AppDelegate.Log("raceDetailModel", raceDetailModel + "");
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
            drivers.setOnClickListener(this);
            bundle.putParcelable(Tags.parcel_Detail, raceDetailModel);
            if (raceDetailModel.getDriver().size() <= 0) {
                bundle.putInt("key", 1);
            } else {
                bundle.putInt("key", 2);
            }
            AppDelegate.showFragment(getChildFragmentManager(), new DriverList(), R.id.frame_race, bundle, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showDetails() {
        class_race_title.setText(racemodel.getName() + "");
        laps.setText("Laps :" + racemodel.getLap() + "");
        startdatetime = racemodel.getStart_date().split(" ");
        String sdate = null, stime = null;
        try {
            sdate = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(startdatetime[0]));
            stime = new SimpleDateFormat("hh:mm:ss a").format(new SimpleDateFormat("HH:mm:ss").parse(startdatetime[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.setText(sdate + "");
        time.setText(stime + "");
        enddatetime = racemodel.getEnd_date().split(" ");
        endday.setText(enddatetime[0] + "");
        endtime.setText(enddatetime[1] + "");
        address.setText(racemodel.getAddresss() + "");
        // AppDelegate.save(getActivity(), String.valueOf(racemodel.getTotal_point()), "fanpoints");
        fan_points.setText(String.valueOf(racemodel.getTotal_point()) + "");
        if (AppDelegate.isValidString(AppDelegate.getValue(getActivity(), "fanpoints"))) {
            fan_points.setText(AppDelegate.getValue(getActivity(), "fanpoints") + "");
        }
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
            Calendar c = Calendar.getInstance();
            result = compare(startdatetime[0], startdatetime[1], df.format(c.getTime()), tf.format(c.getTime()));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.racedetails:
                racedetail.setBackgroundResource(R.drawable.yellowbtn);
                racedetail.setTextColor(getResources().getColor(R.color.black));
                drivers.setBackgroundColor(getResources().getColor(R.color.black));
                drivers.setTextColor(getResources().getColor(android.R.color.white));
                bundle.putParcelable(Tags.parcel_Detail, raceDetailModel);
                AppDelegate.showFragment(getChildFragmentManager(), new Details(), R.id.frame_race, bundle, null);
                break;
            case R.id.drivers:
                drivers.setBackgroundResource(R.drawable.yellowbtn);
                drivers.setTextColor(getResources().getColor(R.color.black));
                racedetail.setBackgroundColor(getResources().getColor(R.color.black));
                racedetail.setTextColor(getResources().getColor(android.R.color.white));
                bundle.putParcelable(Tags.parcel_Detail, raceDetailModel);

                if (raceDetailModel.getDriver().size() <= 0) {
                    bundle.putInt("key", 1);
                    AppDelegate.ShowDialog(getActivity(), "No driver Available.", "Alert!!!");
                    AppDelegate.showFragment(getChildFragmentManager(), new DriverList(), R.id.frame_race, bundle, null);
                } else {
                    bundle.putInt("key", 2);
                    AppDelegate.showFragment(getChildFragmentManager(), new DriverList(), R.id.frame_race, bundle, null);
                }
                break;
            case R.id.interested:
                if (interested.getText().toString().equalsIgnoreCase("CHECK IN")) {
                    AppDelegate.showDialog_twoButtons(getActivity(), "Are you sure? ", "check_in", this);
                } else {
                    AppDelegate.showDialog_twoButtons(getActivity(), "Are you sure? ", "interested", this);
                }
                break;
        }
    }

    private void callInterested() {
        try {
            String track_id = AppDelegate.getValue(getActivity(), Tags.TRACK_ID);
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getComp_id()), Tags.competitionID);
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getRace_id()), Tags.raceID);
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.race_id, raceModel.getRace_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.competition_id, raceModel.getComp_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, Integer.parseInt(track_id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.DRIVER_COMPETITION_INTERESTED,
                            mPostArrayList, RaceDetail.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals("interested")) {
            callInterested();
            //AppDelegate.showToast(getActivity(),"Hellooo");
        } else if (name.equalsIgnoreCase("check_in")) {
            callcheckin();
        }
    }


    private boolean compare(String date, String time, String date1, String time1) {
        boolean result = false;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd" + "HH:mm");
            Date newdate1 = formatter.parse(date + time);
            Date newdate2 = formatter.parse(date1 + time1);
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(newdate1);
            AppDelegate.Log("new time2", newdate1 + "");
            cal1.add(Calendar.MINUTE, -6);
            Date newdate3 = cal1.getTime();
            if (DateUtils.isToday(newdate1)) {
                AppDelegate.Log("date", "Is today true = " + newdate1 + ", 1st con => " + newdate2.after(newdate3) + ", 2nd  con => " + newdate2.before(newdate1));
                if (newdate2.after(newdate3) && newdate2.before(newdate1)) {
                    AppDelegate.Log("date", "newdate1.after true = " + newdate1);
                    interested.setVisibility(View.VISIBLE);
                } else {
                    AppDelegate.Log("date", "newdate1.after false = " + newdate1);
                    interested.setVisibility(View.GONE);
                }
            } else {
                AppDelegate.Log("date", "Is today false = " + newdate1);
            }
//
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return result;
    }


    private void callcheckin() {
        /*handler.removeCallbacks(runnableCode);*/
        try {
            String track_id = AppDelegate.getValue(getActivity(), Tags.TRACK_ID);
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getComp_id()), Tags.competitionID);
            AppDelegate.save(getActivity(), String.valueOf(raceModel.getRace_id()), Tags.raceID);
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.race_id, raceModel.getRace_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.comp_id, raceModel.getComp_id(), ServerRequestConstants.Key_PostintValue);
                // AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, Integer.parseInt(track_id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.DRIVER_CHECKIN,
                            mPostArrayList, RaceDetail.this);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
}
