package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.ProductListAdapter;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class Shop extends Fragment implements OnReciveServerResponse {
    private View rootview;
    private Bundle bundle=new Bundle();
    private RaceDetailModel racedetailModel;
    private Driver driver;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name,fans,total_races,total_wins,rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private static final String TOAST_TEXT = "Test ads are being shown. "
            + "To show live ads, replace the ad unit ID in res/values/strings.xml with your own ad unit ID.";
    private String identifier;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.shop, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
        AdView adView = (AdView) rootview.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
      /*  AdView adView = (AdView)rootview. findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);*/
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Shop");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        productlist=(GridView)rootview.findViewById(R.id.shop);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getproductlist();
    }

    private void getproductlist() {
        try{
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                         mPostAsyncObj = new PostAsync(getActivity(), Shop.this, ServerRequestConstants.DRIVER_PRODUCT_LIST,
                                mPostArrayList, Shop.this);
                    }else{
                         mPostAsyncObj = new PostAsync(getActivity(), Shop.this, ServerRequestConstants.PRODUCT_LIST,
                                mPostArrayList, Shop.this);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        }catch (Exception e){
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(),"Service Time out!!!","Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.PRODUCT_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.product_list);
            parseProduct_List();
        }
      else  if (apiName.equals(ServerRequestConstants.DRIVER_PRODUCT_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.product_list);
            parseProduct_List();
        }
    }
    private void parseProduct_List() {
String shop_list=AppDelegate.getValue(getActivity(),Parameters.product_list);
        try {
            JSONObject obj=new JSONObject(shop_list);
            shoplistModel=new ProductListModel();
            shoplistModel.setStatus(obj.getInt("status"));
            ArrayList<ProductListModel> productListModelArrayList=new ArrayList<>();
            shoplistModel.setDataflow(obj.getInt("dataFlow"));
            if(shoplistModel.getStatus()==1){
             JSONArray response=obj.getJSONArray("response");
                for(int i=0;i<response.length();i++)
                {
                    JSONObject jsonObject=response.getJSONObject(i);
                    shoplistModel=new ProductListModel();
                    shoplistModel.setProduct_id(jsonObject.getInt("id"));
                    shoplistModel.setProduct_name(jsonObject.getString("name"));
                    shoplistModel.setDescription(jsonObject.getString("description"));
                    shoplistModel.setPrice(jsonObject.getDouble("price"));
                    shoplistModel.setImage(jsonObject.getString("image"));
                    shoplistModel.setQuantity(jsonObject.getInt("quantity"));
                    productListModelArrayList.add(shoplistModel);
                }
                setProductList(productListModelArrayList);
            } else{
                AppDelegate.ShowDialog(getActivity(), "No product found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setProductList(final ArrayList<ProductListModel> productListModelArrayList) {
        ProductListAdapter adapter = new ProductListAdapter(getActivity(),productListModelArrayList );

        productlist.setAdapter(adapter);
        productlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
             //   Toast.makeText(getActivity(), "You Clicked at " + [+position], Toast.LENGTH_SHORT).show();
                bundle.putParcelable(Tags.parcel_ProductDetail, productListModelArrayList.get(position));
                AppDelegate.showFragment(getActivity(), new ShopDetail(), R.id.framelayout, bundle, null);
            }
        });
    }



}
