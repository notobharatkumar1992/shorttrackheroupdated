package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.Login;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.CityAdapter;
import adapters.CountryAdapter;
import adapters.StateAdapter;
import adapters.TrackAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.CityListModel;
import model.CountryListModel;
import model.PostAysnc_Model;
import model.StateListModel;
import model.TrackListModel;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;


/**
 * Created by admin on 16-06-2016.
 */
public class DriverRegister extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnDialogClickListener {
    View rootview;
    CircleImageView uploadImg;
    EditText fullname, email_address, contact_no, zipcode, password, confirm_password,city;
    TextView country, state, track, signup;
    CheckBox subscribe;
    Intent i;
    Dialog dialog;
    private static int RESULT_LOAD_IMAGE = 1;
    private Uri selectedImage;
    private String filepath;
    private TrackListModel tracklistitem;
    private TextView title;
    private ListView list;
    private CountryListModel countrylistitem;
    private int country_id;
    private StateListModel statelistitem;
    private int state_id;
    private CityListModel citylistitem;
    private String device_id;
    private Bitmap OriginalPhoto;
    //    private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private int city_id;
    private String message;
    private int dataFlow, status;
    private int track_id;
    private EditText car_number;
    private String car_no;
    private EditText refer_code;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.driver_register, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
        country.setOnClickListener(this);
        state.setOnClickListener(this);
        track.setOnClickListener(this);
        signup.setOnClickListener(this);
        uploadImg.setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        device_id = AppDelegate.getUUID(getActivity());
    }

    private void initializeObjects() {
        uploadImg = (CircleImageView) rootview.findViewById(R.id.uploadimg);
        fullname = (EditText) rootview.findViewById(R.id.full_name);
        email_address = (EditText) rootview.findViewById(R.id.email_address);
        contact_no = (EditText) rootview.findViewById(R.id.contact_no);
        zipcode = (EditText) rootview.findViewById(R.id.zip_code);
        password = (EditText) rootview.findViewById(R.id.password);
        confirm_password = (EditText) rootview.findViewById(R.id.confirm_password);
        country = (TextView) rootview.findViewById(R.id.country);
        state = (TextView) rootview.findViewById(R.id.state);
        track = (TextView) rootview.findViewById(R.id.select_track);
        signup = (TextView) rootview.findViewById(R.id.signup);
        city = (EditText) rootview.findViewById(R.id.city);
        car_number = (EditText) rootview.findViewById(R.id.car_number);
        subscribe = (CheckBox) rootview.findViewById(R.id.subscribe);
        refer_code=(EditText)rootview.findViewById(R.id.refer_code);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    @Override
    public void setOnDialogClickListener(String name) {
        if(name.equals("DriverRegister")){
            if (status == 1) {

                reset();
                if (getActivity() != null) {
                    getActivity().finish();
                }
                if (Login.loginClass != null) {
                    Login.loginClass.finish();
                }
            } else if (status == 0) {

                reset();
            }
        }
    }
    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.uploadimg:
                showImageSelectorList();
                break;
            case R.id.select_track:
                execute_TRACKLIST();
                break;
            case R.id.country:
                execute_Countrylist();
                state.setText("");
                city.setText("");
                break;
            case R.id.state:
                if (country.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select Country first", "Alert!!!");

                } else {
                    execute_Statelist();
                    city.setText("");
                }
                break;
            case R.id.city:
                if (state.getText().toString().isEmpty()) {
                    AppDelegate.ShowDialog(getActivity(), "Please select state first", "Alert!!!");

                } else {
                    //execute_Citylist();
                }
                break;
            case R.id.signup:
                String name, email, contact, password, confirmpass, country, state, city, zipcode, track;
                name = fullname.getText().toString();
                email = email_address.getText().toString();
                contact = contact_no.getText().toString();
                password = this.password.getText().toString();
                confirmpass = confirm_password.getText().toString();
                country = this.country.getText().toString();
                state = this.state.getText().toString();
                city = this.city.getText().toString();
                zipcode = this.zipcode.getText().toString();
                track = this.track.getText().toString();
                car_no = car_number.getText().toString();
                boolean valid = validateall(name, email, contact, password, confirmpass, country, state, city, zipcode, track, car_no);
              //  if (subscribe.isChecked()) {
                    if (valid == true) {
                            execute_signup();
                        }
                   /* }else {
                    AppDelegate.ShowDialog(getActivity(), "Please subscribe to email updates and promotion by sponsors and track.", "Alert!!!");
                }*/

                break;
        }
    }


    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"Camera", "Gallery", "Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AppDelegate.showProgressDialog(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AppDelegate.hideProgressDialog(getActivity());
        }
    }
    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = getActivity()
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();
                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto != null) {
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        uploadImg.setImageBitmap(OriginalPhoto);
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to deliver result.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);
                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    uploadImg.setImageBitmap(OriginalPhoto);
                                }

                            } else {
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(
                                OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);

                        uploadImg.setImageBitmap(OriginalPhoto);

                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }

                        // getActivity().getContentResolver().delete(cropimageUri, null, null);
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean validateall(String name, String email, String contact, String password, String confirmpass, String country, String state, String city, String zipcode, String track, String car_no) {
        boolean validate = true;
        try {
           /* if (name.isEmpty() && email.isEmpty() && contact.isEmpty() && password.isEmpty() && confirmpass.isEmpty() && country.isEmpty() && state.isEmpty() && city.isEmpty() && zipcode.isEmpty() && track.isEmpty() && car_no.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please fill the fields", "Alert!!!");
            } else*/if(capturedFile==null){
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please Select Image.", "Alert!!!");
            }
            else if (name.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter  name", "Alert!!!");
            } else if (email.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter Email id", "Alert!!!");
            } else /*if (!email.isEmpty()) {*/
                if (AppDelegate.CheckEmail(email)==false) {

               /* }else{*/
                    validate = false;
                    AppDelegate.ShowDialog(getActivity(), "Invalid Email Address", "Alert!!!");
              //  }
            } else if (contact.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter Contact no.", "Alert!!!");
            } else if (contact.length() < 10) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter  correct Contact no.", "Alert!!!");
            } else if (car_no.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter Car number.", "Alert!!!");
            } else if (country.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter country", "Alert!!!");
            } else if (state.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter state", "Alert!!!");
            } else if (city.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter city", "Alert!!!");
            } else if (zipcode.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter zip-code", "Alert");
            }  else if (password.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter password", "Alert!!!");
            } else if (confirmpass.isEmpty()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Enter confirm password", "Alert!!!");
            }
           else if (!password.equals(confirmpass)) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Password does not match!!!", "Alert!!!");
            }
            else if (track.isEmpty()) {
                validate = false;
                if(AppDelegate.isValidString(track)){ }else{
                    validate = false;
                }
                AppDelegate.ShowDialog(getActivity(), "Select Track", "Alert!!!");
            }else if (!subscribe.isChecked()) {
                validate = false;
                AppDelegate.ShowDialog(getActivity(), "Please subscribe to email updates and promotion by sponsors and track.", "Alert!!!");

            }  else {
                validate = AppDelegate.password_validation(getActivity(), password);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return validate;
    }
    private void execute_signup() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.username, fullname.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.email, email_address.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.mobile, Long.parseLong(contact_no.getText().toString()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.confirmpassword, confirm_password.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.password, password.getText().toString() + "");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.post_code, Long.parseLong(zipcode.getText().toString()), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.device_type, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.car_number, car_number.getText().toString() + ""/*, ServerRequestConstants.Key_PostintValue*/);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.device_id, device_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.state_id, state_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.city_id, city.getText().toString());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, track_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.login_type, Parameters.login_type_fordriver, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(getActivity(), Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, capturedFile, ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.refer_code, refer_code.getText().toString()+"");

                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverRegister.this, ServerRequestConstants.REGISTER_DRIVER,
                        mPostArrayList, DriverRegister.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

/*
    private void execute_Citylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.state_id, state_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverRegister.this, ServerRequestConstants.GET_CITYLIST,
                        mPostArrayList, DriverRegister.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }*/

    private void execute_Statelist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.country_id, country_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverRegister.this, ServerRequestConstants.GET_STATELIST,
                        mPostArrayList, DriverRegister.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void execute_Countrylist() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverRegister.this, ServerRequestConstants.GET_COUNTRY_LIST,
                        mPostArrayList, DriverRegister.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.TRACKLIST)) {

            AppDelegate.save(getActivity(), result, Parameters.savetracklist);
            parseTracklist();

        } else if (apiName.equals(ServerRequestConstants.GET_COUNTRY_LIST)) {

            AppDelegate.save(getActivity(), result, Parameters.savecountrylist);
            parseCountryList();

        } else if (apiName.equals(ServerRequestConstants.GET_STATELIST)) {

            AppDelegate.save(getActivity(), result, Parameters.savestatelist);
            parseStatelist();

        } /*else if (apiName.equals(ServerRequestConstants.GET_CITYLIST)) {

            AppDelegate.save(getActivity(), result, Parameters.savecitylist);
            parseCitylist();

        }*/ else if (apiName.equals(ServerRequestConstants.REGISTER_DRIVER)) {

            AppDelegate.save(getActivity(), result, Parameters.Registerdriver);
            parseRegister();

        }
    }

    void reset() {
        fullname.setText("");
        email_address.setText("");
        contact_no.setText("");
        password.setText("");
        confirm_password.setText("");
        country.setText("");
        state.setText("");
        city.setText("");
        zipcode.setText("");
        subscribe.setChecked(false);
        car_number.setText("");
        track.setText("");
        /*capturedFile=null;
      //  uploadImg.setImageBitmap(0);
        uploadImg.setImageResource(android.R.color.transparent);*/
    }

    private void parseRegister() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.Registerdriver);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            dataFlow = jsonObject.getInt("dataFlow");
            message = jsonObject.getString("message");
            AppDelegate.ShowDialogID(getActivity(), message + "", "Alert!!!", "DriverRegister", this);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseStatelist() {
        String Statelist = AppDelegate.getValue(getActivity(), Parameters.savestatelist);
        try {
            JSONObject jsonObject = new JSONObject(Statelist);
            ArrayList<StateListModel> stateListModels = new ArrayList<StateListModel>();
            statelistitem = new StateListModel();

            statelistitem.setStatus(jsonObject.getInt("status"));
            statelistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (statelistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    statelistitem = new StateListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    statelistitem.setId(obj.getInt("id"));
                    statelistitem.setRegion_name(obj.getString("name"));
                    stateListModels.add(statelistitem);
                }
                stateset(stateListModels);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void stateset(final ArrayList<StateListModel> stateListModels) {
        Dialogue();
        StateAdapter spinnerAdapter = new StateAdapter(getActivity(), stateListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = stateListModels.get(position).getRegion_name();
                state.setText(timee);
                state_id = stateListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select State ");
        dialog.show();
    }

    private void parseCountryList() {
        String Countrylist = AppDelegate.getValue(getActivity(), Parameters.savecountrylist);
        try {
            JSONObject jsonObject = new JSONObject(Countrylist);
            ArrayList<CountryListModel> countryListModels = new ArrayList<CountryListModel>();
            countrylistitem = new CountryListModel();
            countrylistitem.setStatus(jsonObject.getInt("status"));
            countrylistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (countrylistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    countrylistitem = new CountryListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    countrylistitem.setId(obj.getInt("id"));
                    countrylistitem.setName(obj.getString("name"));
                    countrylistitem.setCode(obj.getString("code"));
                    countryListModels.add(countrylistitem);
                }
                countryset(countryListModels);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void countryset(final ArrayList<CountryListModel> countryListModels) {
        Dialogue();
        CountryAdapter spinnerAdapter = new CountryAdapter(getActivity(), countryListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = countryListModels.get(position).getName();
                country.setText(timee);
                country_id = countryListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select Country ");
        dialog.show();
    }

    private void Dialogue() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        title = (TextView) dialog.findViewById(R.id.dialog_title);
        list = (ListView) dialog.findViewById(R.id.dialog_list);
    }

    private void parseTracklist() {
        String tracklist = AppDelegate.getValue(getActivity(), Parameters.savetracklist);
        try {
            JSONObject jsonObject = new JSONObject(tracklist);
            ArrayList<TrackListModel> trackListModels = new ArrayList<TrackListModel>();
            tracklistitem = new TrackListModel();
            tracklistitem.setStatus(jsonObject.getInt("status"));
            tracklistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (tracklistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    tracklistitem = new TrackListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    tracklistitem.setId(obj.getInt("id"));
                    tracklistitem.setCompany_name(obj.getString("company_name"));
                    tracklistitem.setPayment_method_id(obj.getInt("payment_method_id"));
                    tracklistitem.setCreated(obj.getString("created"));
                    tracklistitem.setUser_id(obj.getInt("id"));
                    trackListModels.add(tracklistitem);
                }
                trackset(trackListModels);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void trackset(final ArrayList<TrackListModel> trackListModels) {
        Dialogue();
        TrackAdapter spinnerAdapter = new TrackAdapter(getActivity(), trackListModels);
        list.setAdapter(spinnerAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String timee = trackListModels.get(position).getCompany_name();
                track.setText(timee);
                track_id = trackListModels.get(position).getId();
                dialog.dismiss();
            }
        });
        title.setText("Select track ");
        dialog.show();
    }

    private void execute_TRACKLIST() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverRegister.this, ServerRequestConstants.TRACKLIST,
                    mPostArrayList, DriverRegister.this);
            AppDelegate.showProgressDialog(getActivity());
            mPostAsyncObj.execute();
        }
    }
}
