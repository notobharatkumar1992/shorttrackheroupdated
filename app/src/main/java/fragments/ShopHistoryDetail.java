package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.ImageAdapter;
import interfaces.OnListItemClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.ProductListModel;
import model.RaceDetailModel;
import model.SponcerModel;

/**
 * Created by admin on 30-06-2016.
 */
public class ShopHistoryDetail extends Fragment {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private Driver driver;
    private LinearLayout pager_indicator;
    private RecyclerView fanlist;
    ImageView driver_img;
    TextView driver_name, fans, total_races, total_wins, rank;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;
    private RecyclerView sponcer;
    private GridView productlist;
    private ProductListModel shoplistModel;
    private ProductListModel product;
    private ImageAdapter imageAdapter;
    private ViewPager viewpager;
    private ImageView[] dots;
    private ArrayList<String> slider;
    private TextView name, price, description, buy;
    private ArrayList<ProductListModel> productListModelArrayList;
    private ImageView image;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.shop_historydetail, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeObjects();
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Shop History");
        TextView  save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        bundle = getArguments();
        product=new ProductListModel();
        product = bundle.getParcelable(Tags.parcel_shopHistoryItem);
        AppDelegate.Log("Tag",product+"");
        image = (ImageView) rootview.findViewById(R.id.image);
        name = (TextView) rootview.findViewById(R.id.name);
        price = (TextView) rootview.findViewById(R.id.price);
        description = (TextView) rootview.findViewById(R.id.description);
        name.setText(product.getProduct_name());
        price.setText("Cost :"+String.valueOf(product.getPrice()));
        description.setText(product.getDescription());
        AppDelegate.getInstance(getActivity()).getImageLoader().get(product.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                   image.setImageBitmap(response.getBitmap());
                }
            }
        });
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
