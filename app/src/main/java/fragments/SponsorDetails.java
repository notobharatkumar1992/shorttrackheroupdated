package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.DetailsAdapter;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.Race;
import model.RaceDetailModel;
import model.RankList;
import model.SponsorDetail;

/**
 * Created by admin on 30-06-2016.
 */
public class SponsorDetails extends Fragment implements OnReciveServerResponse {
    private View rootview;
    TextView company_name, phone, address, product;
    private Bundle bundle;
    private SponsorDetail sponsor;
    private Race raceModel;
    ArrayList<SponsorDetail> sponsorDetails;
    private String identifier;
    int sponsor_id = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.sponsor_details, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        sponsor_id = bundle.getInt(Tags.sponsor_id, 0);
        initializeObjects();
        if (sponsor_id == 0) {
            sponsor = bundle.getParcelable(Tags.sponsor_detail);
            setvalues();
        } else {
            execute_sponcerDetail(sponsor_id);
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Sponsor Details");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        company_name = (TextView) rootview.findViewById(R.id.company_name);
        phone = (TextView) rootview.findViewById(R.id.phone);
        address = (TextView) rootview.findViewById(R.id.address);
        product = (TextView) rootview.findViewById(R.id.product);
    }

    private void setvalues() {
        company_name.setText(sponsor.getCompany_name() + "");
        phone.setText(sponsor.getPhone() + "");
        address.setText(sponsor.getAddress() + "");
        final CircleImageView image = (CircleImageView) rootview.findViewById(R.id.image);
        AppDelegate.getInstance(getActivity()).getImageLoader().get(sponsor.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null)
                    image.setImageBitmap(response.getBitmap());
            }
        });
    }

    private void execute_sponcerDetail(int sponcer_id) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.sponsor_id, sponcer_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), SponsorDetails.this, ServerRequestConstants.SPONCER_DETAIL_DRIVER,
                            mPostArrayList, SponsorDetails.this);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), SponsorDetails.this, ServerRequestConstants.SPONCER_DETAIL,
                            mPostArrayList, SponsorDetails.this);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseSponcer_detail(String result) {
        Bundle bundle = new Bundle();
        try {
            ArrayList<SponsorDetail> sponsorDetails = new ArrayList<>();
            JSONObject obj = new JSONObject(result);
            sponsor = new SponsorDetail();
            int status = (obj.getInt("status"));
            if (status == 1 && (obj.getInt("dataFlow") == 1)) {
                JSONArray jsonArray = obj.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    sponsor = new SponsorDetail();
                    sponsor.setImage(jsonObject.getString("image"));
                    sponsor.setAddress(jsonObject.getString("address"));
                    sponsor.setCompany_name(jsonObject.getString("name"));
                    sponsor.setPhone(jsonObject.getString("phone_number"));
                    sponsorDetails.add(sponsor);
                }
                setvalues();

            } else {
                AppDelegate.ShowDialog(getActivity(), "No Sponsor found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SPONCER_DETAIL_DRIVER)) {
            parseSponcer_detail(result);
        } else if (apiName.equals(ServerRequestConstants.SPONCER_DETAIL)) {
            parseSponcer_detail(result);
        }
    }
}
