package fragments;

import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import ExpendibleRecyclerView.Adapter;
import ExpendibleRecyclerView.ChildGetterSetter;
import ExpendibleRecyclerView.Data;
import ExpendibleRecyclerView.GetterSetterClass;
import UI.MyViewPager;
import adapters.ImageThumbnailAdapter;
import adapters.SponcerBannerAdapter;
import adapters.TrackAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnExpandCollapseListener;
import interfaces.OnReciveServerResponse;
import model.DashboardModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.RaceModel;
import model.SponcerBannerModel;
import model.SponcerModel;
import model.TrackListModel;
import model.TrackModel;


/**
 * Created by admin on 16-06-2016.
 */
public class DashBoardFan extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnExpandCollapseListener, OnDialogClickListener {
    View rootview;

    TextView news;
    private String device_id;
    private int status, user_id;

    private TextView save;
    private LoginModel getProfile_model;
    private Get_News_Model newsModel;
    private String track_id;
    private RecyclerView competitionList;
    private Bundle savedInstanceState;
    ArrayList<Data> structures;
    private Data data = null;
    ArrayList<Object> childList;
    private Adapter crimeExpandableAdapter;
    private DashboardModel dashboard_Model;
    private RaceModel race_Model;
    private SponcerModel sponcerModel;
    private Handler mHandler = new Handler();
    private TextView track_name, edit;
    private ListView track;
    private TrackListModel tracklistitem;
    private String trackName;
    private int trackId;
    private TrackModel trackModel;
    private TextView savetrack;
    private Dialog selectTrack;

    private MyViewPager sponcer;
    private HashMap<Integer, ArrayList<SponcerModel>> arrayListHashMap = new HashMap<>();
    ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
    private SponcerBannerModel value = new SponcerBannerModel();
    private List<Fragment> bannerFragment = new ArrayList();
    private PagerAdapter bannerPagerAdapter;
    private Cursor mImageCursor;
    private ImageThumbnailAdapter mAdapter;
    private TwoWayGridView mImageGrid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.dashboard, container, false);
        AppDelegate.Log("homedash", "driver");
        return rootview;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        track_id = AppDelegate.getValue(getActivity(), Tags.TRACK_ID);
        initializeObjects();
        if (AppDelegate.isValidString(track_id)) {

        } else {
            AppDelegate.ShowDialogID(getActivity(), "Please Select track.", "Alert!!!", "selectTrack", this);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        get_news();
        getdashboard();
        // parse_Track();
    }

    private void parse_Track() {
        String track_response = AppDelegate.getValue(getActivity(), Parameters.change_track);
        try {
            JSONObject object = new JSONObject(track_response);
            trackModel = new TrackModel();
            trackModel.setStatus(object.getInt("status"));
            trackModel.setDataflow(object.getInt("dataFlow"));
            if (trackModel.getStatus() == 1) {
//                JSONObject response = object.getJSONObject("response");
//                trackModel.setId(response.getInt("id"));
//                trackModel.setUser_id(response.getInt("user_id"));
//                trackModel.setTrack_id(response.getInt("track_id"));
//                trackModel.setTotal_credit(response.getInt("total_credit"));
//                trackModel.setTotal_point(response.getInt("total_point"));
//                trackModel.setMessage(object.getString("message"));
                AppDelegate.save(getActivity(), String.valueOf(trackId), Tags.TRACK_ID);
                getdashboard();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getdashboard() {
        sponcer.setAdapter(null);
        try {
            track_id = AppDelegate.getValue(getActivity(), Tags.TRACK_ID);
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, Integer.parseInt(track_id), ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DashBoardFan.this, ServerRequestConstants.GET_DASHBOARD,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_news() {
        try {
            parseProfile();
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DashBoardFan.this, ServerRequestConstants.GET_NEWS,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initializeObjects() {
        sponcer = (MyViewPager) rootview.findViewById(R.id.banner_list);
        news = (TextView) rootview.findViewById(R.id.news);
        competitionList = (RecyclerView) rootview.findViewById(R.id.competition_list);
        track_name = (TextView) rootview.findViewById(R.id.track_name);
        edit = (TextView) rootview.findViewById(R.id.edit);
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        ImageView search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.GONE);
        edit.setOnClickListener(this);
        track_name.setText(AppDelegate.getValue(getActivity(), Tags.TRACK_NAME));
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.edit:

                changetrack();

                break;
        }
    }

    private void changetrack() {
        select_track();
    }

    private void select_track() {
        execute_TRACKLIST();
    }

    private void execute_TRACKLIST() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.TRACKLIST,
                        mPostArrayList, DashBoardFan.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Service Time out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.GET_DASHBOARD)) {
            AppDelegate.save(getActivity(), result, Parameters.get_dashboard);
            AppDelegate.Log("DashBoard", result);
            sponcer.setAdapter(null);
            parse_dashboard();
        } else if (apiName.equals(ServerRequestConstants.TRACKLIST)) {
            AppDelegate.save(getActivity(), result, Parameters.savetracklist);
            parseTracklist();
        } else if (apiName.equals(ServerRequestConstants.GET_DASHBOARD)) {
            AppDelegate.save(getActivity(), result, Parameters.get_dashboard);
            AppDelegate.Log("changeTrack", result);
            parse_changeTrack();
        }
    }

    private void parseTracklist() {
        String tracklist = AppDelegate.getValue(getActivity(), Parameters.savetracklist);
        try {
            JSONObject jsonObject = new JSONObject(tracklist);
            ArrayList<TrackListModel> trackListModels = new ArrayList<TrackListModel>();
            tracklistitem = new TrackListModel();
            tracklistitem.setStatus(jsonObject.getInt("status"));
            tracklistitem.setDataflow(jsonObject.getInt("dataFlow"));
            if (tracklistitem.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    tracklistitem = new TrackListModel();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    tracklistitem.setId(obj.getInt("id"));
                    tracklistitem.setCompany_name(obj.getString("company_name"));
                    tracklistitem.setPayment_method_id(obj.getInt("payment_method_id"));
                    tracklistitem.setCreated(obj.getString("created"));
                    tracklistitem.setUser_id(obj.getInt("id"));
                    trackListModels.add(tracklistitem);
                }
                trackset(trackListModels);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void trackset(final ArrayList<TrackListModel> trackListModels) {
        selectTrack = new Dialog(getActivity());
        selectTrack.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selectTrack.setContentView(R.layout.dialog_select__track);
        selectTrack.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        track = (ListView) selectTrack.findViewById(R.id.select_track);
        savetrack = (TextView) selectTrack.findViewById(R.id.save);
//        value = ".";
//        if (track_name.equals(".")) {
//            AppDelegate.ShowDialog(getActivity(), "Please select Track.", "Alert !!!");
//        } else {
//            savetrack.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    AppDelegate.showDialog_twoButtons(getActivity(), "Are you sure? ", "save", DashBoardFan.this);
//                    selectTrack.dismiss();
//                }
//            });
//        }
        TrackAdapter spinnerAdapter = new TrackAdapter(getActivity(), trackListModels);
        track.setAdapter(spinnerAdapter);
        track.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                trackName = trackListModels.get(position).getCompany_name();
                trackId = trackListModels.get(position).getId();
                savetrack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppDelegate.showDialog_twoButtons(getActivity(), "Are you sure? ", "Fansave", DashBoardFan.this);
                        selectTrack.dismiss();
                    }
                });
            }
        });
        selectTrack.show();
    }

    private void changeTrack() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.track_id, trackId, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(getActivity(), DashBoardFan.this, ServerRequestConstants.GET_DASHBOARD,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parse_changeTrack() {
        String track_response = AppDelegate.getValue(getActivity(), Parameters.get_dashboard);
        try {
            JSONObject object = new JSONObject(track_response);
            trackModel = new TrackModel();
            trackModel.setStatus(object.getInt("status"));
            trackModel.setDataflow(object.getInt("dataFlow"));
            if (trackModel.getStatus() == 1) {
//                JSONObject response = object.getJSONObject("response");
//                trackModel.setId(response.getInt("id"));
//                trackModel.setUser_id(response.getInt("user_id"));
//                trackModel.setTrack_id(response.getInt("track_id"));
//                trackModel.setTotal_credit(response.getInt("total_credit"));
//                trackModel.setTotal_point(response.getInt("total_point"));
//                trackModel.setMessage(object.getString("message"));

                //  get_news();
                getdashboard();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parse_dashboard() {
        competitionList.setAdapter(null);
        String dashboard = AppDelegate.getValue(getActivity(), Parameters.get_dashboard);
        try {
            ArrayList<DashboardModel> dashboardModelArrayList = new ArrayList<>();
            ArrayList<SponcerModel> sponcerModels = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(dashboard);
            dashboard_Model = new DashboardModel();
            dashboard_Model.setStatus(jsonObject.getInt("status"));
            dashboard_Model.setDataflow(jsonObject.getInt("dataFlow"));
            if (dashboard_Model.getStatus() == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("response");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ArrayList<RaceModel> raceModels = new ArrayList<>();
                    JSONObject obj = jsonArray.getJSONObject(i);
                    dashboard_Model = new DashboardModel();
                    dashboard_Model.setId(obj.getInt("id"));
                    dashboard_Model.setCompetition_name(obj.getString("name"));
                    JSONArray race = obj.getJSONArray("races");
                    for (int j = 0; j < race.length(); j++) {
                        JSONObject race_obj = race.getJSONObject(j);
                        race_Model = new RaceModel();
                        race_Model.setRace_id(race_obj.getInt("id"));
                        race_Model.setClass_race_name(race_obj.getString("name"));
                        race_Model.setCompetition_id(race_obj.getInt("competition_id"));
                        raceModels.add(race_Model);
                    }
                    dashboard_Model.setRaces(raceModels);
                    dashboardModelArrayList.add(dashboard_Model);
                }
                getCompetitionList(dashboardModelArrayList);
                JSONArray sponcer = null;
                try {
                    sponcer = jsonObject.getJSONArray("sponcer");
                    sponcerModel = new SponcerModel();
                    for (int i = 0; i < sponcer.length(); i++) {
                        JSONObject race_obj = sponcer.getJSONObject(i);
                        sponcerModel = new SponcerModel();
                        sponcerModel.setSponcer_id(race_obj.getInt("id"));
                        sponcerModel.setSponcer_name(race_obj.getString("sponcer_name"));
                        sponcerModel.setSponcer_img(race_obj.getString("image"));
                        sponcerModels.add(sponcerModel);

                    }
                    showSponcer(sponcerModels);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillChooserArray(List<SponcerModel> sponcer) {
        int position = -1;
        ArrayList<SponcerModel> models = new ArrayList<>();
        for (int i = 0; i < sponcer.size(); i++) {
            if (i % 6 == 0) {
                position++;
                models = new ArrayList<>();
            }
            models.add(sponcer.get(i));
            AppDelegate.Log("get", models + "");
            arrayListHashMap.put(position, models);
        }
        arrayFavCategoModelList.clear();
        for (int i = 0; i < arrayListHashMap.size(); i++) {
            arrayFavCategoModelList.add(new SponcerBannerModel(arrayListHashMap.get(i)));
            AppDelegate.LogT("arrayListHashMap pos = " + i + ", size  = " + arrayListHashMap.get(i).size());
        }
        AppDelegate.LogT("arrayFavCategoModelList = " + arrayFavCategoModelList.size() + ", arrayListHashMap = " + arrayListHashMap.size());
        // mHandler.sendEmptyMessage(2);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        fillChooserArray(sponcerModels);
        bannerFragment.clear();
        for (int i = 0; i < arrayFavCategoModelList.size(); i++) {
            Fragment fragment = new BannerHomeFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.slider_id, arrayFavCategoModelList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        sponcer.setOffscreenPageLimit(bannerFragment.size());
        sponcer.setAdapter(bannerPagerAdapter);
        timeOutLoop();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    int value = sponcer.getCurrentItem();
                    if (bannerFragment.size() == 0) {
                        return;
                    } else {
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                            sponcer.setCurrentItem(value, false);
                        } else {
                            value++;
                            sponcer.setCurrentItem(value, true);
                        }
                        if (isAdded()) {
                            if (sponcer.getCurrentItem() == 0) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.postDelayed(this, 7000);
                            }
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            AppDelegate.LogT("String" + ssb.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setFocusable(true);
                news.requestFocus();
                news.setSelected(true);
                //  news.setText(ssb, TextView.BufferType.SPANNABLE);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    void getCompetitionList(ArrayList<DashboardModel> dashboardModelArrayList) {
        setStructures(dashboardModelArrayList);
        competitionList.setHasFixedSize(true);
        competitionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        crimeExpandableAdapter = new Adapter(getActivity(), generateCrimes(getActivity(), structures), structures, this);
        crimeExpandableAdapter.onRestoreInstanceState(savedInstanceState);
        competitionList.setAdapter(crimeExpandableAdapter);

    }

    private void call() {
        competitionList.post(new Runnable() {
            @Override
            public void run() {
                competitionList.smoothScrollToPosition(crimeExpandableAdapter.getItemCount());
            }
        });
       /* final int speedScroll = 150;
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;
            @Override
            public void run() {
                if(count <competitionList.getChildCount()){
                    competitionList.scrollToPosition(++count);
                    handler.postDelayed(this,speedScroll);
                }
            }
        };

        handler.postDelayed(runnable,speedScroll);*/
    }


    private void setStructures(ArrayList<DashboardModel> dashboardModelArrayList) {
        structures = new ArrayList<>();
        AppDelegate.Log("ArrayList", dashboardModelArrayList + "");
        for (int i = 0; i < dashboardModelArrayList.size(); i++) {
            data = new Data();
            childList = new ArrayList<>();
            data.setTitle(dashboardModelArrayList.get(i).getCompetition_name());
            ArrayList<RaceModel> races = dashboardModelArrayList.get(i).getRaces();
            for (int j = 0; j < races.size(); j++) {
                childList.add(new ChildGetterSetter(races.get(j).getClass_race_name(), races.get(j).getRace_id(), races.get(j).getCompetition_id()));
            }
            data.setChildObjectList(childList);
            structures.add(data);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            if (!competitionList.getAdapter().equals(null)) {
                ((Adapter) competitionList.getAdapter()).onSaveInstanceState(outState);
            }
        } catch (NullPointerException e) {
        }
    }

    public ArrayList<ParentListItem> generateCrimes(Context mContext, ArrayList<Data> childList) {
        GetterSetterClass crimeLab = GetterSetterClass.get(mContext, childList);
        List<Data> crimes = crimeLab.getCrimes();
        Iterator itr = crimes.iterator();
        ArrayList<ParentListItem> parentObjects = new ArrayList<>();
        while (itr.hasNext()) {
            Data data = (Data) itr.next();
            data.setChildObjectList(data.getChildItemList());
            parentObjects.add(data);
        }
        return parentObjects;
    }

    @Override
    public void setOnExpand(String apiName, int position, ParentListItem parentListItem) {
        for (Data data : structures) {
            data.isExpend = 0;
        }
        structures.get(position).isExpend = 1;
        crimeExpandableAdapter.notifyDataSetChanged();
        competitionList.invalidate();
    }

    @Override
    public void setOnCollapse(String apiName, int position) {
        /*crimeExpandableAdapter.notifyDataSetChanged();
        competitionList.invalidate();*/
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals("Fansave")) {
            AppDelegate.save(getActivity(), String.valueOf(trackId), Tags.TRACK_ID);
            AppDelegate.save(getActivity(), trackName, Tags.TRACK_NAME);
            track_name.setText(trackName);
            changeTrack();
            //AppDelegate.showToast(getActivity(),"Hellooo");
        } else if (name.equalsIgnoreCase("selectTrack")) {
            changetrack();
        }
    }
}
