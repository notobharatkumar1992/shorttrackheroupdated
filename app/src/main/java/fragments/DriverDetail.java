package fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.HomeScreen;
import com.noto.shorttrackhero.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import UI.MyViewPager;
import adapters.DriverGamesAdapter;
import adapters.FanListAdapter;
import adapters.PointshistoryAdapter;
import adapters.SponcerBannerAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.DriverDetailModel;
import model.DriverGames;
import model.FanModel;
import model.Get_News_Model;
import model.LoginModel;
import model.PostAysnc_Model;
import model.RaceDetailModel;
import model.SponcerBannerModel;
import model.SponcerModel;
import utils.Constant;

/**
 * Created by admin on 30-06-2016.
 */
public class DriverDetail extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnDialogClickListener {
    private View rootview;

    private Bundle bundle;
    private RaceDetailModel racedetailModel;

    private Driver driver;

    private RecyclerView fanlist;
    CircleImageView driver_img;
    TextView driver_name, fans, total_races, total_wins;
    private FanModel fanModel;
    private int status;
    LoginModel getProfile_model;
    private int user_id;
    private Get_News_Model newsModel;
    private TextView news;
    private SponcerModel sponcerModel;
    DriverDetailModel driverDetailModel;

    private TextView buy, date1, comp1, laps1, points1, position1, date2, comp2, laps2, points2, position2, date3, comp3, laps3, points3, position3, date4, comp4, laps4, points4, position4, date5, comp5, laps5, points5, position5;
    TextView points, twitter, facebook, rank, races, win;
    private DriverGames driverGames;
    private RecyclerView drivergameRecycler;
    private Dialog builder;
    private String identifier;
    private ImageView search;
    private MyViewPager sponcer;
    private HashMap<Integer, ArrayList<SponcerModel>> arrayListHashMap = new HashMap<>();
    ArrayList<SponcerBannerModel> arrayFavCategoModelList = new ArrayList<>();
    private SponcerBannerModel value = new SponcerBannerModel();
    private List<Fragment> bannerFragment = new ArrayList();
    private PagerAdapter bannerPagerAdapter;
    private Handler mHandler = new Handler();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.driver_detail, container, false);
        identifier = AppDelegate.getValue(getActivity(), Tags.IDENTIFIER);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();
        driver = bundle.getParcelable(Tags.parcel_FanDetail);
        initializeObjects();
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(getActivity(), Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void get_news() {
        parseProfile();
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.DRIVER_GET_NEWS,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.GET_NEWS,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void initializeObjects() {
        TextView txttitle = (TextView) HomeScreen.toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        TextView save = (TextView) HomeScreen.toolbar.findViewById(R.id.save);
        save.setVisibility(View.GONE);
        search = (ImageView) HomeScreen.toolbar.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSearch();
            }
        });
        news = (TextView) rootview.findViewById(R.id.news);
        driver_img = (CircleImageView) rootview.findViewById(R.id.driver_img);
        driver_name = (TextView) rootview.findViewById(R.id.driver_name);
        buy = (TextView) rootview.findViewById(R.id.buy);
        rank = (TextView) rootview.findViewById(R.id.rank);
        sponcer = (MyViewPager) rootview.findViewById(R.id.banner_list);
        points = (TextView) rootview.findViewById(R.id.points);
        twitter = (TextView) rootview.findViewById(R.id.twitter);
        facebook = (TextView) rootview.findViewById(R.id.facebook);
        races = (TextView) rootview.findViewById(R.id.race);
        win = (TextView) rootview.findViewById(R.id.wins);
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            buy.setVisibility(View.GONE);
        } else {
            buy.setVisibility(View.VISIBLE);
            buy.setOnClickListener(this);
        }


    }

    private void callSearch() {
        Bundle bundle = new Bundle();
        bundle.putInt("driver_id", driver.getDriver_id());
        AppDelegate.showFragment(getActivity(), new Search_Sponcers(), R.id.framelayout, bundle, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setValues();
        get_news();
        getdriverDetails();
        if (bundle.isEmpty()) {

        } else {
            //  getfanlist();
        }
    }

    private void getdriverDetails() {
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver.getDriver_id(), ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.DRIVER_DRIVER_DETAILS,
                                mPostArrayList, null);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.DRIVER_DETAILS,
                                mPostArrayList, null);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void getfanlist() {
        try {
            {
                if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                    ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver.getDriver_id(), ServerRequestConstants.Key_PostintValue);
                    PostAsync mPostAsyncObj;
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.DRIVER_FAN_LIST,
                                mPostArrayList, null);
                    } else {
                        mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.FAN_LIST,
                                mPostArrayList, null);
                    }
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void setValues() {
        driver_name.setText(driver.getUsername() + "");
        races.setText("" + driver.getTotal_race());
        win.setText("" + driver.getTotal_win());
        rank.setText(driver.getCurrent_rank() + "");
        AppDelegate.getInstance(getActivity()).getImageLoader().get(driver.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), "Time out!!!Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FAN_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_list);
            parseFan_List();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_FAN_LIST)) {
            AppDelegate.save(getActivity(), result, Parameters.fan_list);
            parseFan_List();
        } else if (apiName.equals(ServerRequestConstants.GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_NEWS)) {
            AppDelegate.save(getActivity(), result, Parameters.get_news);
            AppDelegate.Log("NEWS", result);
            parse_news();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_DETAILS)) {
            AppDelegate.save(getActivity(), result, Parameters.driver_details);
            AppDelegate.Log("DRIVER", result);
            parse_driverDetails(result);
        } else if (apiName.equals(ServerRequestConstants.DRIVER_DRIVER_DETAILS)) {
            //  AppDelegate.save(getActivity(), result, Parameters.driver_details);
            AppDelegate.Log("DRIVER", result);
            parse_driverDetails(result);
        } else if (apiName.equals(ServerRequestConstants.PURCHASE_DRIVER)) {
            AppDelegate.save(getActivity(), result, Parameters.purchase_driver);
            AppDelegate.Log("purchase driver", result);
            parse_purchaseDriver();
        }
    }

    private void parse_purchaseDriver() {
        String purchase = AppDelegate.getValue(getActivity(), Parameters.purchase_driver);
        try {
            JSONObject jsonObject = new JSONObject(purchase);
            String message;
            status = jsonObject.getInt("status");
            if (status == 1) {
                message = jsonObject.getString("response");

                try {
                    RaceDetail.fan_points = (TextView) RaceDetail.rootview.findViewById(R.id.fan_points);
                    AppDelegate.save(getActivity(), String.valueOf(jsonObject.getInt("newPoint")), "fanpoints");
                    // AppDelegate.getValue(getActivity(),"fanpoints");
                    //  fan_points.setText( AppDelegate.getValue(context,"fanpoints")+"");
                    // RaceDetail.fan_points.setText(jsonObject.getInt("newPoint"));
                    RaceDetail.fan_points.setText(AppDelegate.getValue(getActivity(), "fanpoints") + "");
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
//                TextView fan_points=(TextView)getActivity().findViewById(R.id.fan_points);
//                fan_points.setText( jsonObject.getInt("newPoint"));
                //  RaceDetail.fan_points.setText( jsonObject.getInt("newPoint"));
            } else {
                try {
                   /* RaceDetail.fan_points=(TextView)RaceDetail.rootview.findViewById(R.id.fan_points);
                    AppDelegate.save(getActivity(), String.valueOf(15), "fanpoints");
                    // AppDelegate.getValue(getActivity(),"fanpoints");
                    //  fan_points.setText( AppDelegate.getValue(context,"fanpoints")+"");
                    // RaceDetail.fan_points.setText(jsonObject.getInt("newPoint"));
                    RaceDetail.fan_points.setText(AppDelegate.getValue(getActivity(), "fanpoints") + "");*/
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                message = jsonObject.getString("message");
            }
            // AppDelegate.ShowDialog(ForgotPassword.this, message + "", "Alert!!!");
            AppDelegate.ShowDialogID(getActivity(), message + "", "Alert!!!", "ForgotPassword", this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parse_driverDetails(String result) {
        String driver_response = AppDelegate.getValue(getActivity(), Parameters.driver_details);
        try {
            JSONObject jsonObject = new JSONObject(result);
            sponcerModel = new SponcerModel();
            driverDetailModel = new DriverDetailModel();
            driverDetailModel.setStatus(jsonObject.getInt("status"));
            if (driverDetailModel.getStatus() == 1) {
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("sponcer");
                    ArrayList<SponcerModel> sponcerModelArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        sponcerModel = new SponcerModel();
                        if (obj.has("sponcer_id")) {
                            sponcerModel.setSponcer_id(obj.getInt("sponcer_id"));
                        } else {
                            sponcerModel.setSponcer_id(0);
                        }
                        sponcerModel.setSponcer_img(obj.getString("image"));
                        sponcerModel.setSponcer_name(obj.getString("sponcer_name"));
                        sponcerModelArrayList.add(sponcerModel);
                    }
                    driverDetailModel.setSponcer(sponcerModelArrayList);
                    setdriverSponcer(sponcerModelArrayList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject response = jsonObject.getJSONObject("response");
                    JSONObject jsonObj = response.getJSONObject("Driver");

                    driver = new Driver();
                    driver.setDriver_id(jsonObj.getInt("id"));
                    driver.setUsername(jsonObj.getString("username"));
                    driver.setCurrent_rank(jsonObj.getInt("current_rank"));
                    driver.setTotal_race(jsonObj.getInt("total_race"));
                    driver.setTotal_win(jsonObj.getString("total_win"));
                    driver.setDriver_img(jsonObj.getString("image"));
                    driver.setFacebook(jsonObj.getString("facebook"));
                    driver.setTwitter(jsonObj.getString("twitter"));
                    setValue();
                    driverDetailModel.setDriver(driver);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject response = jsonObject.getJSONObject("response");
                    JSONArray json = response.getJSONArray("Games");
                    ArrayList<DriverGames> driverGamesArrayList = new ArrayList<>();
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObj = json.getJSONObject(i);
                        driverGames = new DriverGames();
                        driverGames.setCompetition_name(jsonObj.getString("competition name"));
                        driverGames.setLap(jsonObj.getString("lap"));
                        driverGames.setStart_date(jsonObj.getString("start_date"));
                        AppDelegate.Log("TAg1", driverGames.getStart_date() + "");
                        driverGames.setTotal_credit(jsonObj.getInt("total_credit"));
                        AppDelegate.Log("TAg2", driverGames.getTotal_credit() + "");
                        driverGames.setTotal_point(jsonObj.getInt("total_point"));
                        AppDelegate.Log("TAg3", driverGames.getTotal_point() + "");
                        driverGamesArrayList.add(driverGames);
                    }
                    driverDetailModel.setDriverGames(driverGamesArrayList);
                    setdrivergames(driverGamesArrayList);
                    AppDelegate.LogT("driverGamesArrayList" + driverGamesArrayList.size() + "");
                    AppDelegate.LogT("driver detail model" + driverDetailModel + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setdrivergames(ArrayList<DriverGames> driverGamesArrayList) {
        drivergameRecycler = (RecyclerView) rootview.findViewById(R.id.comp);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        DriverGamesAdapter adapter = new DriverGamesAdapter(getActivity(), driverGamesArrayList);
        drivergameRecycler.setLayoutManager(layoutManager);
        AppDelegate.LogT("set adapter" + adapter + "adapter");
        drivergameRecycler.setAdapter(adapter);
        AppDelegate.LogT("set " + adapter + "adapter");
    }

    private void setValue() {
        driver_name.setText(driver.getUsername() + "");
        races.setText(driver.getTotal_race() + "");
        win.setText(driver.getTotal_win() + "");
        rank.setText(driver.getCurrent_rank() + "");
        // points.setText(driver.getTotal_point() + "");
        AppDelegate.getInstance(getActivity()).getImageLoader().get(driver.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
        facebook.setText(driver.getFacebook() + "");
        twitter.setText(driver.getTwitter() + "");
        if (!facebook.getText().toString().isEmpty()) {
            facebook.setOnClickListener(this);
        }
        if (!twitter.getText().toString().isEmpty()) {
            twitter.setOnClickListener(this);
        }
    }

    /* private void setdriverSponcer(ArrayList<SponcerModel> sponcerModelArrayList) {

         LinearLayoutManager layoutManager
                 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
         SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModelArrayList);
         sponcer.setLayoutManager(layoutManager);
         sponcer.setAdapter(adapter);
     }*/
    private void fillChooserArray(List<SponcerModel> sponcer) {
        int position = -1;
        ArrayList<SponcerModel> models = new ArrayList<>();
        for (int i = 0; i < sponcer.size(); i++) {
            if (i % 6 == 0) {
                position++;
                models = new ArrayList<>();
            }
            models.add(sponcer.get(i));
            AppDelegate.Log("get", models + "");
            arrayListHashMap.put(position, models);
        }
        arrayFavCategoModelList.clear();
        for (int i = 0; i < arrayListHashMap.size(); i++) {
            arrayFavCategoModelList.add(new SponcerBannerModel(arrayListHashMap.get(i)));
            AppDelegate.LogT("arrayListHashMap pos = " + i + ", size  = " + arrayListHashMap.get(i).size());
        }
        AppDelegate.LogT("arrayFavCategoModelList = " + arrayFavCategoModelList.size() + ", arrayListHashMap = " + arrayListHashMap.size());
        // mHandler.sendEmptyMessage(2);
    }

    private void setdriverSponcer(ArrayList<SponcerModel> sponcerModels) {
        fillChooserArray(sponcerModels);
        bannerFragment.clear();
        for (int i = 0; i < arrayFavCategoModelList.size(); i++) {
            Fragment fragment = new BannerHomeFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.slider_id, arrayFavCategoModelList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        sponcer.setAdapter(bannerPagerAdapter);
        timeOutLoop();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    int value = sponcer.getCurrentItem();
                    if (bannerFragment.size() == 0) {
                        return;
                    } else {
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                            sponcer.setCurrentItem(value, false);
                        } else {
                            value++;
                            sponcer.setCurrentItem(value, true);
                        }
                        if (isAdded()) {
                            if (sponcer.getCurrentItem() == 0) {
                                mHandler.postDelayed(this, 3000);
                            } else {
                                mHandler.postDelayed(this, 7000);
                            }
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void parse_news() {
        String Citylist = AppDelegate.getValue(getActivity(), Parameters.get_news);
        try {
            JSONObject jsonObject = new JSONObject(Citylist);
            newsModel = new Get_News_Model();
            newsModel.setStatus(jsonObject.getInt("status"));
            newsModel.setDataflow(jsonObject.getInt("dataFlow"));
            if (newsModel.getStatus() == 1) {
                JSONObject jsonArray = jsonObject.getJSONObject("response");
                newsModel.setDescription(jsonArray.getString("Description"));
                SpannableStringBuilder ssb = new SpannableStringBuilder("");
                String[] newsArray = (newsModel.getDescription() + "").split("\\%", -1);
                int counter = 0;
                Log.d("test", "newsArray" + newsArray[0] + "news => " + newsArray.length);
                for (int i = 0; i <= newsArray.length - 1; i++) {
                    if (newsArray[i].length() > 0) {
                        Log.d("test", "text => " + newsArray[i] + " , " + newsArray[i].length() + ", i length = " + i + ", counter = " + counter);
                        ssb.append(newsArray[i] + " ");
                        counter += newsArray[i].length() + 1;
                        Bitmap smiley = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
                        try {
                            if (counter > 0)
                                ssb.setSpan(new ImageSpan(smiley, ImageSpan.ALIGN_BASELINE), counter - 1, counter, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                news.setSelected(true);
                news.setText(ssb);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseFan_List() {
        String Fan_list = AppDelegate.getValue(getActivity(), Parameters.fan_list);
        try {
            ArrayList<FanModel> fanList = new ArrayList<>();
            JSONObject object = new JSONObject(Fan_list);
            fanModel = new FanModel();
            fanModel.setStatus(object.getInt("status"));
            fanModel.setDataFlow(object.getInt("dataFlow"));
            if (fanModel.getStatus() == 1) {
                JSONObject response = object.getJSONObject("response");
                JSONArray FAN = response.getJSONArray("Fan List");
                for (int i = 0; i < FAN.length(); i++) {
                    JSONObject FanDetail = FAN.getJSONObject(i);
                    fanModel = new FanModel();
                    fanModel.setFan_id(FanDetail.getInt("Fan_id"));
                    fanModel.setFan_name(FanDetail.getString("name"));
                    fanModel.setFan_image(FanDetail.getString("image"));
                    fanModel.setCreated(FanDetail.getString("created"));
                    fanList.add(fanModel);
                }
                setFanLIST(fanList);
            } else {
                AppDelegate.ShowDialog(getActivity(), "No record found.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFanLIST(ArrayList<FanModel> fanList) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        FanListAdapter adapter = new FanListAdapter(getActivity(), fanList);
        fanlist.setLayoutManager(layoutManager);
        fanlist.setAdapter(adapter);
    }

    private void showSponcer(final ArrayList<SponcerModel> sponcerModels) {
        /*LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        SponcerBannerAdapter adapter = new SponcerBannerAdapter(getActivity(), sponcerModels);
        banners.setLayoutManager(layoutManager);
        banners.setAdapter(adapter);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook:
                String url = facebook.getText().toString();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.twitter:
                url = facebook.getText().toString();
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.buy:
                AppDelegate.showDialog_twoButtons(getActivity(), "Are you sure. ", Tags.buydriver, this);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals(Tags.buydriver)) {
            execute_buydriver();
            //AppDelegate.showToast(getActivity(),"Hellooo");
        }
    }

    private void execute_buydriver() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver.getDriver_id(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.competition_id, Integer.parseInt(AppDelegate.getValue(getActivity(), Tags.competitionID)), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.race_id, Integer.parseInt(AppDelegate.getValue(getActivity(), Tags.raceID)), ServerRequestConstants.Key_PostintValue);
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {

                } else {
                    PostAsync mPostAsyncObj = new PostAsync(getActivity(), DriverDetail.this, ServerRequestConstants.PURCHASE_DRIVER,
                            mPostArrayList, null);
                    AppDelegate.showProgressDialog(getActivity());
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }
}
