package Constants;

/**
 * Created by admin on 16-06-2016.
 */
public class Parameters {
    public static final String API_KEY = "apiKey";
    public static final String API_KEY_VALUE = "1254kloi";
    public static final String username = "username";
    public static final String email = "email";
    public static final String mobile = "mobile";
    public static final String confirmpassword = "confirmpassword";
    public static final String password = "password";
    public static final String post_code = "post_code";
    public static final String device_type = "device_type";
    public static final String device_id = "device_id";
    public static final String country_id = "country_id";
    public static final String state_id = "state_id";
    public static final String city_id = "city_id";
    public static final String track_id = "track_id";
    public static final String apiKey = "apiKey";
    public static final String uploadedfile0 = "uploadedfile0";
    public static final String savetracklist = "savetracklist";
    public static final String savestatelist = "savestatelist";
    public static final String savecountrylist = "savecountrylist";
    public static final String savecitylist = "savecitylist";
    public static  final String Registerdriver="register_driver";
    public static final String RegisterFan="register_fan";
    public static final String saveForgot="saveForgot";
    public static final String saveLogin="saveLogin";
    public static final String device_token="device_token";
    public static final String login_type="login_type";
    public static final String user_id="user_id";
    public static final int login_type_forfan=3;
    public static final int login_type_fordriver=4;
    public static final String get_profile="get_profile";
    public static final String editfan="editfan";
    public static final String Change_Password="Change_Password";
    public static final String get_news="get_news";
    public static final String get_dashboard="get_dashboard";
    public static final String change_track="change_track";
    public static final String comp_id="comp_id";
    public static final String race_id="race_id";
    public static final String race_details="race_details";
    public static final String driver_id="driver_id";
    public static final String fan_list="fan_list";
    public static final String driver_details="driver_details";
    public static final String product_list="product_list";
    public static final String id="id";
    public static final String product_detail="product_detail";
    public static final String city="city";
    public static final String address="address";
    public static final String quantity="quantity";
    public static final  String product_id="product_id";
    public static final String price="price";
    public static final String purchase_product="purchase_product";
    public static final String contact="contact";
    public static final String name="name";
    public static final String fan_pointsHistory="fan_pointsHistory";
    public static final String fan_ShopsHistory="fan_ShopsHistory";
    public static final String purchase_driver="purchase_driver";
    public static final String competition_id="competition_id";
    public static final String car_number="car_number";
    public static final String refer_friend="refer_friend";
    public static final String fb_link="fb_link";
    public static final String tw_link="tw_link";
    public static final String insta_link="insta_link";
    public static final String about_me="about_me";
    public static final String achievements="achievements";
    public static final String interested="interested";
    public static final String gameHistory="gameHistory";
    public static final String category_id="category_id";
    public static final String subcat_id="subcat_id";
    public static final String sponcer_category="sponcer_category";
    public static final String sponcer_subcategory="sponcer_subcategory";
    public static final String add_Sponcer="add_Sponcer";
    public static final String cat_id="cat_id";
    public static final String sponcer_list="sponcer_list";
    public static final String search_sponcer_category="search_sponcer_category";
    public static final String savevdo="savevdo";
    public static final String notification="notification";
    public static final String checkin="checkin";
    public static final String editprofilefan="editprofilefan";
    public static final String editprofiledriver="editprofiledriver";
    public static final String refer_code="refer_code";
    public static final String sponsor_id="sponsor_id";
    public static final String keyword="keyword";
}
