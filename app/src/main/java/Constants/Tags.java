package Constants;

/**
 * Created by admin on 26-05-2016.
 */
public class Tags {
    public static final String TAG_LAT = "lat";
    public static final String TAG_LONG = "long";
    public static final String TAG_LOGIN = "LoginResponse";
    public static final String TAG_SIGNUP = "signupResponse";
    public static final String ERROR = "error";
    public static final String BRANCH = "";
    public static final String PROVINCE = "province";
    public static final String CITY = "city";
    public static final String LOCATION = "location";
    public static final String AUTOPROVINCE = "autoprovince";
    public static final String AUTOCITY = "autocity";
    public static final String AUTOLOCATION = "autolocation";
    public static final String TAG_MYVEHICLELIST = "myvehiclelist";
    public static final String TAGMAKE = "make";
    public static final String EDITPROFILE = "editprofile";
    public static final String TAG_SELECTVEHICLE = "selectvehicle";
    public static final String URL = "url";
    public static final String URL_API = "urlApi";
    public static final String BHARAT = "Bharat";
    public static final String DATE = "Date";
    public static final String DATABASE = "Database";

    public static final String URL_RESPONSE = "urlResponse";
    public static final String URL_POST = "urlPost";
    public static final String TEST = "test";
    public static final String TRACKER = "Tracker";
    public static final String FORMATED = "formatted";
    public static final String SERVICE = "service";

    public static final String RESULT = "result";
    public static final String INTERNET = "intertnet";

    public static final String PREF = "pref";

    public static final String GCM = "GCM";

    public static final String GPS = "GPS";

    public static final String NULL = "null";
    public static final String FACEBOOK = "facebook";


    public static final String PUSHER = "pusher";

    public static final String POST = "post";

    public static final String user_id = "user_id";

    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String city = "city";

    public static final String result = "result";
    public static final String location = "location";


    public static final String status = "status";


    public static final String size = "size";
    public static final String content = "content";
    public static final String value = "value";
    public static final String exception = "exception";
    public static final String parent = "parent";
    public static final String name = "name";
    public static final String message = "message";
    public static final String last_name = "last_name";
    public static final String PUBNUB = "pubnub";
    public static final String FILE = "FILE";
    public static final String address = "address";
    public static final String TAG_Coupan = "Coupan_List";
    public static final String TAG_REDEEM = "redeem";
    public static final String REQUEST_RESPONSE = "responserequest";
    public static final String THANKS = "thanks";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String PLACE_NAME = "PLACE_NAME";
    public static final String PLACE_ADD = "PLACE_ADD";
    public static final String ADDRESS = "ADDRESS";
    public static final String city_param = "city_param";
    public static final String country_param = "country_param";
    public static final String postalCode = "postalCode";
    public static final String STATE = "state";
    public static final String jpg = "jpg";
    public static final String png = "png";
    public static final String svg = "svg";
    public static final String Location = "location";
    public static final String IDENTIFY_DRIVER = "DRIVER";
    public static final String IDENTIFY_FAN = "FAN";
    public static final String IDENTIFIER ="IDENTIFIER" ;
    public static final String REGISTRATION_ID = "REGISTRATION_ID";
    public static final String TRACK_ID = "TRACK_ID";
    public static final String TRACK_NAME = "TRACK_NAME";
    public static final String LOGIN_STATUS ="Login_status" ;
    public static final String LOGIN ="LOGIN" ;
    public static final String LOGOUT ="LOGOUT" ;
    public static final String parcel_raceDetail="parcel_raceDetail";
    public static final String parcel_Detail="parcel_Detail";
    public static final String parcel_FanDetail="parcel_FanDetail";
    public static final String parcel_ProductDetail="parcel_ProductDetail";
    public static final String LIST_ITEM_TRENDING ="LIST_ITEM_TRENDING" ;
    public static final String buydriver="buydriver";
    public static final String competitionID="competitionID";
    public static final String raceID="raceID";
    public static final String parcel_shopHistoryItem="parcel_shopHistoryItem";
    public static final String parcel_pointsHistoryItem="parcel_pointsHistoryItem";
    public static final String parcel_gameHistoryItem="parcel_gameHistoryItem";
    public static final String driverEmail="driverEmail";
    public static final String driverPassword="driverPassword";
    public static final String FanEmail="driverEmail";
    public static final String fanPassword="driverPassword";
    public static final String slider_id="slider_id";
    public static final String sponsor_detail="sponsor_detail";
    public static final String parcel_racehistoryDetail="parcel_racehistoryDetail";
    public static final String MyDriverList="MyDriverList";
    public static final String DriverList="DriverList";
    public static final String sponsor_id="sponsor_id";
}
