package Constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */

	/*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";
    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";

    //public static final String BASE_URL = "http://192.168.100.81/short/webServices/";
    public static final String BASE_URL = "http://notosolutions.net/short_track/webServices/";
    public static final String BASE_URL_DRIVER = "http://notosolutions.net/short_track/webServicesDriver/";
    //public static final String BASE_URL_DRIVER = "http://192.168.100.81/short/webServicesDriver/";

    public static final String REGISTER_FAN = BASE_URL + "signup";
    public static final String REGISTER_DRIVER = BASE_URL_DRIVER + "DriversignUp";
    public static final String FORGOT_PASS_DRIVER = BASE_URL_DRIVER + "forgotPasswordDriver";
    public static final String GET_COUNTRY_LIST = BASE_URL + "getCountryList";
    public static final String GET_STATELIST = BASE_URL + "getStateList";
    public static final String GET_CITYLIST = BASE_URL + "getCityList";
    public static final String GET_PROFILE = BASE_URL + "profileDetails";
    public static final String DRIVER_GET_PROFILE = BASE_URL_DRIVER + "profileDetails";
    public static final String TRACKLIST = BASE_URL + "trackList";
    public static final String DRIVER_TRACKLIST = BASE_URL_DRIVER + "trackList";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgotpassword";
    public static final String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static final String LOGIN = BASE_URL + "login";
    public static final String DRIVER_LOGIN = BASE_URL_DRIVER + "Driverlogin";
    public static final String EDIT_PROFILE_FAN = BASE_URL + "editProfile";
    public static final String EDIT_PROFILE_DRIVER = BASE_URL_DRIVER + "editProfile";
    public static final String CHANGE_PASSWORD = BASE_URL + "changePassword";
    public static final String DRIVER_CHANGE_PASSWORD = BASE_URL_DRIVER + "changePassword";
    public static final String GET_NEWS = BASE_URL + "getNews";
    public static final String GET_DASHBOARD = BASE_URL + "getDashboard";
    public static final String DRIVER_GET_DASHBOARD = BASE_URL_DRIVER + "getDashboard";
    public static final String CHANGE_TRACK = BASE_URL + "getDashboard";
    public static final String DRIVER_CHANGE_TRACK = BASE_URL_DRIVER + "getDashboard";
    public static final String DRIVER_RACE_DETAILS = BASE_URL_DRIVER + "getRaceDetails";
    public static final String RACE_DETAILS = BASE_URL + "getRaceDetails";
    public static final String FAN_LIST = BASE_URL + "fanList";
    public static final String DRIVER_FAN_LIST = BASE_URL_DRIVER + "fanList";
    public static final String DRIVER_DETAILS = BASE_URL + "driverDetails";
    public static final String PRODUCT_LIST = BASE_URL + "productList";
    public static final String PRODUCT_DETAIL = BASE_URL + "productDetail";
    public static final String PURCHASE_PRODUCT = BASE_URL + "purchaseProduct";
    public static final String FAN_POINTS = BASE_URL + "fanPoints";
    public static final String SHOP_HISTORY = BASE_URL + "productHistory";
    public static final String PURCHASE_DRIVER = BASE_URL + "purchaseDriver";
    public static final String DRIVER_PURCHASE_DRIVER = BASE_URL_DRIVER + "purchaseDriver";
    public static final String REFER_A_FRIEND = BASE_URL + "GetReferfriend";
    public static final String LIKE = "https://www.facebook.com/Short-1009566702472573/";
    public static final String HELP = "http://notosolutions.net/short_track/pages/help";
    public static final String DRIVER_COMPETITION_INTERESTED = BASE_URL_DRIVER + "competitionIntersted";
    public static final String DRIVER_DRIVER_DETAILS = BASE_URL_DRIVER + "driverDetails";
    public static final String DRIVER_PRODUCT_LIST = BASE_URL_DRIVER + "productList";
    public static final String DRIVER_PRODUCT_DETAIL = BASE_URL_DRIVER + "productDetail";
    public static final String DRIVER_PURCHASE_PRODUCT = BASE_URL_DRIVER + "purchaseProduct";
    public static final String DRIVER_GET_NEWS = BASE_URL_DRIVER + "getNews";
    public static final String DRIVER_CHECKINnotification = BASE_URL_DRIVER + "drivercheckinminuteNotification";
    public static final String DRIVER_CHECKIN = BASE_URL_DRIVER + "checkin";
    public static final String DRIVER_SHOP_HISTORY = BASE_URL_DRIVER + "productHistory";
    public static final String DRIVER_GAME_HISTORY = BASE_URL_DRIVER + "getDriverGameHistory";
    public static final String video_url = "https://drive.google.com/file/d/0B5qvBdX_vkYTTEZDa2UxOS1pd1E/view?ts=578175bd";
    public static final String DRIVER_SPONCER_CATEGORY = BASE_URL_DRIVER + "getSponcerCategoryList";
    public static final String DRIVER_SPONCER_SUBCATEGORY = BASE_URL_DRIVER + "getSponcerSubcatList";
    public static final String DRIVER_ADD_SPONCER = BASE_URL_DRIVER + "addSponcer";
    public static final String DRIVER_SEARCH_SPONCER_CATEGORY = BASE_URL_DRIVER + "searchSponsorscat";
    public static final String SEARCH_SPONCER_CATEGORY = BASE_URL + "searchSponsorscat";
    public static final String DRIVER_SPONCER_LIST = BASE_URL_DRIVER + "DriverSponsorslist";
    public static final String SPONCER_LIST = BASE_URL + "DriverSponsorslist";
    public static final String DRIVER_VDO = BASE_URL_DRIVER + "getVideoUrl";
    public static final String DRIVER_COMPETITIONSEARCH_SPONCERCATEGORY = BASE_URL_DRIVER + "compsearchSponsorscat";
    public static final String COMPETITIONSEARCH_SPONCERCATEGORY = BASE_URL + "compsearchSponsorscat";
    public static final String DRIVER_COMPETITIONSPONCERLIST = BASE_URL_DRIVER + "CompSponsorslist";
    public static final String COMPETITIONSPONCERLIST = BASE_URL + "CompSponsorslist";
    public static final String DRIVER_NOTIFICATION = BASE_URL_DRIVER + "getNotificationsList";
    public static final String NOTIFICATION = BASE_URL + "getNotificationsList";

    public static final String SPONCER_DETAIL = BASE_URL + "getSponsorDetail";
    public static final String SPONCER_DETAIL_DRIVER = BASE_URL_DRIVER + "getSponsorDetail";
    public static final String RACE_HISTORY = BASE_URL + "getFanRaceList";
    public static final String RACE_HISTORY_DETAIL = BASE_URL + "getFanRaceDetail";
}