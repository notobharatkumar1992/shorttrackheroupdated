package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 03-08-2016.
 */
public class SponsorDetail implements Parcelable {
    int status;
    String image;
    int dataflow;
    int id;
    String company_name, phone, product, address;

    protected SponsorDetail(Parcel in) {
        status = in.readInt();
        image = in.readString();
        dataflow = in.readInt();
        id = in.readInt();
        company_name = in.readString();
        phone = in.readString();
        product = in.readString();
        address = in.readString();
    }

    public static final Creator<SponsorDetail> CREATOR = new Creator<SponsorDetail>() {
        @Override
        public SponsorDetail createFromParcel(Parcel in) {
            return new SponsorDetail(in);
        }

        @Override
        public SponsorDetail[] newArray(int size) {
            return new SponsorDetail[size];
        }
    };

    @Override
    public String toString() {
        return "SponsorDetail{" +
                "status=" + status +
                ", image='" + image + '\'' +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", company_name='" + company_name + '\'' +
                ", phone='" + phone + '\'' +
                ", product='" + product + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public SponsorDetail() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(image);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeString(company_name);
        dest.writeString(phone);
        dest.writeString(product);
        dest.writeString(address);
    }
}
