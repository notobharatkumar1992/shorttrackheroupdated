package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 29-06-2016.
 */
public class RaceDetailModel implements Parcelable {
    int status,dataflow;
   Race race;
    ArrayList<Driver> driver;
    ArrayList<SponcerModel> sponcer;

    public RaceDetailModel() {
    }

    protected RaceDetailModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        race = in.readParcelable(Race.class.getClassLoader());
        driver = in.createTypedArrayList(Driver.CREATOR);
        sponcer = in.createTypedArrayList(SponcerModel.CREATOR);
    }

    public static final Creator<RaceDetailModel> CREATOR = new Creator<RaceDetailModel>() {
        @Override
        public RaceDetailModel createFromParcel(Parcel in) {
            return new RaceDetailModel(in);
        }

        @Override
        public RaceDetailModel[] newArray(int size) {
            return new RaceDetailModel[size];
        }
    };

    @Override
    public String toString() {
        return "RaceDetailModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", race=" + race +
                ", driver=" + driver +
                ", sponcer=" + sponcer +
                '}';
    }
    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }


    public ArrayList<Driver> getDriver() {
        return driver;
    }

    public void setDriver(ArrayList<Driver> driver) {
        this.driver = driver;
    }

    public ArrayList<SponcerModel> getSponcer() {
        return sponcer;
    }

    public void setSponcer(ArrayList<SponcerModel> sponcer) {
        this.sponcer = sponcer;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeParcelable(race, flags);
        dest.writeTypedList(driver);
        dest.writeTypedList(sponcer);
    }
}
