package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 17-06-2016.
 */
public class TrackListModel implements Parcelable{
    int status,dataflow,id,user_id,payment_method_id;
    String company_name,created;

    public TrackListModel() {
    }
    protected TrackListModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        user_id = in.readInt();
        company_name = in.readString();
        payment_method_id = in.readInt();
        created = in.readString();
    }
    public static final Creator<TrackListModel> CREATOR = new Creator<TrackListModel>() {
        @Override
        public TrackListModel createFromParcel(Parcel in) {
            return new TrackListModel(in);
        }

        @Override
        public TrackListModel[] newArray(int size) {
            return new TrackListModel[size];
        }
    };

    @Override
    public String toString() {
        return "TrackListModel{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", user_id=" + user_id +
                ", company_name='" + company_name + '\'' +
                ", payment_method_id='" + payment_method_id + '\'' +
                ", created='" + created + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(int payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public static Creator<TrackListModel> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeString(company_name);
        dest.writeInt(payment_method_id);
        dest.writeString(created);
    }
}
