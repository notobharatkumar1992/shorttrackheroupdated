package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 29-06-2016.
 */
public class Driver implements Parcelable {
    int driver_id,position,fan_id;
    String username,driver_address,Driver_img,total_win;
    int total_race,current_rank,total_credit,total_point;
String facebook,twitter;
String purchase_point;

    protected Driver(Parcel in) {
        driver_id = in.readInt();
        position = in.readInt();
        fan_id = in.readInt();
        username = in.readString();
        driver_address = in.readString();
        Driver_img = in.readString();
        total_win = in.readString();
        total_race = in.readInt();
        current_rank = in.readInt();
        total_credit = in.readInt();
        total_point = in.readInt();
        facebook = in.readString();
        twitter = in.readString();
        purchase_point = in.readString();
    }

    public static final Creator<Driver> CREATOR = new Creator<Driver>() {
        @Override
        public Driver createFromParcel(Parcel in) {
            return new Driver(in);
        }

        @Override
        public Driver[] newArray(int size) {
            return new Driver[size];
        }
    };

    @Override
    public String toString() {
        return "Driver{" +
                "driver_id=" + driver_id +
                ", position=" + position +
                ", fan_id=" + fan_id +
                ", username='" + username + '\'' +
                ", driver_address='" + driver_address + '\'' +
                ", Driver_img='" + Driver_img + '\'' +
                ", total_win='" + total_win + '\'' +
                ", total_race=" + total_race +
                ", current_rank=" + current_rank +
                ", total_credit=" + total_credit +
                ", total_point=" + total_point +
                ", facebook='" + facebook + '\'' +
                ", twitter='" + twitter + '\'' +
                ", purchase_point='" + purchase_point + '\'' +
                '}';
    }

    public Driver() {
    }

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getFan_id() {
        return fan_id;
    }

    public void setFan_id(int fan_id) {
        this.fan_id = fan_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDriver_address() {
        return driver_address;
    }

    public void setDriver_address(String driver_address) {
        this.driver_address = driver_address;
    }

    public String getDriver_img() {
        return Driver_img;
    }

    public void setDriver_img(String driver_img) {
        Driver_img = driver_img;
    }

    public String getTotal_win() {
        return total_win;
    }

    public void setTotal_win(String total_win) {
        this.total_win = total_win;
    }

    public int getTotal_race() {
        return total_race;
    }

    public void setTotal_race(int total_race) {
        this.total_race = total_race;
    }

    public int getCurrent_rank() {
        return current_rank;
    }

    public void setCurrent_rank(int current_rank) {
        this.current_rank = current_rank;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getPurchase_point() {
        return purchase_point;
    }

    public void setPurchase_point(String purchase_point) {
        this.purchase_point = purchase_point;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(driver_id);
        dest.writeInt(position);
        dest.writeInt(fan_id);
        dest.writeString(username);
        dest.writeString(driver_address);
        dest.writeString(Driver_img);
        dest.writeString(total_win);
        dest.writeInt(total_race);
        dest.writeInt(current_rank);
        dest.writeInt(total_credit);
        dest.writeInt(total_point);
        dest.writeString(facebook);
        dest.writeString(twitter);
        dest.writeString(purchase_point);
    }
}
