package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 01-07-2016.
 */
public class FanModel  implements Parcelable{
    int fan_id,status,dataFlow;
    String fan_name, fan_image, created;

    protected FanModel(Parcel in) {
        fan_id = in.readInt();
        status = in.readInt();
        dataFlow = in.readInt();
        fan_name = in.readString();
        fan_image = in.readString();
        created = in.readString();
    }

    public static final Creator<FanModel> CREATOR = new Creator<FanModel>() {
        @Override
        public FanModel createFromParcel(Parcel in) {
            return new FanModel(in);
        }

        @Override
        public FanModel[] newArray(int size) {
            return new FanModel[size];
        }
    };

    @Override
    public String toString() {
        return "FanModel{" +
                "fan_id=" + fan_id +
                ", status=" + status +
                ", dataFlow=" + dataFlow +
                ", fan_name='" + fan_name + '\'' +
                ", fan_image='" + fan_image + '\'' +
                ", created='" + created + '\'' +
                '}';
    }

    public FanModel() {
    }

    public int getFan_id() {
        return fan_id;
    }

    public void setFan_id(int fan_id) {
        this.fan_id = fan_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataFlow() {
        return dataFlow;
    }

    public void setDataFlow(int dataFlow) {
        this.dataFlow = dataFlow;
    }

    public String getFan_name() {
        return fan_name;
    }

    public void setFan_name(String fan_name) {
        this.fan_name = fan_name;
    }

    public String getFan_image() {
        return fan_image;
    }

    public void setFan_image(String fan_image) {
        this.fan_image = fan_image;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(fan_id);
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeString(fan_name);
        dest.writeString(fan_image);
        dest.writeString(created);
    }
}
