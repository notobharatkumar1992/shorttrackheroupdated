package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 27-06-2016.
 */
public class Get_News_Model implements Parcelable
{
    public Get_News_Model() {
    }

    int status, dataflow;
    String Description;

    protected Get_News_Model(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        Description = in.readString();
    }

    public static final Creator<Get_News_Model> CREATOR = new Creator<Get_News_Model>() {
        @Override
        public Get_News_Model createFromParcel(Parcel in) {
            return new Get_News_Model(in);
        }

        @Override
        public Get_News_Model[] newArray(int size) {
            return new Get_News_Model[size];
        }
    };

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeString(Description);
    }
}
