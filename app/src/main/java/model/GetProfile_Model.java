package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 23-06-2016.
 */
public class GetProfile_Model implements Parcelable {
    int status, dataflow, id, role_id, admin_id, country_id, state_id, is_verified, login_type, language, is_login;
    String city_id, country, state, city_name, thumb, username, email, post_code, first_name, c_code, mobile, user_image, verification_code, refer_code, refer_by, device_id, device_token, created, modified, fan_detail;
    String car_number, dob, fb_link, tw_link, insta_link,   total_win, view_achievement, about_me, sponcer_list, current_rank;
    int driver_id, total_race, total_credit, total_point,track_id,payment_method_id;
    double height, weight;

    @Override
    public String toString() {
        return "GetProfile_Model{" +
                "status=" + status +
                ", dataflow=" + dataflow +
                ", id=" + id +
                ", role_id=" + role_id +
                ", admin_id=" + admin_id +
                ", country_id=" + country_id +
                ", state_id=" + state_id +
                ", is_verified=" + is_verified +
                ", login_type=" + login_type +
                ", language=" + language +
                ", is_login=" + is_login +
                ", city_id='" + city_id + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city_name='" + city_name + '\'' +
                ", thumb='" + thumb + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", post_code='" + post_code + '\'' +
                ", first_name='" + first_name + '\'' +
                ", c_code='" + c_code + '\'' +
                ", mobile='" + mobile + '\'' +
                ", user_image='" + user_image + '\'' +
                ", verification_code='" + verification_code + '\'' +
                ", refer_code='" + refer_code + '\'' +
                ", refer_by='" + refer_by + '\'' +
                ", device_id='" + device_id + '\'' +
                ", device_token='" + device_token + '\'' +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                ", fan_detail='" + fan_detail + '\'' +
                ", car_number='" + car_number + '\'' +
                ", dob='" + dob + '\'' +
                ", fb_link='" + fb_link + '\'' +
                ", tw_link='" + tw_link + '\'' +
                ", insta_link='" + insta_link + '\'' +
                ", track_id='" + track_id + '\'' +
                ", payment_method_id='" + payment_method_id + '\'' +
                ", total_win='" + total_win + '\'' +
                ", view_achievement='" + view_achievement + '\'' +
                ", about_me='" + about_me + '\'' +
                ", sponcer_list='" + sponcer_list + '\'' +
                ", current_rank='" + current_rank + '\'' +
                ", driver_id=" + driver_id +
                ", total_race=" + total_race +
                ", total_credit=" + total_credit +
                ", total_point=" + total_point +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }

    protected GetProfile_Model(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        id = in.readInt();
        role_id = in.readInt();
        admin_id = in.readInt();
        country_id = in.readInt();
        state_id = in.readInt();
        is_verified = in.readInt();
        login_type = in.readInt();
        language = in.readInt();
        is_login = in.readInt();
        city_id = in.readString();
        country = in.readString();
        state = in.readString();
        city_name = in.readString();
        thumb = in.readString();
        username = in.readString();
        email = in.readString();
        post_code = in.readString();
        first_name = in.readString();
        c_code = in.readString();
        mobile = in.readString();
        user_image = in.readString();
        verification_code = in.readString();
        refer_code = in.readString();
        refer_by = in.readString();
        device_id = in.readString();
        device_token = in.readString();
        created = in.readString();
        modified = in.readString();
        fan_detail = in.readString();
        car_number = in.readString();
        dob = in.readString();
        fb_link = in.readString();
        tw_link = in.readString();
        insta_link = in.readString();
        track_id = in.readInt();
        payment_method_id = in.readInt();
        total_win = in.readString();
        view_achievement = in.readString();
        about_me = in.readString();
        sponcer_list = in.readString();
        current_rank = in.readString();
        driver_id = in.readInt();
        total_race = in.readInt();
        total_credit = in.readInt();
        total_point = in.readInt();
        height = in.readDouble();
        weight = in.readDouble();
    }

    public static final Creator<GetProfile_Model> CREATOR = new Creator<GetProfile_Model>() {
        @Override
        public GetProfile_Model createFromParcel(Parcel in) {
            return new GetProfile_Model(in);
        }

        @Override
        public GetProfile_Model[] newArray(int size) {
            return new GetProfile_Model[size];
        }
    };

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFb_link() {
        return fb_link;
    }

    public void setFb_link(String fb_link) {
        this.fb_link = fb_link;
    }

    public String getTw_link() {
        return tw_link;
    }

    public void setTw_link(String tw_link) {
        this.tw_link = tw_link;
    }

    public String getInsta_link() {
        return insta_link;
    }

    public void setInsta_link(String insta_link) {
        this.insta_link = insta_link;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    public int getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(int payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getTotal_win() {
        return total_win;
    }

    public void setTotal_win(String total_win) {
        this.total_win = total_win;
    }

    public String getView_achievement() {
        return view_achievement;
    }

    public void setView_achievement(String view_achievement) {
        this.view_achievement = view_achievement;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getSponcer_list() {
        return sponcer_list;
    }

    public void setSponcer_list(String sponcer_list) {
        this.sponcer_list = sponcer_list;
    }

    public String getCurrent_rank() {
        return current_rank;
    }

    public void setCurrent_rank(String current_rank) {
        this.current_rank = current_rank;
    }

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public int getTotal_race() {
        return total_race;
    }

    public void setTotal_race(int total_race) {
        this.total_race = total_race;
    }

    public int getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(int total_credit) {
        this.total_credit = total_credit;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public GetProfile_Model() {
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDataflow() {
        return dataflow;
    }

    public void setDataflow(int dataflow) {
        this.dataflow = dataflow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(int is_verified) {
        this.is_verified = is_verified;
    }

    public int getLogin_type() {
        return login_type;
    }

    public void setLogin_type(int login_type) {
        this.login_type = login_type;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getC_code() {
        return c_code;
    }

    public void setC_code(String c_code) {
        this.c_code = c_code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(String verification_code) {
        this.verification_code = verification_code;
    }

    public String getRefer_code() {
        return refer_code;
    }

    public void setRefer_code(String refer_code) {
        this.refer_code = refer_code;
    }

    public String getRefer_by() {
        return refer_by;
    }

    public void setRefer_by(String refer_by) {
        this.refer_by = refer_by;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getFan_detail() {
        return fan_detail;
    }

    public void setFan_detail(String fan_detail) {
        this.fan_detail = fan_detail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(id);
        dest.writeInt(role_id);
        dest.writeInt(admin_id);
        dest.writeInt(country_id);
        dest.writeInt(state_id);
        dest.writeInt(is_verified);
        dest.writeInt(login_type);
        dest.writeInt(language);
        dest.writeInt(is_login);
        dest.writeString(city_id);
        dest.writeString(country);
        dest.writeString(state);
        dest.writeString(city_name);
        dest.writeString(thumb);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(post_code);
        dest.writeString(first_name);
        dest.writeString(c_code);
        dest.writeString(mobile);
        dest.writeString(user_image);
        dest.writeString(verification_code);
        dest.writeString(refer_code);
        dest.writeString(refer_by);
        dest.writeString(device_id);
        dest.writeString(device_token);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(fan_detail);
        dest.writeString(car_number);
        dest.writeString(dob);
        dest.writeString(fb_link);
        dest.writeString(tw_link);
        dest.writeString(insta_link);
        dest.writeInt(track_id);
        dest.writeInt(payment_method_id);
        dest.writeString(total_win);
        dest.writeString(view_achievement);
        dest.writeString(about_me);
        dest.writeString(sponcer_list);
        dest.writeString(current_rank);
        dest.writeInt(driver_id);
        dest.writeInt(total_race);
        dest.writeInt(total_credit);
        dest.writeInt(total_point);
        dest.writeDouble(height);
        dest.writeDouble(weight);
    }
}
