package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 20-06-2016.
 */
public class LoginModel implements Parcelable{
    int status,dataFlow,id,role_id,country_id,state_id,post_code,is_verified,login_type,language,is_login;
    String username,first_name,last_name,email,city,mobile,c_code,user_image,password,verification_code,created,modified,message,device_type,device_id;

    public LoginModel() {
    }

    protected LoginModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        id = in.readInt();
        role_id = in.readInt();
        country_id = in.readInt();
        state_id = in.readInt();
        post_code = in.readInt();
        is_verified = in.readInt();
        login_type = in.readInt();
        language = in.readInt();
        is_login = in.readInt();
        username = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        city = in.readString();
        mobile = in.readString();
        c_code = in.readString();
        user_image = in.readString();
        password = in.readString();
        verification_code = in.readString();
        created = in.readString();
        modified = in.readString();
        message = in.readString();
        device_type = in.readString();
        device_id = in.readString();
    }

    public static final Creator<LoginModel> CREATOR = new Creator<LoginModel>() {
        @Override
        public LoginModel createFromParcel(Parcel in) {
            return new LoginModel(in);
        }

        @Override
        public LoginModel[] newArray(int size) {
            return new LoginModel[size];
        }
    };

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public int getDataFlow() {
        return dataFlow;
    }

    public void setDataFlow(int dataFlow) {
        this.dataFlow = dataFlow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getPost_code() {
        return post_code;
    }

    public void setPost_code(int post_code) {
        this.post_code = post_code;
    }

    public int getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(int is_verified) {
        this.is_verified = is_verified;
    }

    public int getLogin_type() {
        return login_type;
    }

    public void setLogin_type(int login_type) {
        this.login_type = login_type;
    }

    public int getLanguage() {
        return language;
    }

    public void setLanguage(int language) {
        this.language = language;
    }

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getC_code() {
        return c_code;
    }

    public void setC_code(String c_code) {
        this.c_code = c_code;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public void setVerification_code(String verification_code) {
        this.verification_code = verification_code;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(id);
        dest.writeInt(role_id);
        dest.writeInt(country_id);
        dest.writeInt(state_id);
        dest.writeInt(post_code);
        dest.writeInt(is_verified);
        dest.writeInt(login_type);
        dest.writeInt(language);
        dest.writeInt(is_login);
        dest.writeString(username);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(city);
        dest.writeString(mobile);
        dest.writeString(c_code);
        dest.writeString(user_image);
        dest.writeString(password);
        dest.writeString(verification_code);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(message);
        dest.writeString(device_type);
        dest.writeString(device_id);
    }
}
