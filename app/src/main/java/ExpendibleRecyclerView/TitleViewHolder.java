package ExpendibleRecyclerView;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.noto.shorttrackhero.R;

public class TitleViewHolder extends ParentViewHolder {
    TextView title, add, sub, display, delete;
    View itemView;
    private Data data;
    public TitleViewHolder(View itemView) {
        super(itemView);
       this.itemView= itemView;
        title = (TextView) itemView.findViewById(R.id.title);
        add = (TextView) itemView.findViewById(R.id.add);
    }

}
