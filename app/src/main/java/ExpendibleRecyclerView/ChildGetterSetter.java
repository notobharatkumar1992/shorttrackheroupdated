package ExpendibleRecyclerView;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ChildGetterSetter implements Parcelable{
     int race_id,comp_id;
    String subtitle;

    protected ChildGetterSetter(Parcel in) {
        race_id = in.readInt();
        comp_id = in.readInt();
        subtitle = in.readString();
    }

    public static final Creator<ChildGetterSetter> CREATOR = new Creator<ChildGetterSetter>() {
        @Override
        public ChildGetterSetter createFromParcel(Parcel in) {
            return new ChildGetterSetter(in);
        }

        @Override
        public ChildGetterSetter[] newArray(int size) {
            return new ChildGetterSetter[size];
        }
    };

    @Override
    public String toString() {
        return "ChildGetterSetter{" +
                "race_id=" + race_id +
                ", comp_id=" + comp_id +
                ", subtitle='" + subtitle + '\'' +
                '}';
    }

    public ChildGetterSetter() {

   }
    public ChildGetterSetter(String subtitle,int race_id,int comp_id) {
        this.subtitle = subtitle;
        this.race_id=race_id;
        this.comp_id=comp_id;

    }

    public int getRace_id() {
        return race_id;
    }

    public int getComp_id() {
        return comp_id;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void removeSubtitle() {
        Log.e("delete group", this.subtitle = null + "");
        this.subtitle = null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(race_id);
        dest.writeInt(comp_id);
        dest.writeString(subtitle);
    }
}
