package ExpendibleRecyclerView;

import android.os.Parcel;
import android.os.Parcelable;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
/*import com.bignerdranch.expandablerecyclerview.Model.ParentObject;*/

import java.util.List;


public class Data implements ParentListItem ,Parcelable {
    private List<Object> childList;
    String Title;
    public int isExpend = 0;

    public Data(Parcel in) {
        Title = in.readString();
        isExpend = in.readInt();
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }
        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public Data() {

    }

    @Override
    public String toString() {
        return "Data{" +
                "childList=" + childList +
                ", Title='" + Title + '\'' +
                '}';
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setChildObjectList(List<Object> list) {
        childList = list;
    }

    @Override
    public List<Object> getChildItemList() {
        return childList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Title);
        dest.writeInt(isExpend);
    }
}
