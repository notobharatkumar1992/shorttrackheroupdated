package com.noto.shorttrackhero;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.LoginModel;
import model.PostAysnc_Model;

public class Login extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {
    EditText email, password;
    TextView login, forgot_password, signup;
    public static Activity loginClass;
    Toolbar toolbar;
    String Email, Password;
    Intent intent;
    private String identifier;
    private LoginModel items;
    ImageView toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginClass = this;
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        setup_toolbar();
        initializeObjects();
    }

    private void setup_toolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //  toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loginClass != null)
                    loginClass.finish();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initializeObjects() {
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (TextView) findViewById(R.id.login);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        signup = (TextView) findViewById(R.id.signup);
        toggle = (ImageView) findViewById(R.id.toggle);
        toggle.setOnClickListener(this);
        login.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
        signup.setOnClickListener(this);
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            String emailid = AppDelegate.getValue(this, Tags.driverEmail);
            if (AppDelegate.isValidString(emailid)) {
                AppDelegate.Log("emailid", emailid + "" + "hellooo");
                toggle.setSelected(true);
                email.setText("" + AppDelegate.getValue(this, Tags.driverEmail));
                password.setText("" + AppDelegate.getValue(this, Tags.driverPassword));
            } else {
                AppDelegate.Log("emailid", emailid + "" + "byee");
                toggle.setSelected(false);

                email.setText("");
                password.setText("");
            }
        } else {
            String emailid = AppDelegate.getValue(this, Tags.FanEmail);
            if (AppDelegate.isValidString(emailid)) {
                toggle.setSelected(true);
                email.setText("" + AppDelegate.getValue(this, Tags.FanEmail));
                password.setText("" + AppDelegate.getValue(this, Tags.fanPassword));
            } else {
                toggle.setSelected(false);
                email.setText("");
                password.setText("");
            }
        }
    }

    private boolean validate_emailid(String email, String password) {
        boolean result = true;
        try {
            if (email.isEmpty()) {
                result = false;
                AppDelegate.ShowDialog(Login.this, "Please enter Email Id !!!", "Alert!!!");
            } else if (password == null) {
                result = false;
                AppDelegate.ShowDialog(Login.this, "Please enter Password", "Alert!!!");
            } else if (AppDelegate.CheckEmail(email) == false) {
                result = false;
                AppDelegate.ShowDialog(Login.this, "Invalid Email Address", "Alert!!!");
            } else {
                result = AppDelegate.password_validation(this, password);
            }

        } catch (Exception e) {
            AppDelegate.LogE(e);
            intent = new Intent(Login.this, Login.class);
            startActivity(intent);
        }
        return result;
    }

    private void getTextfromFields() {
        Email = email.getText().toString();
        Password = password.getText().toString();
    }

    private void setFields() {
        if (toggle.isSelected()) {

        } else {
            email.setText("");
            password.setText("");
        }
    }

    @Override
    public void onBackPressed() {
        if (EntryPoint.mActivity != null) {
            EntryPoint.mActivity.finish();
        }
        if (Splash.mActivity != null) {
            Splash.mActivity.finish();
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggle:
                if (toggle.isSelected()) {
                    email.setText("");
                    password.setText("");
                    toggle.setSelected(false);

                } else {
                    toggle.setSelected(true);
                }
                break;
            case R.id.signup:
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
                break;
            case R.id.login:
                getTextfromFields();
                Boolean result = validate_emailid(Email, Password);
                if (result == true) {
                    if (toggle.isSelected()) {
                        AppDelegate.Log("toggle _if", toggle.isSelected() + "");
                        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                            AppDelegate.save(this, Email, Tags.driverEmail);
                            AppDelegate.save(this, Password, Tags.driverPassword);
                        } else {
                            AppDelegate.save(this, Email, Tags.FanEmail);
                            AppDelegate.save(this, Password, Tags.fanPassword);
                        }
                    } else {
                        AppDelegate.Log("toggle_else", toggle.isSelected() + "");
                        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                            AppDelegate.removeValue(this, Tags.driverEmail);
                            AppDelegate.removeValue(this, Tags.driverPassword);
                        } else {
                            AppDelegate.removeValue(this, Tags.FanEmail);
                            AppDelegate.removeValue(this, Tags.fanPassword);
                        }
                    }
                    LoginMethod();
                }
                break;
            case R.id.forgot_password:
                i = new Intent(Login.this, ForgotPassword.class);
                startActivity(i);
                break;
        }
    }
    private void LoginMethod() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.email, Email);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.password, Password);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_id, AppDelegate.getUUID(this), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_type, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(Login.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_FAN)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.login_type, Parameters.login_type_forfan, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.LOGIN,
                            mPostArrayList, null);
                } else {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.login_type, Parameters.login_type_fordriver, ServerRequestConstants.Key_PostintValue);
                    mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.DRIVER_LOGIN,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
            }catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }
    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.LOGIN)) {
            AppDelegate.save(this, result, Parameters.saveLogin);
            parseLogin();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_LOGIN)) {
            AppDelegate.save(this, result, Parameters.saveLogin);
            parseLogin();
        }
    }
    @Override
    protected void onDestroy() {
        if (loginClass != null) {
            loginClass = null;
        }
        super.onDestroy();
    }
    private void parseLogin() {
        String response = AppDelegate.getValue(this, Parameters.saveLogin);
        items = new LoginModel();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            items.setStatus(jsonObject.getInt("status"));
            items.setMessage(jsonObject.getString("message"));
            AppDelegate.LogT("parseLogin = " + items.getStatus() + "");
            if (items.getStatus() == 1) {
                JSONObject jsonobj = jsonObject.getJSONObject("response");
                AppDelegate.save(this, Tags.LOGIN, Tags.LOGIN_STATUS);
              //  AppDelegate.ShowDialogToast(this, items.getMessage(), "Alert!!!");
                //AppDelegate.toast(this, items.getMessage() + "", "Time out!!!");
                AppDelegate.save(this,jsonobj.getString("username"),"username");
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    AppDelegate.save(this,jsonobj.getString("image"),"user_image");
                } else {
                    AppDelegate.save(this,jsonobj.getString("user_image"),"user_image");
                }
                Intent i = new Intent(Login.this, PlayVideo.class);
                startActivity(i);
                setFields();
            } else {
                AppDelegate.ShowDialog(this, items.getMessage() + "", "Time out!!!");
                setFields();
                AppDelegate.save(this, Tags.LOGOUT, Tags.LOGIN_STATUS);
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }
}