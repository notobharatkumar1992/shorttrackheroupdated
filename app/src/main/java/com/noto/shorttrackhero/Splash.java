package com.noto.shorttrackhero;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import Constants.Tags;

public class Splash extends FragmentActivity {
   static Splash mActivity;
    String key=null;
    private Intent mainIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        mActivity=this;
       /* RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.splash);
        relativeLayout.setBackgroundResource(R.drawable.splash);*/
        key=AppDelegate.getValue(this, Tags.LOGIN_STATUS);
        if (key.equalsIgnoreCase(Tags.LOGIN)) {
            mainIntent = new Intent(Splash.this, HomeScreen.class);
            startActivity(mainIntent);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mainIntent = new Intent(Splash.this, EntryPoint.class);
                    startActivity(mainIntent);
                }
            }, 2000);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mActivity!=null){
            mActivity=null;
        }
    }
}
