package com.noto.shorttrackhero;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import Async.LoadImage;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import adapters.NavigationAdapter;
import fragments.AddSponcer;
import fragments.ChangePassword;
import fragments.CheckOut;
import fragments.DashBoardDriver;
import fragments.DashBoardFan;
import fragments.DriverDetail;
import fragments.DriverList;
import fragments.FanList;
import fragments.GameHistory;
import fragments.GameHistoryDetail;
import fragments.MyListing;
import fragments.MyProfile;
import fragments.Notification;
import fragments.PointHistoryDetail;
import fragments.PointsHistory;
import fragments.RaceDetail;
import fragments.RaceHistoryDetail;
import fragments.Search_Sponcers;
import fragments.Search_SponcersComp;
import fragments.Shop;
import fragments.ShopDetail;
import fragments.ShopHistory;
import fragments.ShopHistoryDetail;
import fragments.SponsorDetails;
import fragments.ThankYou;
import interfaces.OnReciveServerResponse;
import model.GetProfile_Model;
import model.PostAysnc_Model;
import utils.Constant;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnReciveServerResponse, AdapterView.OnClickListener, AdapterView.OnItemClickListener {

    private static NavigationView navigationView;
    private String login_Response;
    Fragment f;
    public FrameLayout frame;
    public static Toolbar toolbar;
    public FragmentManager fragmentManager;
    public String usernam;
    public int id;
    public CircleImageView userimg;
    public String userimge;
    public TextView username;
    public String identifier;
    Activity mActivity;
    private int user_id;
    public String refercode;
    public ImageView search;
    public static View headerLayout;
    public GetProfile_Model getProfile_model;
    public ListView navigate;
    private String[] itemlist;
    private Integer[] imagelist;
    private ActionBarDrawerToggle mDrawerToggle;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);
        identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
        if(AppDelegate.isValidString(AppDelegate.getValue(this, "fanpoints"))){
            AppDelegate.removeValue(this,"fanpoints");
        }
        mActivity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        double width = getResources().getDisplayMetrics().widthPixels / 1.4;
        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        params.width = (int) width;
        navigationView.setLayoutParams(params);
        navigationView.setNavigationItemSelectedListener(this);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.menuicn, getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        initializeObjects();
    }

    private void initializeObjects() {
        headerLayout = navigationView.getHeaderView(0);
        // userimg = (ImageView) headerLayout.findViewById(R.id.user_img);
        userimg = (CircleImageView) findViewById(R.id.user_img);
        username = (TextView) findViewById(R.id.username);
        //  username = (TextView) headerLayout.findViewById(R.id.username);
        frame = (FrameLayout) findViewById(R.id.framelayout);
        frame.setBackgroundColor(Color.WHITE);
        search = (ImageView) toolbar.findViewById(R.id.search);
        fragmentManager = getSupportFragmentManager();
        login_Response = AppDelegate.getValue(this, Parameters.saveLogin);
        try {
            userimg.setOnClickListener(this);
        } catch (Exception e) {

        }
        navigate = (ListView) findViewById(R.id.Listview);
        navigate.setOnItemClickListener(this);
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {

            {
                itemlist = new String[]{"Home", "Shop", "My Sponsors","Game History", "Shop History", "Help", "Notifications", "Refer a Friend", "Like", "Logout"};
                imagelist = new Integer[]{R.drawable.homeicon, R.drawable.bagicon, R.drawable.sponsor, R.drawable.pointhistory, R.drawable.shophistory, R.drawable.helpicon, R.drawable.notification, R.drawable.emailicon, R.drawable.like, R.drawable.logout};
                NavigationAdapter spinnerAdapter = new NavigationAdapter(this, itemlist, imagelist);
                navigate.setAdapter(spinnerAdapter);
                navigate.getChildAt(0);
            }
        } else {
            itemlist = new String[]{"Home","My Competitions", "Shop", "Point History", "Shop History", "Help", "Notifications", "Refer a Friend", "Like", "Logout"};
            imagelist = new Integer[]{R.drawable.homeicon, R.drawable.pointhistory, R.drawable.bagicon, R.drawable.pointhistory, R.drawable.shophistory, R.drawable.helpicon, R.drawable.notification, R.drawable.emailicon, R.drawable.like, R.drawable.logout};
            NavigationAdapter spinnerAdapter = new NavigationAdapter(this, itemlist, imagelist);
            navigate.setAdapter(spinnerAdapter);
            navigate.getChildAt(0);
        }
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            AppDelegate.showFragmentAnimation(fragmentManager, new DashBoardDriver(), R.id.framelayout);
        } else {
            AppDelegate.showFragmentAnimation(fragmentManager, new DashBoardFan(), R.id.framelayout);
        }
      //  getprofile();
        setvalues();

    }

    private void getprofile() {
        setvalues();
        try {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, id, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    mPostAsyncObj = new PostAsync(this, HomeScreen.this, ServerRequestConstants.DRIVER_GET_PROFILE,
                            mPostArrayList, null);
                } else {
                    mPostAsyncObj = new PostAsync(this, HomeScreen.this, ServerRequestConstants.GET_PROFILE,
                            mPostArrayList, null);
                }
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    public void setvalues() {
        String login = AppDelegate.getValue(this, Parameters.saveLogin);
        Log.e("login value", login + "");
        try {
            JSONObject json = new JSONObject(login);
            int status = json.getInt("status");
            if (status == 1) {
                JSONObject jsonObject = json.getJSONObject("response");
                id = jsonObject.getInt("id");
                usernam = jsonObject.getString("username");
                identifier = AppDelegate.getValue(this, Tags.IDENTIFIER);
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                    userimge = jsonObject.getString("image");
                } else {
                    userimge = jsonObject.getString("user_image");
                }

                username.setText(AppDelegate.getValue(this,"username") + "");
                user_id = jsonObject.getInt("id");
                new LoadImage().execute(AppDelegate.getValue(this,"user_image")+"");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof ThankYou) {
            f = new Shop();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof MyProfile) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof ChangePassword) {
            super.onBackPressed();
        }
        else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof RaceHistoryDetail) {
            super.onBackPressed();
        }
        else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof MyListing) {
            super.onBackPressed();
        }else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof Search_Sponcers) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof Search_SponcersComp) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof CheckOut) {
            f = new Shop();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof DriverDetail) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof SponsorDetails) {
            super.onBackPressed();
        }else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof DriverList) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof FanList) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof Notification) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof PointsHistory) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof PointHistoryDetail) {
            super.onBackPressed();
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof RaceDetail) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof Shop) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof GameHistory) {
            f = new DashBoardDriver();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof GameHistoryDetail) {
            f = new GameHistory();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof ShopDetail) {
            f = new Shop();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof ShopHistory) {
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                f = new DashBoardDriver();
            } else {
                f = new DashBoardFan();
            }
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof ShopHistoryDetail) {
            f = new ShopHistory();
            AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof DashBoardFan) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                AlertDialog diaBox = AskOption();
                diaBox.show();
            }
        } else if (getSupportFragmentManager().findFragmentById(R.id.framelayout) instanceof DashBoardDriver) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                AlertDialog diaBox = AskOption();
                diaBox.show();
            }
        }
    }

    private AlertDialog AskOption() {
        // TODO Auto-generated method stub
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                        moveTaskToBack(true);
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if (image != null) {
                userimg.setImageBitmap(image);
            } else {

            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String value = itemlist[position];
                if (value.trim().equalsIgnoreCase("Home")) {
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        f = new DashBoardDriver();
                        search.setVisibility(View.GONE);
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    } else {
                        search.setVisibility(View.GONE);
                        f = new DashBoardFan();
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    }
                }else if (value.trim().equalsIgnoreCase("My Competitions")) {
                    f = new MyListing();
                    search.setVisibility(View.GONE);
                    AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                } else if (value.trim().equalsIgnoreCase("Shop")) {
                    f = new Shop();
                    search.setVisibility(View.GONE);
                    AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                } else if (value.trim().equalsIgnoreCase("My Sponsors")) {
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        search.setVisibility(View.GONE);
                        f = new AddSponcer();
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    }
                } else if (value.trim().equalsIgnoreCase("Game History")) {
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                        f = new GameHistory();
                        search.setVisibility(View.GONE);
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    }
                } else if (value.trim().equalsIgnoreCase("Point History")) {
                    if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {

                    } else {
                        f = new PointsHistory();
                        search.setVisibility(View.GONE);
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                    }
                } else if (value.trim().equalsIgnoreCase("Shop History")) {
                    f = new ShopHistory();
                    search.setVisibility(View.GONE);
                    AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                } else if (value.trim().equalsIgnoreCase("Help")) {
                    search.setVisibility(View.GONE);
                    String url = ServerRequestConstants.HELP;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else if (value.trim().equalsIgnoreCase("Notifications")) {
                    search.setVisibility(View.GONE);
                    f = new Notification();
                    AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                } else if (value.trim().equalsIgnoreCase("Refer a Friend")) {
                    search.setVisibility(View.GONE);
                    call_reafer();
                } else if (value.trim().equalsIgnoreCase("Like")) {
                    search.setVisibility(View.GONE);
                    String url = ServerRequestConstants.LIKE;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else if (value.trim().equalsIgnoreCase("Logout")) {
                    search.setVisibility(View.GONE);
                    AppDelegate.removeValue(HomeScreen.this, Tags.LOGIN_STATUS);
                    if (Select_Track.mActivity != null) {
                        Select_Track.mActivity.finish();
                    }
                    if (PlayVideo.activity != null) {
                        PlayVideo.activity.finish();
                    }
                    if (Login.loginClass != null) {
                        Login.loginClass.finish();
                    }
                    if (EntryPoint.mActivity != null) {
                        EntryPoint.mActivity.finish();
                    }
                    if (Splash.mActivity != null) {
                        Splash.mActivity.finish();
                    }
                    Intent intent = new Intent(HomeScreen.this, EntryPoint.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 400);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    private void call_reafer() {
        TextView txttitle = (TextView) toolbar.findViewById(R.id.title);
        txttitle.setText("Short Track Hero");
        try {
            if (AppDelegate.haveNetworkConnection(HomeScreen.this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(HomeScreen.this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(HomeScreen.this).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                // AppDelegate.getInstance(HomeScreen.this).setPostParamsSecond(mPostArrayList, Parameters.comp_id, raceModel.getComp_id(), ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj = new PostAsync(HomeScreen.this, this, ServerRequestConstants.REFER_A_FRIEND,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(HomeScreen.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(HomeScreen.this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.user_img:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        f = new MyProfile();
                        AppDelegate.showFragmentAnimation(fragmentManager, f, R.id.framelayout);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);

                    }
                }, 400);
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(HomeScreen.this);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(this, "Service Time Out!!!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.REFER_A_FRIEND)) {
            AppDelegate.save(HomeScreen.this, result, Parameters.refer_friend);
            parseFan_referFriend();
        } else if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            AppDelegate.save(this, result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        } else if (apiName.equals(ServerRequestConstants.DRIVER_GET_PROFILE)) {
            AppDelegate.save(this, result, Parameters.get_profile);
            Log.e("get_profile", result + "");
            parseProfile();
        }
    }

    public void parseProfile() {
        String Register = AppDelegate.getValue(this, Parameters.get_profile);
        AppDelegate.Log("response", Register + "");
        if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                int status = jsonObject.getInt("status");

                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    try {
                        JSONObject fanresponse = response.getJSONObject("driver_detail");
                        getProfile_model.setUser_image(fanresponse.getString("user_image"));
                      //  new LoadImage().execute(AppDelegate.getValue(this,"user_image")+"");
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    // capturedFile=getProfile_model.getUser_image();
                }

            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            try {
                JSONObject jsonObject = new JSONObject(Register);
                int status = jsonObject.getInt("status");
                getProfile_model = new GetProfile_Model();
                if (status == 1) {
                    JSONObject response = jsonObject.getJSONObject("response");
                    getProfile_model.setUser_image(response.getString("user_image"));
                  //  new LoadImage().execute(AppDelegate.getValue(this,"user_image")+"");
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }


    private void parseFan_referFriend() {
        String response = AppDelegate.getValue(this, Parameters.refer_friend);
        try {
            JSONObject object = new JSONObject(response);
            int status = object.getInt("status");
            if (status == 1) {
                refercode = object.getString("response");
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "I am giving you a Five free points on the Short Track Hero app. To accept use code " + "'" + refercode + "'" + " to Sign up.");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                startActivity(Intent.createChooser(sharingIntent, "Choose an option"));
            } else {
                AppDelegate.ShowDialog(this, "Bad response.", "Alert!!!");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
