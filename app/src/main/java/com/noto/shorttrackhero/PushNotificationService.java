package com.noto.shorttrackhero;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by admin on 23-05-2016.
 */
public class PushNotificationService extends GcmListenerService {
    private int status = 0;
    private String message;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        message = data.getString("message");
        Log.d("Push message", message + ", data = " + data);
//        data.get
        //createNotification(mTitle, push_msg);
        showNotification(data);
    }

    private void showNotification(Bundle bundle) {
       /* JSONObject jsonObject = null;
        String name = "", date = "";
        try {
            jsonObject = new JSONObject(bundle.getString("message"));
            status = jsonObject.getInt("status");
            if(status==1) {
                message = jsonObject.getString("response");
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }*/
        Intent intent = new Intent(this, EntryPoint.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
       /* Intent intent = new Intent(this, CoupanDetail.class);
        Bundle bundleData = new Bundle();
        CoupanItems coupan = new CoupanItems();
        coupan.setCode(name);
        coupan.setExpiry_date(date);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        bundleData.putParcelable("coupan_detail", coupan);
        intent.putExtras(bundleData);*/

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.icon60)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new Notification.BigTextStyle().bigText(message))
                .setContentText("");
//        Intent intent = new Intent(this, );
       /* PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);*/
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(fullScreenPendingIntent);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
            notification = notificationBuilder.build();
        } else if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification(R.drawable.icon60,
                    getString(R.string.app_name), System.currentTimeMillis());
        } else {
            notification = notificationBuilder.build();
        }

        notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);
    }
}