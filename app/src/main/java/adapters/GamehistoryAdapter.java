package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Constants.Tags;
import UI.CircleImageView;
import fragments.GameHistoryDetail;
import fragments.ShopHistoryDetail;
import model.GameHistoryModel;
import model.ProductListModel;


public class GamehistoryAdapter extends RecyclerView.Adapter<GamehistoryAdapter.ViewHolder> {

    View v;
    private List<GameHistoryModel> pointsHistory;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.game_history_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GameHistoryModel dv = pointsHistory.get(position);
        holder.comp_name.setText(dv.getCompetitions_name() + "");
        holder.track_name.setText(String.valueOf(dv.getTrack_name()) + "");
        holder.race_name.setText(String.valueOf(dv.getRace_name()) + "");
        holder.prize.setText(String.valueOf(dv.getCurrent_rank()) + "");
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.parcel_gameHistoryItem, dv);
                AppDelegate.showFragment(context, new GameHistoryDetail(), R.id.framelayout, bundle, null);
            }
        });
        AppDelegate.getInstance(context).getImageLoader().get(dv.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.image.setImageBitmap(response.getBitmap());
                }
            }
        });
    }
    public GamehistoryAdapter(FragmentActivity context, List<GameHistoryModel> pointsHistory) {
        this.context = context;
        this.pointsHistory = pointsHistory;
    }

    @Override
    public int getItemCount() {
        return pointsHistory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        CardView cardView;
        private  TextView track_name;
        private  TextView prize;
        private  TextView race_name;
        private  TextView comp_name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.image);
            comp_name = (TextView) itemView.findViewById(R.id.competion_name);
            race_name = (TextView) itemView.findViewById(R.id.class_name);
            track_name = (TextView) itemView.findViewById(R.id.track_name);
            prize = (TextView) itemView.findViewById(R.id.prize);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}