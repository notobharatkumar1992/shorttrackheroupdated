package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import fragments.DriverDetail;
import fragments.FanList;
import fragments.RaceDetail;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.LoginModel;
import model.PostAysnc_Model;


public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.ViewHolder> implements OnDialogClickListener, OnReciveServerResponse {
    View v;
    private List<Driver> driver;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;
    private LoginModel getProfile_model;
    private int user_id = 0;
    private int driver_id;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.driver_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        String identifier = AppDelegate.getValue(context, Tags.IDENTIFIER);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        bundle = new Bundle();
        final Driver dv = driver.get(position);
        holder.driver_name.setText(dv.getUsername() + "");
        holder.prizes.setText(dv.getTotal_win() + "");
        holder.points.setText(dv.getTotal_point() + "");
        holder.rank.setText(dv.getCurrent_rank() + "");
        AppDelegate.getInstance(context).getImageLoader().get(dv.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
        holder.pick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*bundle.putParcelable(Tags.parcel_FanDetail, dv);
                AppDelegate.showFragment(context, new FanList(), R.id.framelayout, bundle, null);*/
                driver_id = dv.getDriver_id();
                AppDelegate.showDialog_twoButtons(context, "Are you sure. ", Tags.buydriver, DriverListAdapter.this);
            }
        });
        holder.fans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putParcelable(Tags.parcel_FanDetail, dv);
                AppDelegate.showFragment(context, new FanList(), R.id.framelayout, bundle, null);
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putParcelable(Tags.parcel_FanDetail, dv);
                AppDelegate.showFragment(context, new DriverDetail(), R.id.framelayout, bundle, null);
            }
        });
    }

    public DriverListAdapter(FragmentActivity context, List<Driver> driver) {
        this.context = context;
        this.driver = driver;
    }

    @Override
    public int getItemCount() {
        return driver.size();
    }

    @Override
    public void setOnDialogClickListener(String name) {
        if (name.equals(Tags.buydriver)) {
            execute_buydriver();
        }
    }

    private void parseProfile() {
        String Register = AppDelegate.getValue(context, Parameters.saveLogin);
        AppDelegate.Log("response", Register + "");
        try {
            JSONObject jsonObject = new JSONObject(Register);
            int status = jsonObject.getInt("status");
            getProfile_model = new LoginModel();
            if (status == 1) {
                JSONObject response = jsonObject.getJSONObject("response");
                getProfile_model.setId(response.getInt("id"));
                user_id = getProfile_model.getId();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void execute_buydriver() {
        try {
            parseProfile();
            String identifier = AppDelegate.getValue(context, Tags.IDENTIFIER);
            if (AppDelegate.haveNetworkConnection(context, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.user_id, user_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.driver_id, driver_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.competition_id, Integer.parseInt(AppDelegate.getValue(context, Tags.competitionID)), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Parameters.race_id, Integer.parseInt(AppDelegate.getValue(context, Tags.raceID)), ServerRequestConstants.Key_PostintValue);
                if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {

                } else {
                    PostAsync mPostAsyncObj = new PostAsync(context, DriverListAdapter.this, ServerRequestConstants.PURCHASE_DRIVER,
                            mPostArrayList, null);
                    AppDelegate.showProgressDialog(context);
                    mPostAsyncObj.execute();
                }
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(context, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(context);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(context, "Time out!!!Please Try Again!", "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.PURCHASE_DRIVER)) {
            AppDelegate.save(context, result, Parameters.purchase_driver);
            AppDelegate.Log("purchase driver", result);
            parse_purchaseDriver();
        }
    }

    private void parse_purchaseDriver() {
        String purchase = AppDelegate.getValue(context, Parameters.purchase_driver);
        try {
            JSONObject jsonObject = new JSONObject(purchase);
            String message;
            int status = jsonObject.getInt("status");
            if (status == 1) {
                message = jsonObject.getString("response");
                RaceDetail.fan_points=(TextView)context.findViewById(R.id.fan_points);
                AppDelegate.save(context, String.valueOf(jsonObject.getInt("newPoint")), "fanpoints");
                // AppDelegate.getValue(getActivity(),"fanpoints");
              //  fan_points.setText( AppDelegate.getValue(context,"fanpoints")+"");
               // RaceDetail.fan_points.setText(jsonObject.getInt("newPoint"));
                RaceDetail.fan_points.setText(AppDelegate.getValue(context, "fanpoints") + "");
            } else {
              /*  RaceDetail.fan_points=(TextView)context.findViewById(R.id.fan_points);
                AppDelegate.save(context, String.valueOf(10), "fanpoints");
                // AppDelegate.getValue(getActivity(),"fanpoints");
                //  fan_points.setText( AppDelegate.getValue(context,"fanpoints")+"");
                // RaceDetail.fan_points.setText(jsonObject.getInt("newPoint"));
                RaceDetail.fan_points.setText(AppDelegate.getValue(context, "fanpoints") + "");*/
                message = jsonObject.getString("message");
            }
            AppDelegate.ShowDialogID(context, message + "", "Alert!!!", "ForgotPassword", this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView driver_img;
        TextView driver_name, fans, prizes, points, rank, pick;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            driver_img = (CircleImageView) itemView.findViewById(R.id.driver_img);
            driver_name = (TextView) itemView.findViewById(R.id.driver_name);
            fans = (TextView) itemView.findViewById(R.id.fans);
            prizes = (TextView) itemView.findViewById(R.id.prizes);
            points = (TextView) itemView.findViewById(R.id.points);
            rank = (TextView) itemView.findViewById(R.id.rank);
            cardView = (CardView) itemView.findViewById(R.id.card);
            pick = (TextView) itemView.findViewById(R.id.pick);
            String identifier = AppDelegate.getValue(context, Tags.IDENTIFIER);
            if (identifier.equalsIgnoreCase(Tags.IDENTIFY_DRIVER)) {
                pick.setVisibility(View.GONE);
            } else {
                pick.setVisibility(View.VISIBLE);
            }
        }
    }
}