package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.DriverGames;
import model.PointsHistoryModel;

public class DriverGamesAdapter extends RecyclerView.Adapter<DriverGamesAdapter.ViewHolder> {
    View v;
    private List<DriverGames> pointsHistory;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.driver_comp_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        //  AppDelegate.LogT("dvDVVV");
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        DriverGames dv = pointsHistory.get(position);
        AppDelegate.LogT("getstartdate" + dv + "");
        String[] strings = dv.getStart_date().split(" ");
        String sdate = null;
        try {
            sdate = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(strings[0]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date2.setText(sdate + "");
        holder.points2.setText(String.valueOf(dv.getTotal_point() )+ "");
        holder.position2.setText(String.valueOf(dv.getTotal_credit()) + "");
        holder.comp2.setText(dv.getCompetition_name() + "");
        holder.laps2.setText(dv.getLap() + "");
    }

    public DriverGamesAdapter(FragmentActivity context, ArrayList<DriverGames> pointsHistory) {
        this.context = context;
        this.pointsHistory = pointsHistory;
    }

    @Override
    public int getItemCount() {
        AppDelegate.LogT("pointsHistorysize" + pointsHistory.size() + "");
        return pointsHistory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView points2, date2, position2, comp2, laps2;
        public ViewHolder(View itemView) {
            super(itemView);
            points2 = (TextView) itemView.findViewById(R.id.point2);
            date2 = (TextView) itemView.findViewById(R.id.date2);
            position2 = (TextView) itemView.findViewById(R.id.pos2);
            comp2 = (TextView) itemView.findViewById(R.id.comp2);
            laps2 = (TextView) itemView.findViewById(R.id.lap2);
        }
    }
}