package adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import model.CountryListModel;
import model.Race;
import model.RankList;


public class DetailsAdapter extends ArrayAdapter<RankList> {
    // TODO Auto-generated constructor stub
    private final Activity context;
    ArrayList<RankList> title;
    Integer image;

    public DetailsAdapter(Activity context, ArrayList<RankList> title) {
        super(context, R.layout.detail_listitem,title);
        {
            this.context = context;
            this.title = title;
            AppDelegate.LogT("title in details adapter" +title+ "");
        }
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.detail_listitem, null, true);
        AppDelegate.LogT(title.get(position).getPoint()+"");
        TextView rank = (TextView) rowView.findViewById(R.id.rank);
        AppDelegate.LogT(title.get(position).getRank()+"");
        TextView points = (TextView) rowView.findViewById(R.id.points);
        rank.setText(title.get(position).getRank()+"");
        points.setText(title.get(position).getPoint()+"");
        //txtTitle.setText(title.get(position).getName());
        return rowView;
    }
}
