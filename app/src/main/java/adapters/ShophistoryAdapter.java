package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Constants.Tags;
import UI.CircleImageView;
import fragments.ShopHistoryDetail;
import model.PointsHistoryModel;
import model.ProductListModel;
public class ShophistoryAdapter extends RecyclerView.Adapter<ShophistoryAdapter.ViewHolder> {
    View v;
    private List<ProductListModel> pointsHistory;
    FragmentActivity context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    private Bundle bundle;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_history_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final ProductListModel dv = pointsHistory.get(position);
        holder.name.setText(dv.getProduct_name() + "");
        holder.quantity.setText(String.valueOf(dv.getQuantity()) + "");
        holder.price.setText(String.valueOf(dv.getPrice()) + "");
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.parcel_shopHistoryItem, dv);
                AppDelegate.showFragment(context, new ShopHistoryDetail(), R.id.framelayout, bundle, null);
            }
        });
        AppDelegate.getInstance(context).getImageLoader().get(dv.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.image.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    public ShophistoryAdapter(FragmentActivity context, List<ProductListModel> pointsHistory) {
        this.context = context;
        this.pointsHistory = pointsHistory;
    }

    @Override
    public int getItemCount() {
        return pointsHistory.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView image;
        CardView cardView;
        TextView name, quantity, price;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (CircleImageView) itemView.findViewById(R.id.img);
            name = (TextView) itemView.findViewById(R.id.name);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            price = (TextView) itemView.findViewById(R.id.price);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}