package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import UI.CircleImageView;
import fragments.DriverDetail;
import fragments.FanList;
import fragments.RaceDetail;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.Driver;
import model.LoginModel;
import model.PostAysnc_Model;


public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.ViewHolder>  {
    View v;
    private List<Driver> driver;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.race_driver_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Driver dv = driver.get(position);
        holder.driver_name.setText(dv.getUsername() + "");
        holder.prizes.setText(dv.getTotal_win() + "");
        holder.points.setText(dv.getTotal_point() + "");
        holder.rank.setText(dv.getCurrent_rank() + "");
        AppDelegate.getInstance(context).getImageLoader().get(dv.getDriver_img(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.driver_img.setImageBitmap(response.getBitmap());
                }
            }
        });
       /* holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putParcelable(Tags.parcel_FanDetail, dv);
                AppDelegate.showFragment(context, new DriverDetail(), R.id.framelayout, bundle, null);
            }
        });*/
    }

    public DriverAdapter(FragmentActivity context, List<Driver> driver) {
        this.context = context;
        this.driver = driver;
    }

    @Override
    public int getItemCount() {
        return driver.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView driver_img;
        TextView driver_name,  prizes, points, rank;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            driver_img = (CircleImageView) itemView.findViewById(R.id.driver_img);
            driver_name = (TextView) itemView.findViewById(R.id.driver_name);
            prizes = (TextView) itemView.findViewById(R.id.prizes);
            points = (TextView) itemView.findViewById(R.id.points);
            rank = (TextView) itemView.findViewById(R.id.rank);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}