package adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.noto.shorttrackhero.AppDelegate;
import com.noto.shorttrackhero.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import UI.CircleImageView;
import model.Driver;
import model.FanModel;


public class FanListAdapter extends RecyclerView.Adapter<FanListAdapter.ViewHolder> {
    View v;
    private List<FanModel> driver;
    Context context;
    private LayoutInflater mInflater;
    private Map map = new HashMap<>();
    private Map immap = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fan_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FanModel dv = driver.get(position);
        holder.fan_name.setText(dv.getFan_name() + "");
        holder.time.setText(dv.getCreated()+"");
        AppDelegate.getInstance(context).getImageLoader().get(dv.getFan_image(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.fan_img.setImageBitmap(response.getBitmap());
                }
            }
        });
    }
    public FanListAdapter(Context context, List<FanModel> driver) {
        this.context = context;
        this.driver = driver;
    }
    @Override
    public int getItemCount() {
        return driver.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView fan_img;
        TextView fan_name, time;


        public ViewHolder(View itemView) {
            super(itemView);
            fan_img = (CircleImageView) itemView.findViewById(R.id.fan_img);
            fan_name = (TextView) itemView.findViewById(R.id.fan_name);
            time = (TextView) itemView.findViewById(R.id.member);

        }
    }
}