package adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.noto.shorttrackhero.R;

import java.util.ArrayList;

import model.TrackListModel;


public class TrackAdapter extends ArrayAdapter<TrackListModel> {
    // TODO Auto-generated constructor stub
    private final Activity context;
    ArrayList<TrackListModel> title;
    Integer image;

    public TrackAdapter(Activity context,
                        ArrayList<TrackListModel> title) {
        super(context, R.layout.dialog_listitem, title);
        {
            this.context = context;
            this.title = title;
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.listvalue);
        txtTitle.setText(title.get(position).getCompany_name());
        Log.e("set", title.get(position).getCompany_name() + "");
        return rowView;
    }
}
